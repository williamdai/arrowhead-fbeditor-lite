﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading;
using System.Xml.Linq;
using System.IO;

namespace FBEditor
{
    /// <summary>
    /// Interaction logic for PopupWindow_CreateNewSubApp.xaml
    /// </summary>
    public partial class PopupWindow_CreateNewSubApp : UserControl
    {
        private bool _hideRequest = false;
        private UIElement _parent;
        private bool bChanged = false;
        private Random rnd = new Random();

        public PopupWindow_CreateNewSubApp()
        {
            InitializeComponent();

            Visibility = Visibility.Hidden;
        }

        public void SetParent(UIElement parent)
        {
            _parent = parent;
        }

        private List<string> getAllFBTypes(string sDIR, string sFile)
        {
            List<string> sFilePath = new List<string>();
            //Find Source File in the Root Folder
            //Search through Library
            string[] fileList = Directory.GetFiles(sDIR, sFile);
            if (fileList.Count() > 0)
            {
                sFilePath.AddRange(fileList);
            }
            
            foreach (string SubDIR in Directory.GetDirectories(sDIR))
            {
                sFilePath.AddRange(getAllFBTypes(SubDIR, sFile));
            }

            return sFilePath;
        }

        public string ShowHandlerDialog()
        {
            //Load All FB Types in the FBLib
            List<string> FBTypeList = getAllFBTypes("FBLib\\", "*.fbt");
            FBTypeList.AddRange(getAllFBTypes("FBLib\\", "*.app"));
            for (int i = 0; i < FBTypeList.Count; i++)
            {
                FBTypeList[i] = FBTypeList[i].Substring(FBTypeList[i].LastIndexOf("\\") + 1).Replace(".fbt", "").Replace(".app", "");
            }
            textbox_FBName.Text = "FB" + rnd.Next(100, 1000).ToString();

            combobox_FBType.ItemsSource = FBTypeList;
            Visibility = Visibility.Visible;

            _parent.IsEnabled = false;

            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }

                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            if (!bChanged) return "";

            return (combobox_FBType.SelectedValue as string) + "/" + textbox_FBName.Text;
        }
    
        private void HideHandlerDialog()
        {
            _hideRequest = true;
            Visibility = Visibility.Hidden;
            _parent.IsEnabled = true;
        }

        private void button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            bChanged = false;
            HideHandlerDialog();
        }

        private void button_FB_Click(object sender, RoutedEventArgs e)
        {
            bChanged = true;
            HideHandlerDialog();
        }
    }
}
