﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Linq;
using Northwoods.GoXam;
using Northwoods.GoXam.Model;

namespace FBEditor
{
    /// <summary>
    /// Interaction logic for CompositeFBNetworkEditor.xaml
    /// </summary>
    public partial class CompositeFBNetworkEditor : UserControl
    {
        public CompositeFBNetworkEditor()
        {
            InitializeComponent();

            newInstanceWindow.SetParent(FBDiagram);
            newInstanceWindow.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            newInstanceWindow.VerticalAlignment = System.Windows.VerticalAlignment.Top;

       }

        public void ReloadIOList(XElement xFBType)
        {
            //GetXML() then Replace Interface part from xFBType
            XElement xData = GetXML();

            xFBType.Element("FBNetwork").Remove();
            xFBType.Add(xData);

            LoadXML(xFBType);
        }

        // only use the saved route points after the layout has completed,
        // because links will get the default routing
        private void UpdateRoutes(object sender, DiagramEventArgs e)
        {
            // just set the Route points once per Load
            FBDiagram.LayoutCompleted -= UpdateRoutes;
            foreach (Link link in FBDiagram.Links)
            {
                Wire wire = link.Data as Wire;
                if (wire != null && wire.Points != null && wire.Points.Count() > 1)
                {
                    link.Route.Points = (IList<Point>)wire.Points;
                }
            }
            FBDiagram.PartManager.UpdatesRouteDataPoints = true;  // OK for CustomPartManager to update Transition.Points automatically
        }

        public void LoadXML(XElement xFBType)
        {
            // because we cannot data-bind the Route.Points property,
            // we use a custom PartManager to read/write the Wire.Points data property
            FBDiagram.PartManager = new CustomPartManager();

            var model = new CustomModel();
            
            // set the Route.Points after nodes have been built and the layout has finished
            FBDiagram.LayoutCompleted += UpdateRoutes;
            IDELibrary.FBXMLConverter converter = new IDELibrary.FBXMLConverter();
            XElement xData = null;
            converter.generateFBDiagramFromXElement(ref xData, xFBType);
            model.Load<Unit, Wire>(xData, "Node", "Conn");
            model.Modifiable = true;
            model.HasUndoManager = true;

            FBDiagram.Model = model;

            //FBDiagram.MouseRightButtonUp += Port_RightButtonUp;
        }

        public XElement GetXML()
        {
            var model = FBDiagram.Model as CustomModel;
            if (model == null) return null;
            // copy the Route.Points into each Transition data
            foreach (Link link in FBDiagram.Links)
            {
                Wire wire = link.Data as Wire;
                if (wire != null)
                {
                    wire.Points = new List<Point>(link.Route.Points);
                }
            }
            XElement root = model.Save<Unit, Wire>("Diagram", "Node", "Conn");
            model.IsModified = false;
            IDELibrary.FBXMLConverter converter = new IDELibrary.FBXMLConverter();
            return converter.generateFBTFromFBDiagram(root);
        }

        private void Button_CreateNewFBI_Click(object sender, RoutedEventArgs e)
        {
            newInstanceWindow.Margin = new Thickness(150, 150, 0, 0);
            string str = newInstanceWindow.ShowHandlerDialog();
            if (str == "") return;
            string sFBType = str.Split('/')[0];
            string sFBI = str.Split('/')[1];
            Unit nUnit = new Unit();
            Random rnd = new Random();
            nUnit.Key = "FBI" + rnd.Next(5000, 100000).ToString();
            nUnit.Text = sFBI;
            nUnit.Category = "FBTemplate";
            nUnit.Type = sFBType;
            nUnit.Location = new Point(150, 150);
            //Load File and Add all I/Os
            XDocument xDoc = XDocument.Load(searchFileByName("FBLib\\", sFBType + ".fbt"));
            if (xDoc.Root.Descendants("EventInputs").FirstOrDefault() != null)
            {
                foreach (XElement xIO in xDoc.Root.Descendants("EventInputs").FirstOrDefault().Elements())
                {
                    nUnit.AddSocket("EI", xIO.Attribute("Name").Value, "#59a34c");
                }
            }
            if (xDoc.Root.Descendants("EventOutputs").FirstOrDefault() != null)
            {
                foreach (XElement xIO in xDoc.Root.Descendants("EventOutputs").FirstOrDefault().Elements())
                {
                    nUnit.AddSocket("EO", xIO.Attribute("Name").Value, "#59a34c");
                }
            }
            if (xDoc.Root.Descendants("InputVars").FirstOrDefault() != null)
            {
                foreach (XElement xIO in xDoc.Root.Descendants("InputVars").FirstOrDefault().Elements())
                {
                    nUnit.AddSocket("DI", xIO.Attribute("Name").Value, "#ff8a2e");
                }
            }
            if (xDoc.Root.Descendants("OutputVars").FirstOrDefault() != null)
            {
                foreach (XElement xIO in xDoc.Root.Descendants("OutputVars").FirstOrDefault().Elements())
                {
                    nUnit.AddSocket("DO", xIO.Attribute("Name").Value, "#ff8a2e");
                }
            }
            if (xDoc.Root.Descendants("Sockets").FirstOrDefault() != null)
            {
                foreach (XElement xIO in xDoc.Root.Descendants("Sockets").FirstOrDefault().Elements())
                {
                    nUnit.AddSocket("SOCKET", xIO.Attribute("Name").Value, "#ff5d5d");
                }
            }
            if (xDoc.Root.Descendants("Plugs").FirstOrDefault() != null)
            {
                foreach (XElement xIO in xDoc.Root.Descendants("Plugs").FirstOrDefault().Elements())
                {
                    nUnit.AddSocket("PLUG", xIO.Attribute("Name").Value, "#ff5d5d");
                }
            }
            var model = FBDiagram.Model as CustomModel;
            if (model == null) return;
            model.AddNode(nUnit);
        }

        private string searchFileByName(string sDIR, string sFile)
        {
            string sFilePath = "";
            //Find Source File in the Root Folder
            //Search through Library
            string[] fileList = Directory.GetFiles(sDIR, sFile);
            if (fileList.Count() > 0)
            {
                foreach (string sfile in fileList)
                {
                    if (!sfile.EndsWith(".cpp") && !sfile.EndsWith(".dll"))
                    {
                        sFilePath = sfile;
                        break;
                    }
                }
            }
            else
            {
                foreach (string SubDIR in Directory.GetDirectories(sDIR))
                {
                    sFilePath = searchFileByName(SubDIR, sFile);
                    if (sFilePath != "")
                    {
                        break;
                    }

                }
            }
            return sFilePath;
        }

    }

    public class CustomPartManager : PartManager
    {
        public bool bShowDataConn;

        public CustomPartManager()
        {
            this.UpdatesRouteDataPoints = true;  // call UpdateRouteDataPoints when Link.Route.Points has changed
            this.bShowDataConn = true;
        }

        // this supports undo/redo of link route reshaping
        protected override void UpdateRouteDataPoints(Link link)
        {
            if (!this.UpdatesRouteDataPoints) return;   // in coordination with Load_Click and UpdateRoutes, above
            var data = link.Data as Wire;
            if (data != null)
            {
                data.Points = new List<Point>(link.Route.Points);

                if (data.ConnType == "")
                {
                    bool bFoundFrom = false;
                    bool bFoundTo = false;

                    if (data.From.StartsWith("DIP") || data.From.StartsWith("DI"))
                    {
                        bFoundFrom = true;
                    }
                    else if (data.To.StartsWith("DO"))
                    {
                        bFoundTo = true;
                    }

                    if (!bFoundFrom || !bFoundTo)
                    {
                        //New Connection, Setup Type
                        foreach (var node in Nodes)
                        {
                            var unit = node.Data as Unit;
                            if (data.From == unit.Key)
                            {
                                if (!bFoundFrom)
                                {
                                    foreach (Socket s in unit.DataOutputs)
                                    {
                                        if (data.FromPort == s.Name)
                                        {
                                            bFoundFrom = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        foreach (var node in Nodes)
                        {
                            var unit = node.Data as Unit;
                            if (data.To == unit.Key)
                            {
                                if (!bFoundTo)
                                {
                                    foreach (Socket s in unit.DataInputs)
                                    {
                                        if (data.ToPort == s.Name)
                                        {
                                            bFoundTo = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (bFoundFrom && bFoundTo)
                    {
                        data.ConnType = "DataConn";
                    }

                    //Check Event Connection
                    if (data.ConnType == "")
                    {
                        bFoundFrom = false;
                        bFoundTo = false;

                        if (data.From.StartsWith("EI"))
                        {
                            bFoundFrom = true;
                        }
                        else if (data.To.StartsWith("EO"))
                        {
                            bFoundTo = true;
                        }

                        if (!bFoundFrom || !bFoundTo)
                        {
                            foreach (var node in Nodes)
                            {
                                var unit = node.Data as Unit;
                                if (data.From == unit.Key)
                                {
                                    if (!bFoundFrom)
                                    {
                                        foreach (Socket s in unit.EventOutputs)
                                        {
                                            if (data.FromPort == s.Name)
                                            {
                                                bFoundFrom = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            foreach (var node in Nodes)
                            {
                                var unit = node.Data as Unit;
                                if (data.To == unit.Key)
                                {
                                    if (!bFoundTo)
                                    {
                                        foreach (Socket s in unit.EventInputs)
                                        {
                                            if (data.ToPort == s.Name)
                                            {
                                                bFoundTo = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (bFoundFrom && bFoundTo)
                        {
                            data.ConnType = "EventConn";
                        }
                    }

                    //Check Adapter Connection
                    if (data.ConnType == "")
                    {
                        bFoundFrom = false;
                        bFoundTo = false;

                        foreach (var node in Nodes)
                        {
                            var unit = node.Data as Unit;
                            if (data.From == unit.Key)
                            {
                                if (!bFoundFrom)
                                {
                                    foreach (Socket s in unit.Plugs)
                                    {
                                        if (data.FromPort == s.Name)
                                        {
                                            bFoundFrom = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        foreach (var node in Nodes)
                        {
                            var unit = node.Data as Unit;
                            if (data.To == unit.Key)
                            {
                                if (!bFoundTo)
                                {
                                    foreach (Socket s in unit.Sockets)
                                    {
                                        if (data.ToPort == s.Name)
                                        {
                                            bFoundTo = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        
                        if (bFoundFrom && bFoundTo)
                        {
                            data.ConnType = "AdapterConn";
                        }
                    }
                }

                if (data.ConnType == "DataConn" || data.ConnType == "AdapterConn")
                {
                    if (bShowDataConn && !data.Visible)
                    {
                        data.Visible = true;
                    }
                    else if (!bShowDataConn && data.Visible)
                    {
                        data.Visible = false;
                    }
                }
                else if (data.ConnType == "EventConn")
                {
                    data.Visible = true;
                }
            }
        }
    }

    public class InertListBox : ListBox
    {
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new InertListBoxItem();
        }
    }

    public class InertListBoxItem : ListBoxItem
    {
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            e.Handled = false;
        }
    }

    public class CustomModel : GraphLinksModel<Unit, String, String, Wire>
    {
        // When a Unit gets an extra Socket or when a Socket is removed,
        // tell the Diagram.PartManager that some (or all) of the port FrameworkElements
        // in the Node corresponding to a unit may have moved or changed size.
        protected override void HandleNodePropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.HandleNodePropertyChanged(sender, e);
            Unit unit = sender as Unit;
            if (unit != null && (e.PropertyName == "AddedSocket" || e.PropertyName == "RemovedSocket"))
            {
                RaiseChanged(new ModelChangedEventArgs() { Model = this, Change = ModelChange.InvalidateRelationships, Data = unit });
            }
        }

        protected override void InsertNode(Unit nodedata)
        {
            if (nodedata.Key == "Unit")
            {
                nodedata.Category = "DIPTemplate";
                nodedata.Text = "0";
                nodedata.Type = "";
                Random rnd = new Random();
                nodedata.Key = "DIP" + rnd.Next(5000, 100000).ToString();
                nodedata.AddSocket("DI", "DIP", "Black");
            }
            
            base.InsertNode(nodedata);
        }

        protected override bool CheckLinkValid(Unit fromdata, string fromparam, Unit todata, string toparam, bool ignoreexistinglink, Wire oldlinkdata)
        {
            //Check if both event
            bool bFoundFrom = false;
            bool bFoundTo = false;
            if (fromdata.Type == "EI")
            {
                foreach (Socket s in fromdata.EventInputs)
                {
                    if (fromparam == s.Name)
                    {
                        bFoundFrom = true;
                        break;
                    }
                }
            }
            else
            {
                foreach (Socket s in fromdata.EventOutputs)
                {
                    if (fromparam == s.Name)
                    {
                        bFoundFrom = true;
                        break;
                    }
                }
            }
            if (todata.Type == "EO")
            {
                foreach (Socket s in todata.EventOutputs)
                {
                    if (toparam == s.Name)
                    {
                        bFoundTo = true;
                        break;
                    }
                }
            }
            else
            {
                foreach (Socket s in todata.EventInputs)
                {
                    if (toparam == s.Name)
                    {
                        bFoundTo = true;
                        break;
                    }
                }
            }
            
            if (bFoundFrom && bFoundTo) return true;

            //Check if both data
            bFoundFrom = false;
            bFoundTo = false;
            if (fromdata.Category == "DIPTemplate")
            {
                bFoundFrom = true;
            }
            else if (fromdata.Type == "DI")
            {
                foreach (Socket s in fromdata.DataInputs)
                {
                    if (fromparam == s.Name)
                    {
                        bFoundFrom = true;
                        break;
                    }
                }
            }
            else
            {
                foreach (Socket s in fromdata.DataOutputs)
                {
                    if (fromparam == s.Name)
                    {
                        bFoundFrom = true;
                        break;
                    }
                }
            }
            if (todata.Type == "DO")
            {
                foreach (Socket s in todata.DataOutputs)
                {
                    if (toparam == s.Name)
                    {
                        bFoundTo = true;
                        break;
                    }
                }
            }
            else
            {
                foreach (Socket s in todata.DataInputs)
                {
                    if (toparam == s.Name)
                    {
                        bFoundTo = true;
                        break;
                    }
                }
            }

            if (bFoundFrom && bFoundTo) return true;

            //Check if both Adapter with same type
            string sFoundFrom = "";
            string sFoundTo = "";
            
            foreach (Socket s in fromdata.Plugs)
            {
                if (fromparam == s.Name)
                {
                    sFoundFrom = fromdata.Type;
                    break;
                }
            }

            foreach (Socket s in todata.Sockets)
            {
                if (toparam == s.Name)
                {
                    sFoundTo = todata.Type;
                    break;
                }
            }

            if (sFoundFrom == sFoundTo && sFoundFrom != "") return true;

            return false;
            //return base.CheckLinkValid(fromdata, fromparam, todata, toparam, ignoreexistinglink, oldlinkdata);
        }

        protected override void DeleteNode(Unit nodedata)
        {
            if (nodedata.Type != "EI" && nodedata.Type != "EO"  && nodedata.Type != "DI" && nodedata.Type != "DO")
                base.DeleteNode(nodedata);
        }
    }


    // Each set of "Sockets" has to be a property that is an ObservableCollection
    [Serializable]
    public class Unit : GraphLinksModelNodeData<String>, INotifyPropertyChanged
    {
        //Extra Field For FB Type
        public string Type { get; set; }
        public string Mapping
        {
            get { return _Mapping; }
            set
            {
                if (_Mapping != value)
                {
                    string old = _Mapping;
                    _Mapping = value;
                    RaisePropertyChanged("Mapping", old, value);
                }
            }
        }
        private string _Mapping = "";
        
        // When a Unit is copied, it needs separate collections of Sockets
        public override object Clone()
        {
            Unit unit = (Unit)base.Clone();
            unit._EventInputs = new ObservableCollection<Socket>();
            foreach (Socket s in this.EventInputs) unit._EventInputs.Add((Socket)s.Clone());
            unit._EventOutputs = new ObservableCollection<Socket>();
            foreach (Socket s in this.EventOutputs) unit._EventOutputs.Add((Socket)s.Clone());
            unit._DataInputs = new ObservableCollection<Socket>();
            foreach (Socket s in this.DataInputs) unit._DataInputs.Add((Socket)s.Clone());
            unit._DataOutputs = new ObservableCollection<Socket>();
            foreach (Socket s in this.DataOutputs) unit._DataOutputs.Add((Socket)s.Clone());
            unit._Plugs = new ObservableCollection<Socket>();
            foreach (Socket s in this.Plugs) unit._Plugs.Add((Socket)s.Clone());
            unit._Sockets = new ObservableCollection<Socket>();
            foreach (Socket s in this.Sockets) unit._Sockets.Add((Socket)s.Clone());
            // if you add properties that are not supposed to be shared, deal with them here
            return unit;
        }

        // Property change for undo/redo:
        // We treat adding and removing socket as property changes, and
        // there are settable properties of Socket for each socket to handle.
        public override void ChangeDataValue(ModelChangedEventArgs e, bool undo)
        {
            // Data might be either a Unit or a Socket
            Socket sock = e.Data as Socket;
            if (sock != null)
            {  // if it's a Socket, let it handle undo/redo changes
                sock.ChangeDataValue(e, undo);
            }
            else
            {
                // assume we're dealing with a change to this Unit
                switch (e.PropertyName)
                {
                    case "AddedSocket":
                        sock = e.OldValue as Socket;
                        if (undo)
                            RemoveSocket(sock);
                        else
                            InsertSocket(sock);
                        break;
                    case "RemovedSocket":
                        sock = e.OldValue as Socket;
                        if (undo)
                            InsertSocket(sock);
                        else
                            RemoveSocket(sock);
                        break;
                    // if you add undo-able properties to Unit, handle them here
                    default:
                        base.ChangeDataValue(e, undo);
                        break;
                }
            }
        }

        // write the base element
        public XElement baseMakeXElement(XName n)
        {
            XElement e = new XElement("Node");
            e.Add(XHelper.Attribute("Key", base.Key, ""));
            e.Add(XHelper.Attribute("Category", base.Category, ""));
            e.Add(XHelper.Attribute("Location", base.Location, new Point()));
            e.Add(XHelper.Attribute("Instance", base.Text, ""));
            e.Add(XHelper.Attribute("Type", this.Type, ""));
            return e;
        }

        // load the base element
        public void baseLoadFromXElement(XElement e)
        {
            if (e == null) return;
            base.Key = XHelper.Read("Key", e, "");
            base.Category = XHelper.Read("Category", e, "");
            base.Location = XHelper.Read("Location", e, new Point());
            base.Text = XHelper.Read("Instance", e, "");
            this.Type = XHelper.Read("Type", e, "");
        }

        // write the Sockets as child elements
        public override XElement MakeXElement(XName n)
        {
            //XElement e = base.MakeXElement(n);
            XElement e = baseMakeXElement(n);
            e.Add(this.EventInputs.Select(s => s.MakeXElement()));
            e.Add(this.EventOutputs.Select(s => s.MakeXElement()));
            e.Add(this.DataInputs.Select(s => s.MakeXElement()));
            e.Add(this.DataOutputs.Select(s => s.MakeXElement()));
            e.Add(this.Plugs.Select(s => s.MakeXElement()));
            e.Add(this.Sockets.Select(s => s.MakeXElement()));
            return e;
        }

        // read the child elements as Sockets
        public override void LoadFromXElement(XElement e)
        {
            //base.LoadFromXElement(e);
            baseLoadFromXElement(e);
            foreach (XElement c in e.Elements("Var"))
            {
                InsertSocket(new Socket().LoadFromXElement(c, this));
            }
        }

        public IEnumerable<Socket> EventInputs
        {
            get { return _EventInputs; }
        }
        private ObservableCollection<Socket> _EventInputs = new ObservableCollection<Socket>();

        public IEnumerable<Socket> EventOutputs
        {
            get { return _EventOutputs; }
        }
        private ObservableCollection<Socket> _EventOutputs = new ObservableCollection<Socket>();

        public IEnumerable<Socket> DataInputs
        {
            get { return _DataInputs; }
        }
        private ObservableCollection<Socket> _DataInputs = new ObservableCollection<Socket>();

        public IEnumerable<Socket> DataOutputs
        {
            get { return _DataOutputs; }
        }
        private ObservableCollection<Socket> _DataOutputs = new ObservableCollection<Socket>();

        public IEnumerable<Socket> Plugs
        {
            get { return _Plugs; }
        }
        private ObservableCollection<Socket> _Plugs = new ObservableCollection<Socket>();

        public IEnumerable<Socket> Sockets
        {
            get { return _Sockets; }
        }
        private ObservableCollection<Socket> _Sockets = new ObservableCollection<Socket>();

        // used to find whether a Socket exists for a name
        public Socket FindSocket(String name)
        {
            int i = IndexOf(_EventInputs, name);
            if (i >= 0) return _EventInputs[i];
            i = IndexOf(_EventOutputs, name);
            if (i >= 0) return _EventOutputs[i];
            i = IndexOf(_DataInputs, name);
            if (i >= 0) return _DataInputs[i];
            i = IndexOf(_DataOutputs, name);
            if (i >= 0) return _DataOutputs[i];
            i = IndexOf(_Plugs, name);
            if (i >= 0) return _Plugs[i];
            i = IndexOf(_Sockets, name);
            if (i >= 0) return _Sockets[i];
            return null;
        }

        // create a new Socket
        public void AddSocket(String side, String name, String color)
        {
            Add(Find(side), new Socket() { Unit = this, Side = side, Index = Find(side).Count, Name = name, Color = color });
        }

        // insert an existing Socket
        public void InsertSocket(Socket sock)
        {
            Add(Find(sock.Side), sock);
        }

        // remove an existing Socket
        public void RemoveSocket(Socket sock)
        {
            Remove(Find(sock.Side), sock.Name);
        }

        public ObservableCollection<Socket> Find(String side)
        {
            switch (side)
            {
                case "EI": return _EventInputs;
                case "EO": return _EventOutputs;
                case "DI": return _DataInputs;
                case "DO": return _DataOutputs;
                case "PLUG": return _Plugs;
                case "SOCKET": return _Sockets;
            }
            return null;
        }

        private void Add(ObservableCollection<Socket> socks, Socket s)
        {
            // don't do anything if it's already there
            if (socks.Contains(s)) return;
            // update the collection
            socks.Insert(s.Index, s);
            int n = socks.Count;
            for (int j = 0; j < n; j++)
            {
                socks[j].Index = j;  // always update the Socket.Index
            }
            // notify about the change
            RaisePropertyChanged("AddedSocket", s, null);
        }

        private int IndexOf(ObservableCollection<Socket> socks, String name)
        {
            for (int i = 0; i < socks.Count; i++)
            {
                if (socks[i].Name == name) return i;
            }
            return -1;
        }

        private void Remove(ObservableCollection<Socket> socks, String name)
        {
            int i = IndexOf(socks, name);
            if (i >= 0)
            {  // don't do anything unless it's actually removed
                Socket s = socks[i];
                // update the collection
                socks.RemoveAt(i);
                // always update the Socket.Index
                for (int j = 0; j < socks.Count; j++) socks[j].Index = j;
                // notify about the change
                RaisePropertyChanged("RemovedSocket", s, null);
            }
        }
    }

    [Serializable]
    public class Socket : ICloneable, INotifyPropertyChanged, IChangeDataValue
    {
        // implement ICloneable for copying
        public Object Clone()
        {
            return MemberwiseClone() as Socket;
        }

        // implement INotifyPropertyChanged for data-binding
        [field: NonSerializedAttribute()]
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(String pname, Object oldval, Object newval)
        {
            ModelChangedEventArgs e = new ModelChangedEventArgs(pname, this, oldval, newval);
            // implement INotifyPropertyChanged:
            if (this.PropertyChanged != null) this.PropertyChanged(this, e);
            // implement support for model and undo/redo:
            if (this.Unit != null) this.Unit.OnPropertyChanged(e);
        }

        // implement IChangeDataValue for undo/redo
        public void ChangeDataValue(ModelChangedEventArgs e, bool undo)
        {
            switch (e.PropertyName)
            {
                case "Color": this.Color = (String)e.GetValue(undo); break;
                default: throw new NotImplementedException("Var change: " + e.ToString());
            }
        }

        public XElement MakeXElement()
        {
            XElement e = new XElement("Var");
            e.Add(XHelper.Attribute("Name", this.Name, ""));
            e.Add(XHelper.Attribute("Type", this.Side, ""));
            e.Add(XHelper.Attribute("DataType", this.DataType, ""));
            e.Add(XHelper.Attribute("Index", this.Index, 0));
            e.Add(XHelper.Attribute("Color", this.Color, "Black"));
            e.Add(XHelper.Attribute("ArraySize", this.ArraySize, ""));
            return e;
        }

        public Socket LoadFromXElement(XElement e, Unit unit)
        {
            if (e == null) return this;
            this.Name = XHelper.Read("Name", e, "");
            this.Side = XHelper.Read("Type", e, "");
            this.DataType = XHelper.Read("DataType", e, "");
            this.Index = XHelper.Read("Index", e, 0);
            this.Color = XHelper.Read("Color", e, "Black");
            this.ArraySize = XHelper.Read("ArraySize", e, "");
            this.ConnPort = "";
            this.Unit = unit;

            try
            {
                if (this.Side == "DI")
                {
                    XElement xConnToCheck = e.Parent.Parent.Elements("Conn").Where(item => item.Attribute("ToPort").Value == this.Name && item.Attribute("To").Value == e.Parent.Attribute("Key").Value).FirstOrDefault();
                    if (xConnToCheck.Attribute("From").Value.StartsWith("FBI"))
                    {
                        this.ConnPort = getDiagramInstanceFromKey(e.Parent.Parent, xConnToCheck.Attribute("From").Value) + "." + xConnToCheck.Attribute("FromPort").Value;
                    }
                    else if (xConnToCheck.Attribute("From").Value.StartsWith("DI"))
                    {
                        this.ConnPort = getDiagramInstanceFromKey(e.Parent.Parent, xConnToCheck.Attribute("From").Value);
                    }
                }
                else if (this.Side == "DO")
                {
                    foreach (XElement xConnToCheck in e.Parent.Parent.Elements("Conn").Where(item => item.Attribute("FromPort").Value == this.Name && item.Attribute("From").Value == e.Parent.Attribute("Key").Value))
                    {
                        if (xConnToCheck.Attribute("To").Value.StartsWith("FBI"))
                        {
                            this.ConnPort += getDiagramInstanceFromKey(e.Parent.Parent, xConnToCheck.Attribute("To").Value) + "." + xConnToCheck.Attribute("ToPort").Value + ",";
                        }
                        else if (xConnToCheck.Attribute("To").Value.StartsWith("DO"))
                        {
                            this.ConnPort += getDiagramInstanceFromKey(e.Parent.Parent, xConnToCheck.Attribute("To").Value) + ",";
                        }
                    }

                    if (this.ConnPort.Length > 0)
                    {
                        this.ConnPort = this.ConnPort.Substring(0, this.ConnPort.Length - 1);
                    }
                }
            }
            catch (Exception)
            { }

            return this;
        }

        // these properties aren't expected to change after initialization
        public Unit Unit { get; set; }  // parent pointer
        public String Name { get; set; }
        public String Side { get; set; }
        public String DataType { get; set; }
        public int Index { get; set; }
        public String ArraySize { get; set; }

        public String ConnPort
        {
            get { return _ConnPort; }
            set
            {
                if (_ConnPort != value)
                {
                    String old = _ConnPort;
                    _ConnPort = value;
                    RaisePropertyChanged("ConnPort", old, value);
                }
            }
        }
        private String _ConnPort = "";

        // these property may change dynamically, so they implement change notification
        public String Color
        {
            get { return _Color; }
            set
            {
                if (_Color != value)
                {
                    String old = _Color;
                    _Color = value;
                    RaisePropertyChanged("Color", old, value);
                }
            }
        }
        private String _Color = "Black";

        public bool Visible
        {
            get { return _Visible; }
            set
            {
                if (_Visible != value)
                {
                    bool old = _Visible;
                    _Visible = value;
                    RaisePropertyChanged("Visible", old, value);
                }
            }
        }
        private bool _Visible = false;

        private string getDiagramInstanceFromKey(XElement xDiagram, string sKey)
        {
            try
            {
                return xDiagram.Descendants("Node").Where(item => item.Attribute("Key").Value == sKey).FirstOrDefault().Attribute("Instance").Value;
            }
            catch (Exception)
            {
                return "";
            }
        }
    }

    [Serializable]
    public class Wire : GraphLinksModelLinkData<String, String>
    {
        //additional properties defined
        public String ConnType
        {
            get { return _ConnType; }
            set
            {
                if (_ConnType != value)
                {
                    String old = _ConnType;
                    _ConnType = value;
                    RaisePropertyChanged("ConnType", old, value);
                }
            }
        }
        private String _ConnType = "";

        public bool Visible
        {
            get { return _Visible; }
            set
            {
                if (_Visible != value)
                {
                    bool old = _Visible;
                    _Visible = value;
                    RaisePropertyChanged("Visible", old, value);
                }
            }
        }
        private bool _Visible = false;

        // write the Sockets as child elements
        public override XElement MakeXElement(XName n)
        {
            XElement e = base.MakeXElement(n);
            XAttribute xConnType = new XAttribute("ConnType", this.ConnType);
            e.Add(xConnType);
            return e;
        }

        // read the child elements as Sockets
        public override void LoadFromXElement(XElement e)
        {
            base.LoadFromXElement(e);
            this.ConnType = e.Attribute("ConnType").Value;
            this.Visible = true;
        }
    }


    public class CustomRoute : Route
    {
        protected override double GetEndSegmentLength(Node node, FrameworkElement port, Spot spot, bool from)
        {
            double esl = base.GetEndSegmentLength(node, port, spot, from);
            Unit unit = node.Data as Unit;
            if (unit != null)
            {
                Socket sock = unit.FindSocket(Node.GetPortId(port));
                if (sock != null)
                {
                    var socks = unit.Find(sock.Side);
                    Point thispt = node.GetElementPoint(port, from ? GetFromSpot() : GetToSpot());
                    Point otherpt = this.Link.GetOtherNode(node).GetElementPoint(this.Link.GetOtherPort(port),
                                                                                 from ? GetToSpot() : GetFromSpot());
                    if (Math.Abs(thispt.X - otherpt.X) > 20 || Math.Abs(thispt.Y - otherpt.Y) > 20)
                    {
                        if (sock.Side == "Top" || sock.Side == "Bottom")
                        {
                            if (otherpt.X < thispt.X)
                            {
                                return esl + 4 + sock.Index * 8;
                            }
                            else
                            {
                                return esl + (socks.Count - sock.Index - 1) * 8;
                            }
                        }
                        else
                        {
                            if (otherpt.Y < thispt.Y)
                            {
                                return esl + 4 + sock.Index * 8;
                            }
                            else
                            {
                                return esl + (socks.Count - sock.Index - 1) * 8;
                            }
                        }
                    }
                }
            }
            return esl;
        }

        protected override bool HasCurviness()
        {
            if (Double.IsNaN(this.Curviness)) return true;
            return base.HasCurviness();
        }

        protected override double ComputeCurviness()
        {
            try
            {
                if (Double.IsNaN(this.Curviness))
                {
                    var fromnode = this.Link.FromNode;
                    var fromport = this.Link.FromPort;
                    var fromspot = GetFromSpot();
                    var frompt = fromnode.GetElementPoint(fromport, fromspot);
                    var tonode = this.Link.ToNode;
                    var toport = this.Link.ToPort;
                    var tospot = GetToSpot();
                    var topt = tonode.GetElementPoint(toport, tospot);
                    if (Math.Abs(frompt.X - topt.X) > 20 || Math.Abs(frompt.Y - topt.Y) > 20)
                    {
                        if ((fromspot == Spot.MiddleLeft || fromspot == Spot.MiddleRight) &&
                            (tospot == Spot.MiddleLeft || tospot == Spot.MiddleRight))
                        {
                            double fromseglen = GetEndSegmentLength(fromnode, fromport, fromspot, true);
                            double toseglen = GetEndSegmentLength(tonode, toport, tospot, false);
                            var c = (fromseglen - toseglen) / 2;
                            if (frompt.X + fromseglen >= topt.X - toseglen)
                            {
                                if (frompt.Y < topt.Y) return c;
                                if (frompt.Y > topt.Y) return -c;
                            }
                        }
                        else if ((fromspot == Spot.MiddleTop || fromspot == Spot.MiddleBottom) &&
                                 (tospot == Spot.MiddleTop || tospot == Spot.MiddleBottom))
                        {
                            double fromseglen = GetEndSegmentLength(fromnode, fromport, fromspot, true);
                            double toseglen = GetEndSegmentLength(tonode, toport, tospot, false);
                            var c = (fromseglen - toseglen) / 2;
                            if (frompt.Y + fromseglen >= topt.Y - toseglen)
                            {
                                if (frompt.X < topt.X) return c;
                                if (frompt.X > topt.X) return -c;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            { }

            return base.ComputeCurviness();
        }

        internal Spot GetFromSpot()
        {
            Spot s = this.FromSpot;
            if (s.IsDefault)
            {
                Link link = this.Link;
                if (link != null)
                {
                    FrameworkElement port = link.FromPort;
                    if (port != null)
                    {
                        s = Node.GetFromSpot(port);  // normally, get Spot from the port
                    }
                }
            }
            return s;
        }

        internal Spot GetToSpot()
        {
            Spot s = this.ToSpot;
            if (s.IsDefault)
            {
                Link link = this.Link;
                if (link != null)
                {
                    FrameworkElement port = link.ToPort;
                    if (port != null)
                    {
                        s = Node.GetToSpot(port);  // normally, get Spot from the port
                    }
                }
            }
            return s;
        }
    }
}