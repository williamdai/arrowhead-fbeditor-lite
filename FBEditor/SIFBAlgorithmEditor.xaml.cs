﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using ICSharpCode.AvalonEdit.CodeCompletion;
using ICSharpCode.AvalonEdit.Folding;
using ICSharpCode.AvalonEdit.Highlighting;
using Microsoft.Win32;

namespace FBEditor
{
    /// <summary>
    /// Interaction logic for SIFBAlgorithmEditor.xaml
    /// </summary>
    public partial class SIFBAlgorithmEditor : UserControl
    {
        public XElement xALGData;
        
        public SIFBAlgorithmEditor()
        {
            InitializeComponent();

            // initial highlighting now set by XAML

            textEditor.ShowLineNumbers = true;
        }

        public void LoadXML(XElement xALGs, XElement xFBType, string sALGName)
        {
            XElement xALG = (xALGs.Descendants("Algorithm").Where(item => item.Attribute("Name").Value == sALGName).FirstOrDefault());
            if (xALG != null)
            {
                this.xALGData = xALG;
            
                textbox_Title.Text = xALG.Attribute("Name").Value;
                textblock_Comment.Text = "";
                if (xALG.Attribute("Comment") != null)
                {
                    textblock_Comment.Text = xALG.Attribute("Comment").Value;
                }

                if (xALG.Element("Other").Attribute("Language") != null)
                {
                    if (xALG.Element("Other").Attribute("Language").Value == "CPP" && xALG.Element("Other").Attribute("Text") != null)
                    {
                        textEditor.Text = xALG.Element("Other").Attribute("Text").Value;
                    }
                }
                else
                {
                    textEditor.Text = "";
                }
            }
        }

        public XElement getXML()
        {
            XElement xALG = new XElement("Algorithm");
            XAttribute xALGName = new XAttribute("Name", textbox_Title.Text);
            XAttribute xALGComment = new XAttribute("Comment", textblock_Comment.Text);
            XElement xCode = new XElement("Other");
            XAttribute xCodeLanguage = new XAttribute("Language", "CPP");
            XAttribute xCodeText = new XAttribute("Text", textEditor.Text);
            xCode.Add(xCodeLanguage, xCodeText);
            xALG.Add(xALGName, xALGComment, xCode);
            return xALG;
        }
	
    }
}
