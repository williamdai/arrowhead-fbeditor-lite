﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading;
using System.Xml.Linq;

namespace FBEditor
{
    /// <summary>
    /// Interaction logic for PopupWindow_ECTranEditor.xaml
    /// </summary>
    public partial class PopupWindow_ECTranEditor : UserControl
    {
        private bool _hideRequest = false;
        private UIElement _parent;
        private bool bChanged = false;
        private string sECTranCond;
        private string sECTComment;
        
        public PopupWindow_ECTranEditor()
        {
            InitializeComponent();

            Visibility = Visibility.Hidden;
        }

        public void SetParent(UIElement parent)
        {
            _parent = parent;
        }

        public List<string> ShowHandlerDialog(string sECTranCond, List<string> EIList, string sECTComment)
        {
            textblock_Title.Text = "Edit EC Transition";
            this.sECTranCond = sECTranCond;
            this.sECTComment = sECTComment;

            string sGuardCond = "";
            string sEventCond = "";
            
            if (sECTranCond == "1")
            {
                sEventCond = sECTranCond;
            }
            else if ((sECTranCond.IndexOf("[") == 0 && sECTranCond.IndexOf("]") == sECTranCond.Length - 1))
            {
                sGuardCond = sECTranCond;
            }
            else if (sECTranCond.IndexOf("[") > 0 && sECTranCond.IndexOf("]") > sECTranCond.IndexOf("["))
            {
                sEventCond = sECTranCond.Substring(0, sECTranCond.IndexOf("[")).Replace(" ", "");
                sGuardCond = sECTranCond.Substring(sECTranCond.IndexOf("[") + 1, sECTranCond.LastIndexOf("]") - sECTranCond.IndexOf("[") - 1);
            }
            else
            {
                sEventCond = sECTranCond.Replace(" ", "");
            }

            textbox_Comment.Text = sGuardCond;
            textbox_ECTComment.Text = sECTComment;
            combobox_Event.ItemsSource = EIList;
            combobox_Event.SelectedValue = sEventCond;
            Visibility = Visibility.Visible;

            _parent.IsEnabled = false;

            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }

                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            string sReturn = (combobox_Event.SelectedValue as string);
            if (textbox_Comment.Text.Replace(" ", "") != "")
            {
                sReturn += " [ " + textbox_Comment.Text + " ]";
            }
           
            List<string> newList = new List<string>();

            if (bChanged)
            {
                newList.Add(sReturn);
                newList.Add(textbox_ECTComment.Text);
            }
            else
            {
                newList.Add(this.sECTranCond);
                newList.Add(this.sECTComment);
            }

            return newList;
        }
    
        private void HideHandlerDialog()
        {
            _hideRequest = true;
            Visibility = Visibility.Hidden;
            _parent.IsEnabled = true;
        }

        private void button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            bChanged = false;
            HideHandlerDialog();
        }

        private void button_OK_Click(object sender, RoutedEventArgs e)
        {
            bChanged = true;
            HideHandlerDialog();
        }
    }
}
