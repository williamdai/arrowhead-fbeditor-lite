﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Linq;
using Northwoods.GoXam;
using Northwoods.GoXam.Model;

namespace FBEditor
{
    /// <summary>
    /// Interaction logic for SystemConfigurationApplicationEditor.xaml
    /// </summary>
    public partial class SystemConfigurationApplicationEditor : UserControl
    {

        public SystemConfigurationApplicationEditor()
        {
            InitializeComponent();

            newInstanceWindow.SetParent(FBDiagram);
            newInstanceWindow.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            newInstanceWindow.VerticalAlignment = System.Windows.VerticalAlignment.Top;
        }

        // only use the saved route points after the layout has completed,
        // because links will get the default routing
        private void UpdateRoutes(object sender, DiagramEventArgs e)
        {
            // just set the Route points once per Load
            FBDiagram.LayoutCompleted -= UpdateRoutes;
            foreach (Link link in FBDiagram.Links)
            {
                Wire wire = link.Data as Wire;
                if (wire != null && wire.Points != null && wire.Points.Count() > 1)
                {
                    link.Route.Points = (IList<Point>)wire.Points;
                }
            }
            FBDiagram.PartManager.UpdatesRouteDataPoints = true;  // OK for CustomPartManager to update Transition.Points automatically
        }

        public void LoadXML(XElement xFBType, string sAPP, XElement xMappings)
        {
            // because we cannot data-bind the Route.Points property,
            // we use a custom PartManager to read/write the Wire.Points data property
            FBDiagram.PartManager = new CustomApplicationPartManager();

            var model = new CustomApplicationModel();
            
            // set the Route.Points after nodes have been built and the layout has finished
            FBDiagram.LayoutCompleted += UpdateRoutes;
            IDELibrary.FBXMLConverter converter = new IDELibrary.FBXMLConverter();
            XElement xData = null;
            converter.generateSCApplicationFromXElement(ref xData, xFBType);
            model.Load<Unit, Wire>(xData, "Node", "Conn");
            model.Modifiable = true;
            model.HasUndoManager = true;

            FBDiagram.Model = model;
            
        }

        public XElement GetXML()
        {
            var model = FBDiagram.Model as CustomApplicationModel;
            if (model == null) return null;
            // copy the Route.Points into each Transition data
            foreach (Link link in FBDiagram.Links)
            {
                Wire wire = link.Data as Wire;
                if (wire != null)
                {
                    wire.Points = new List<Point>(link.Route.Points);
                }
            }
            XElement root = model.Save<Unit, Wire>("Diagram", "Node", "Conn");
            model.IsModified = false;
            IDELibrary.FBXMLConverter converter = new IDELibrary.FBXMLConverter();
            return converter.generateFBTFromSCApplication(root);
        }

        private void Button_CreateNewFBI_Click(object sender, RoutedEventArgs e)
        {
            newInstanceWindow.Margin = new Thickness(150, 150, 0, 0);
            string str = newInstanceWindow.ShowHandlerDialog();
            if (str == "") return;
            string sFBType = str.Split('/')[0];
            string sFBI = str.Split('/')[1];
            Unit nUnit = new Unit();
            Random rnd = new Random();
            nUnit.Key = "FBI" + rnd.Next(5000, 100000).ToString();
            nUnit.Text = sFBI;
            nUnit.Category = "FBTemplate";
            nUnit.Type = sFBType;
            nUnit.Location = new Point(150, 150);
            //Load File and Add all I/Os
            string sFileToLoad = searchFileByName("FBLib\\", sFBType + ".fbt");
            if (sFileToLoad != "")
            {
                XDocument xDoc = XDocument.Load(sFileToLoad);
                if (xDoc.Root.Descendants("EventInputs").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("EventInputs").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("EI", xIO.Attribute("Name").Value, "#59a34c");
                    }
                }
                if (xDoc.Root.Descendants("EventOutputs").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("EventOutputs").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("EO", xIO.Attribute("Name").Value, "#59a34c");
                    }
                }
                if (xDoc.Root.Descendants("InputVars").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("InputVars").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("DI", xIO.Attribute("Name").Value, "#ff8a2e");
                    }
                }
                if (xDoc.Root.Descendants("OutputVars").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("OutputVars").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("DO", xIO.Attribute("Name").Value, "#ff8a2e");
                    }
                }
                if (xDoc.Root.Descendants("Sockets").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("Sockets").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("SOCKET", xIO.Attribute("Name").Value, "#ff5d5d");
                    }
                }
                if (xDoc.Root.Descendants("Plugs").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("Plugs").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("PLUG", xIO.Attribute("Name").Value, "#ff5d5d");
                    }
                }
            }
            else
            {
                sFileToLoad = searchFileByName("FBLib\\", sFBType + ".app");
                if (sFileToLoad == "") return;
                XDocument xDoc = XDocument.Load(sFileToLoad);
                if (xDoc.Root.Descendants("SubAppEventInputs").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("SubAppEventInputs").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("EI", xIO.Attribute("Name").Value, "#59a34c");
                    }
                }
                if (xDoc.Root.Descendants("SubAppEventOutputs").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("SubAppEventOutputs").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("EO", xIO.Attribute("Name").Value, "#59a34c");
                    }
                }
                if (xDoc.Root.Descendants("InputVars").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("InputVars").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("DI", xIO.Attribute("Name").Value, "#ff8a2e");
                    }
                }
                if (xDoc.Root.Descendants("OutputVars").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("OutputVars").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("DO", xIO.Attribute("Name").Value, "#ff8a2e");
                    }
                }
            }
            var model = FBDiagram.Model as CustomApplicationModel;
            if (model == null) return;
            model.AddNode(nUnit);
        }

        private string searchFileByName(string sDIR, string sFile)
        {
            string sFilePath = "";
            //Find Source File in the Root Folder
            //Search through Library
            string[] fileList = Directory.GetFiles(sDIR, sFile);
            if (fileList.Count() > 0)
            {
                foreach (string sfile in fileList)
                {
                    if (!sfile.EndsWith(".cpp") && !sfile.EndsWith(".dll"))
                    {
                        sFilePath = sfile;
                        break;
                    }
                }
            }
            else
            {
                foreach (string SubDIR in Directory.GetDirectories(sDIR))
                {
                    sFilePath = searchFileByName(SubDIR, sFile);
                    if (sFilePath != "")
                    {
                        break;
                    }

                }
            }
            return sFilePath;
        }

    }

    public class CustomApplicationPartManager : PartManager
    {
        public bool bShowDataConn; 
        public bool bShowMapping;

        public CustomApplicationPartManager()
        {
            this.UpdatesRouteDataPoints = true;
            this.bShowDataConn = true;
            this.bShowMapping = false;
        }

        // this supports undo/redo of link route reshaping
        protected override void UpdateRouteDataPoints(Link link)
        {
            if (!this.UpdatesRouteDataPoints) return;   // in coordination with Load_Click and UpdateRoutes, above
            var data = link.Data as Wire;
            if (data != null)
            {
                data.Points = new List<Point>(link.Route.Points);

                if (data.ConnType == "")
                {
                    bool bFoundFrom = false;
                    bool bFoundTo = false;

                    if (data.From.StartsWith("DIP"))
                    {
                        bFoundFrom = true;
                    }

                    if (!bFoundFrom)
                    {
                        //New Connection, Setup Type
                        foreach (var node in Nodes)
                        {
                            var unit = node.Data as Unit;
                            if (data.From == unit.Key)
                            {
                                if (!bFoundFrom)
                                {
                                    foreach (Socket s in unit.DataOutputs)
                                    {
                                        if (data.FromPort == s.Name)
                                        {
                                            bFoundFrom = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    foreach (var node in Nodes)
                    {
                        var unit = node.Data as Unit;
                        if (data.To == unit.Key)
                        {
                            if (!bFoundTo)
                            {
                                foreach (Socket s in unit.DataInputs)
                                {
                                    if (data.ToPort == s.Name)
                                    {
                                        bFoundTo = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    
                    if (bFoundFrom && bFoundTo)
                    {
                        data.ConnType = "DataConn";
                    }

                    //Check Event Connection
                    if (data.ConnType == "")
                    {
                        bFoundFrom = false;
                        bFoundTo = false;

                        foreach (var node in Nodes)
                        {
                            var unit = node.Data as Unit;
                            if (data.From == unit.Key)
                            {
                                if (!bFoundFrom)
                                {
                                    foreach (Socket s in unit.EventOutputs)
                                    {
                                        if (data.FromPort == s.Name)
                                        {
                                            bFoundFrom = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        foreach (var node in Nodes)
                        {
                            var unit = node.Data as Unit;
                            if (data.To == unit.Key)
                            {
                                if (!bFoundTo)
                                {
                                    foreach (Socket s in unit.EventInputs)
                                    {
                                        if (data.ToPort == s.Name)
                                        {
                                            bFoundTo = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        

                        if (bFoundFrom && bFoundTo)
                        {
                            data.ConnType = "EventConn";
                        }
                    }

                    //Check Adapter Connection
                    if (data.ConnType == "")
                    {
                        bFoundFrom = false;
                        bFoundTo = false;

                        foreach (var node in Nodes)
                        {
                            var unit = node.Data as Unit;
                            if (data.From == unit.Key)
                            {
                                if (!bFoundFrom)
                                {
                                    foreach (Socket s in unit.Plugs)
                                    {
                                        if (data.FromPort == s.Name)
                                        {
                                            bFoundFrom = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        foreach (var node in Nodes)
                        {
                            var unit = node.Data as Unit;
                            if (data.To == unit.Key)
                            {
                                if (!bFoundTo)
                                {
                                    foreach (Socket s in unit.Sockets)
                                    {
                                        if (data.ToPort == s.Name)
                                        {
                                            bFoundTo = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        if (bFoundFrom && bFoundTo)
                        {
                            data.ConnType = "AdapterConn";
                        }
                    }
                }

                if (data.ConnType == "DataConn" || data.ConnType == "AdapterConn")
                {
                    if (bShowDataConn && !data.Visible)
                    {
                        data.Visible = true;
                    }
                    else if (!bShowDataConn && data.Visible)
                    {
                        data.Visible = false;
                    }
                }
                else if (data.ConnType == "EventConn")
                {
                    data.Visible = true;
                }
            }
        }
    }

    public class CustomApplicationModel : GraphLinksModel<Unit, String, String, Wire>
    {
        // When a Unit gets an extra Socket or when a Socket is removed,
        // tell the Diagram.PartManager that some (or all) of the port FrameworkElements
        // in the Node corresponding to a unit may have moved or changed size.
        protected override void HandleNodePropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.HandleNodePropertyChanged(sender, e);
            Unit unit = sender as Unit;
            if (unit != null && (e.PropertyName == "AddedSocket" || e.PropertyName == "RemovedSocket"))
            {
                RaiseChanged(new ModelChangedEventArgs() { Model = this, Change = ModelChange.InvalidateRelationships, Data = unit });
            }
        }

        protected override void InsertNode(Unit nodedata)
        {
            if (nodedata.Key == "Unit")
            {
                nodedata.Category = "DIPTemplate";
                nodedata.Text = "0";
                nodedata.Type = "";
                Random rnd = new Random();
                nodedata.Key = "DIP" + rnd.Next(5000, 100000).ToString();
                nodedata.AddSocket("DI", "DIP", "#ff8a2e");
            }
            
            base.InsertNode(nodedata);
        }

        protected override bool CheckLinkValid(Unit fromdata, string fromparam, Unit todata, string toparam, bool ignoreexistinglink, Wire oldlinkdata)
        {
            //Check if both event
            bool bFoundFrom = false;
            bool bFoundTo = false;
            
            foreach (Socket s in fromdata.EventOutputs)
            {
                if (fromparam == s.Name)
                {
                    bFoundFrom = true;
                    break;
                }
            }
            foreach (Socket s in todata.EventInputs)
            {
                if (toparam == s.Name)
                {
                    bFoundTo = true;
                    break;
                }
            }
            
            if (bFoundFrom && bFoundTo) return true;

            //Check if both data
            bFoundFrom = false;
            bFoundTo = false;
            if (fromdata.Category == "DIPTemplate")
            {
                bFoundFrom = true;
            }

            if (!bFoundFrom)
            {
                foreach (Socket s in fromdata.DataOutputs)
                {
                    if (fromparam == s.Name)
                    {
                        bFoundFrom = true;
                        break;
                    }
                }
            }
            foreach (Socket s in todata.DataInputs)
            {
                if (toparam == s.Name)
                {
                    bFoundTo = true;
                    break;
                }
            }
            
            if (bFoundFrom && bFoundTo) return true;

            //Check if both Adapter with same type
            string sFoundFrom = "";
            string sFoundTo = "";

            foreach (Socket s in fromdata.Plugs)
            {
                if (fromparam == s.Name)
                {
                    sFoundFrom = fromdata.Type;
                    break;
                }
            }

            foreach (Socket s in todata.Sockets)
            {
                if (toparam == s.Name)
                {
                    sFoundTo = todata.Type;
                    break;
                }
            }

            if (sFoundFrom == sFoundTo && sFoundFrom != "") return true;

            return false;
            //return base.CheckLinkValid(fromdata, fromparam, todata, toparam, ignoreexistinglink, oldlinkdata);
        }
    }
}