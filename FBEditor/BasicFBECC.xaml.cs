﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Xml.Linq;
using Northwoods.GoXam;
using Northwoods.GoXam.Model;
using Northwoods.GoXam.Tool;
using System.Globalization;

namespace FBEditor
{
    /// <summary>
    /// Interaction logic for BasicFBECC.xaml
    /// </summary>
    public partial class BasicFBECC : UserControl
    {
        private List<string> EIList;
        private List<string> EOList;
        private List<string> ALGList;

        public BasicFBECC()
        {
            InitializeComponent();

            ECTranWindow.SetParent(ECCMainWindow);
            ECTranWindow.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            ECTranWindow.VerticalAlignment = System.Windows.VerticalAlignment.Top;

            commentWindow.SetParent(ECCMainWindow);
            commentWindow.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            commentWindow.VerticalAlignment = System.Windows.VerticalAlignment.Top;
        }

        public void LoadXML(XElement xFBType)
        {
            // because we cannot data-bind the Route.Points property,
            // we use a custom PartManager to read/write the Transition.Points data property
            ECCDiagram.PartManager = new ECCCustomPartManager();

            // initialize it from data in an XML file that is an embedded resource
            IDELibrary.FBXMLConverter xmlConverter = new IDELibrary.FBXMLConverter();
            XElement xData = xmlConverter.generateBFBECCFromXML(xFBType);

            // create the diagram's data model
            var model = new GraphLinksModel<ECState, String, String, ECTransition>();
            ECCDiagram.LayoutCompleted += UpdateRoutes;
            model.Load<ECState, ECTransition>(xData, "ECState", "ECTransition");
            model.Modifiable = true;  // let the user modify the graph
            model.HasUndoManager = true;  // support undo/redo
            ECCDiagram.Model = model;

            // add a tool that lets the user shift the position of the link labels
            var tool = new SimpleLabelDraggingTool();
            tool.Diagram = ECCDiagram;
            ECCDiagram.MouseMoveTools.Insert(0, tool);

            UpdateECActionCount();

            UpdateECStateList(xFBType);
        }

        public void LoadFile(string sFileName)
        {
            // because we cannot data-bind the Route.Points property,
            // we use a custom PartManager to read/write the Transition.Points data property
            ECCDiagram.PartManager = new ECCCustomPartManager();

            IDELibrary.FBXMLConverter xmlConverter = new IDELibrary.FBXMLConverter();
            XElement xData = xmlConverter.generateBFBECCFromFile(sFileName);

            var model = new GraphLinksModel<ECState, String, String, ECTransition>();
            ECCDiagram.LayoutCompleted += UpdateRoutes;
            model.Load<ECState, ECTransition>(xData, "ECState", "ECTransition");
            model.Modifiable = true;  
            model.HasUndoManager = true; 
            ECCDiagram.Model = model;

            var tool = new SimpleLabelDraggingTool();
            tool.Diagram = ECCDiagram;
            ECCDiagram.MouseMoveTools.Insert(0, tool);

            UpdateECActionCount();

            XDocument xDoc = XDocument.Load(sFileName);
            UpdateECStateList(xDoc.Root);
        }

        public XElement getXML()
        {
            XElement xECC = new XElement("ECC");

            //EC States
            foreach (Node e in ECCDiagram.Nodes)
            {
                ECState u = e.Data as ECState;

                XElement xECState = new XElement("ECState");
                AddAttributeToNode(ref xECState, "Name", u.Text);
                AddAttributeToNode(ref xECState, "Comment", u.Comment);
                AddAttributeToNode(ref xECState, "x", u.Location.X.ToString());
                AddAttributeToNode(ref xECState, "y", u.Location.Y.ToString());

                foreach (ECActionSocket s in u.ECActions)
                {
                    XElement xECAction = new XElement("ECAction");
                    AddAttributeToNode(ref xECAction, "Algorithm", s.Algorithm);
                    AddAttributeToNode(ref xECAction, "Output", s.Output);
                    xECState.Add(xECAction);
                }

                xECC.Add(xECState);

                //EC Transitions From this EC State
                foreach (Link l in ECCDiagram.Links.Where(item => (item.Data as ECTransition).From == u.Key))
                {
                    ECTransition ect = l.Data as ECTransition;
                    
                    XElement xECTran = new XElement("ECTransition");
                    AddAttributeToNode(ref xECTran, "Source", getNodeNameFromLink(ect.From));
                    AddAttributeToNode(ref xECTran, "Destination", getNodeNameFromLink(ect.To));
                    AddAttributeToNode(ref xECTran, "Condition", ect.Text.Substring(0, ect.Text.LastIndexOf("{")));
                    AddAttributeToNode(ref xECTran, "Comment", ect.Comment);
                    Point sp = ect.Points.ElementAt(0);
                    Point ep = ect.Points.ElementAt(ect.Points.Count() - 1);
                    AddAttributeToNode(ref xECTran, "x", Convert.ToString(ep.X - sp.X, CultureInfo.InvariantCulture));
                    AddAttributeToNode(ref xECTran, "y", Convert.ToString(ep.Y - sp.Y, CultureInfo.InvariantCulture));
                    xECC.Add(xECTran);
                }
            }

            return xECC;
        }

        private string getNodeNameFromLink(string linknode)
        {
            foreach (Node e in ECCDiagram.Nodes)
            {
                ECState u = e.Data as ECState;
                if (u.Key == linknode)
                {
                    return u.Text;
                }
            }

            return "";
        }

        private void AddAttributeToNode(ref XElement node, string sAttr, string sValue)
        {
            if (node.Attribute(sAttr) != null)
            {
                node.Attribute(sAttr).Value = sValue;
            }
            else
            {
                XAttribute xAttr = new XAttribute(sAttr, sValue);
                node.Add(xAttr);
            }
        }

        public void UpdateECStateList(XElement xFBType)
        {
            UpdateEventList(xFBType);

            //Update the Algorithm List
            var vALGList = xFBType.Descendants("BasicFB").FirstOrDefault();
            if (vALGList != null)
            {
                UpdateALGList(xFBType.Descendants("BasicFB").FirstOrDefault());
            }
        }

        public void UpdateEventList(XElement xFBType)
        {
            //Update the EI List
            EIList = new List<string>();
            EIList.Add("");
            EIList.Add("1");
            var vEIList = xFBType.Descendants("EventInputs").FirstOrDefault();

            if (vEIList != null)
            {
                if (vEIList.Elements().Count() > 0)
                {
                    foreach (XElement xEvent in vEIList.Elements())
                    {
                        EIList.Add(xEvent.Attribute("Name").Value);
                    }
                }
            }

            //Update the EO List
            EOList = new List<string>();
            EOList.Add("");
            var vEOList = xFBType.Descendants("EventOutputs").FirstOrDefault();

            if (vEOList != null)
            {
                if (vEOList.Elements().Count() > 0)
                {
                    foreach (XElement xEvent in vEOList.Elements())
                    {
                        EOList.Add(xEvent.Attribute("Name").Value);
                    }
                }
            }

            UpdateDiagramListItemSource(true, false);
        }

        public void UpdateALGList(XElement xALGs)
        {
            //Update the Algorithm List
            ALGList = new List<string>();
            ALGList.Add("");

            if (xALGs.Elements("Algorithm").Count() > 0)
            {
                foreach (XElement xALG in xALGs.Elements("Algorithm"))
                {
                    ALGList.Add(xALG.Attribute("Name").Value);
                }
            }

            UpdateDiagramListItemSource(false, true);
        }

        private void UpdateDiagramListItemSource(bool bEO, bool bALG)
        {
            foreach (ECState u in ECCDiagram.NodesSource)
            {
                foreach (ECActionSocket s in u.ECActions)
                {
                    if (bEO)
                        s.EOList = EOList;
                    if (bALG)
                        s.ALGList = ALGList;
                }
            }
        }

        private void UpdateECActionCount()
        {
            foreach (ECState u in ECCDiagram.NodesSource)
            {
                if (u.ECActions.Count() > 0)
                {
                    u.Visible = true;
                }
                else
                {
                    u.Visible = false;
                }
                foreach (ECActionSocket s in u.ECActions)
                {
                    s.Ref = u.Text + "_" + s.Index.ToString();
                }
            }
        }

        // only use the saved route points after the layout has completed,
        // because links will get the default routing
        private void UpdateRoutes(object sender, DiagramEventArgs e)
        {
            // just set the Route points once per Load
            ECCDiagram.LayoutCompleted -= UpdateRoutes;
            foreach (Link link in ECCDiagram.Links)
            {
                ECTransition transition = link.Data as ECTransition;
                if (transition != null && transition.Points != null && transition.Points.Count() > 1)
                {
                    link.Route.Points = (IList<Point>)transition.Points;
                }
            }
            ECCDiagram.PartManager.UpdatesRouteDataPoints = true;  // OK for CustomPartManager to update Transition.Points automatically
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // find the Adornment "parent" for the Button
            Adornment ad = Part.FindAncestor<Adornment>(e.OriginalSource as UIElement);
            if (ad == null) return;
            // its AdornedPart should be a Node that is bound to a State object
            ECState ecs = ad.AdornedPart.Data as ECState;
            if (ecs == null) return;
            // make all changes to the model within a transaction
            ECCDiagram.StartTransaction("Add State");
            // create a new State, add it to the model, and create a link from
            // the selected node data to the new node data
            ECActionSocket newECACtion = new ECActionSocket();
            newECACtion.Algorithm = "";
            newECACtion.Output = "";
            newECACtion.Index = getNextECActionIndex(ecs.Text);
            ecs.InsertSocket(newECACtion);
            UpdateECActionCount();
            UpdateDiagramListItemSource(true, true);
            ECCDiagram.CommitTransaction("Add State");
        }

        private int getNextECActionIndex(string name)
        {
            foreach (Node e in ECCDiagram.Nodes)
            {
                ECState u = e.Data as ECState;
                if (u.Text == name)
                    return u.ECActions.Count();
            }

            return 0;
        }

        private void TextBlock_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            string sECState = (sender as StackPanel).Tag.ToString().Split('_')[0];
            string sID = (sender as StackPanel).Tag.ToString().Split('_')[1];
            try
            {
                int iID = Convert.ToInt32(sID);
                if (iID >= 0)
                {
                    foreach (Node node in ECCDiagram.Nodes)
                    {
                        ECState u = node.Data as ECState;
                        if (u.Text == sECState)
                        {
                            u.RemoveSocket(u.ECActions.ElementAt(iID));
                            UpdateECActionCount();
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            { }
        }

        private void EditECStateComment_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            string sECState = (sender as TextBlock).Text;
            double iX = e.GetPosition(ECCMainWindow).X - 150;
            if (iX < 0)
            {
                iX = 20;
            }
            double iY = e.GetPosition(ECCMainWindow).Y;
            if (iY > 250)
            {
                iY = 250;
            }
            foreach (Node node in ECCDiagram.Nodes)
            {
                ECState u = node.Data as ECState;
                if (u.Text == sECState)
                {
                    commentWindow.Margin = new Thickness(iX, iY, 0, 0);
                    u.Comment = commentWindow.ShowHandlerDialog(sECState + " Comment", u.Comment);
                    break;
                }
            }
        }

        private void RemoveECState(string sECState)
        {
            foreach (Node node in ECCDiagram.Nodes)
            {
                ECState u = node.Data as ECState;
                if (u.Text == sECState)
                {
                    List<ECTransition> tList = new List<ECTransition>();

                    foreach (Link link in ECCDiagram.Links)
                    {
                        ECTransition t = link.Data as ECTransition;
                        if (t.From == u.Key || t.To == u.Key)
                        {
                            tList.Add(t);
                        }
                    }

                    foreach (ECTransition t in tList)
                    {
                        ECCDiagram.PartManager.RemoveLinkForData(t, ECCDiagram.Model);
                    }

                    ECCDiagram.PartManager.RemoveNodeForData(u, ECCDiagram.Model);
                    break;
                }
            }
        }

        private void TextBlock_ECTran_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            double iX = e.GetPosition(ECCMainWindow).X - 150;
            if (iX < 0)
            {
                iX = 20;
            }
            double iY = e.GetPosition(ECCMainWindow).Y;
            if (iY > 250)
            {
                iY = 250;
            }

            ECTranWindow.Margin = new Thickness(iX, iY, 0, 0);
            List<string> results = ECTranWindow.ShowHandlerDialog((sender as TextBlock).Text, EIList, (sender as TextBlock).Tag == null ? "" : (sender as TextBlock).Tag.ToString());
            (sender as TextBlock).Text = results[0];
            (sender as TextBlock).Tag = results[1];
        }
    }

    public class ECCCustomPartManager : PartManager
    {
        public ECCCustomPartManager()
        {
            this.UpdatesRouteDataPoints = true;  // call UpdateRouteDataPoints when Link.Route.Points has changed
        }

        // this supports undo/redo of link route reshaping
        protected override void UpdateRouteDataPoints(Link link)
        {
            if (!this.UpdatesRouteDataPoints) return;   // in coordination with Load_Click and UpdateRoutes, above
            var data = link.Data as ECTransition;
            if (data != null)
            {
                data.Points = new List<Point>(link.Route.Points);
            }
        }
    }

    // the data for each node; the predefined data class is enough
    [Serializable]
    public class ECState : GraphLinksModelNodeData<String>
    {
        public string Comment { get; set; }
        public string selected { get; set; }
        public bool Visible
        {
            get { return _Visible; }
            set
            {
                if (_Visible != value)
                {
                    bool old = _Visible;
                    _Visible = value;
                    RaisePropertyChanged("Visible", old, value);
                }
            }
        }
        private bool _Visible = true;

        public ECState()
        {
            this.Key = "ECS" + GetRandomNumber().ToString();  // be sure to provide an initial non-null value for the Key
            this.Text = "NewState";
            this.Category = "StateTemplate";
            this.Visible = false;
            Comment = "";
        }

        private int GetRandomNumber()
        {
            Random rand = new Random((int)DateTime.Now.Ticks);
            return rand.Next(10, 100);
        }

        public override void OnPropertyChanged(ModelChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Data is ECState && e.PropertyName == "Text")
            {
                if ((string)e.NewValue == "")
                {
                    MessageBox.Show("EC State Name cannot be empty!", "ERROR");
                    return;
                }
            }
        }

        // note that adding properties here means also overriding MakeXElement and LoadFromXElement
        // write the Sockets as child elements
        public override XElement MakeXElement(XName n)
        {
            XElement e = new XElement("ECState");
            e.Add(XHelper.Attribute("Key", base.Key, ""));
            e.Add(XHelper.Attribute("Name", base.Text, ""));
            e.Add(XHelper.Attribute("Location", base.Location, new Point()));
            e.Add(XHelper.Attribute("Comment", this.Comment, ""));
            e.Add(XHelper.Attribute("Category", base.Category, ""));
            return e;
        }

        // read the child elements as Sockets
        public override void LoadFromXElement(XElement e)
        {
            if (e == null) return;
            base.Key = XHelper.Read("Key", e, "");
            base.Text = XHelper.Read("Name", e, "");
            base.Location = XHelper.Read("Location", e, new Point());
            this.Comment = XHelper.Read("Comment", e, "");
            base.Category = XHelper.Read("Category", e, "");
            foreach (XElement c in e.Elements("ECAction"))
            {
                InsertSocket(new ECActionSocket().LoadFromXElement(c));
            }
        }

        public IEnumerable<ECActionSocket> ECActions
        {
            get { return _ECActions; }
        }
        private ObservableCollection<ECActionSocket> _ECActions = new ObservableCollection<ECActionSocket>();

        // insert an existing Socket
        public void InsertSocket(ECActionSocket sock)
        {
            Add(_ECActions, sock);
        }

        // remove an existing Socket
        public void RemoveSocket(ECActionSocket sock)
        {
            Remove(_ECActions, sock.Index);
        }

        private void Add(ObservableCollection<ECActionSocket> socks, ECActionSocket s)
        {
            // don't do anything if it's already there
            if (socks.Contains(s)) return;
            // update the collection
            socks.Insert(s.Index, s);
            int n = socks.Count;
            for (int j = 0; j < n; j++)
            {
                socks[j].Index = j;  // always update the Socket.Index
            }
            // notify about the change
            RaisePropertyChanged("AddedSocket", s, null);
        }

        private int IndexOf(ObservableCollection<ECActionSocket> socks, int sIndex)
        {
            for (int i = 0; i < socks.Count; i++)
            {
                if (socks[i].Index == sIndex) return i;
            }
            return -1;
        }

        private void Remove(ObservableCollection<ECActionSocket> socks, int sIndex)
        {
            int i = IndexOf(socks, sIndex);
            if (i >= 0)
            {  // don't do anything unless it's actually removed
                ECActionSocket s = socks[i];
                // update the collection
                socks.RemoveAt(i);
                // always update the Socket.Index
                for (int j = 0; j < socks.Count; j++) socks[j].Index = j;
                // notify about the change
                RaisePropertyChanged("RemovedSocket", s, null);
            }
        }

        // Property change for undo/redo:
        // We treat adding and removing socket as property changes, and
        // there are settable properties of Socket for each socket to handle.
        public override void ChangeDataValue(ModelChangedEventArgs e, bool undo)
        {
            // Data might be either a Unit or a Socket
            ECActionSocket sock = e.Data as ECActionSocket;
            if (sock != null)
            {  // if it's a Socket, let it handle undo/redo changes
                sock.ChangeDataValue(e, undo);
            }
            else
            {
                // assume we're dealing with a change to this Unit
                switch (e.PropertyName)
                {
                    case "AddedSocket":
                        sock = e.OldValue as ECActionSocket;
                        if (undo)
                            RemoveSocket(sock);
                        else
                            InsertSocket(sock);
                        break;
                    case "RemovedSocket":
                        sock = e.OldValue as ECActionSocket;
                        if (undo)
                            InsertSocket(sock);
                        else
                            RemoveSocket(sock);
                        break;
                    // if you add undo-able properties to Unit, handle them here
                    default:
                        base.ChangeDataValue(e, undo);
                        break;
                }
            }
        }
    }

    [Serializable]
    public class ECActionSocket : ICloneable, INotifyPropertyChanged, IChangeDataValue
    {
        // implement ICloneable for copying
        public Object Clone()
        {
            return MemberwiseClone() as Socket;
        }

        // implement INotifyPropertyChanged for data-binding
        [field: NonSerializedAttribute()]
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(String pname, Object oldval, Object newval)
        {
            ModelChangedEventArgs e = new ModelChangedEventArgs(pname, this, oldval, newval);
            // implement INotifyPropertyChanged:
            if (this.PropertyChanged != null) this.PropertyChanged(this, e);
            // implement support for model and undo/redo:
            if (this.Unit != null) this.Unit.OnPropertyChanged(e);
        }

        public XElement MakeXElement()
        {
            XElement e = new XElement("ECAction");
            e.Add(XHelper.Attribute("Algorithm", this.Algorithm, ""));
            e.Add(XHelper.Attribute("Output", this.Output, ""));
            e.Add(XHelper.Attribute("Index", this.Index, 0));
            return e;
        }

        public ECActionSocket LoadFromXElement(XElement e)
        {
            if (e == null) return this;
            this.Algorithm = XHelper.Read("Algorithm", e, "");
            this.Output = XHelper.Read("Output", e, "");
            this.Index = XHelper.Read("Index", e, 0);
            this.EOList = new List<string>();
            this.ALGList = new List<string>();
            return this;
        }

        // these properties aren't expected to change after initialization
        public ECState Unit { get; set; }  // parent pointer
        public String Algorithm { get; set; }
        public String Output { get; set; }
        public int Index { get; set; }

        public List<string> ALGList
        {
            get { return _ALGList; }
            set
            {
                if (_ALGList != value)
                {
                    List<string> old = _ALGList;
                    _ALGList = value;
                    RaisePropertyChanged("ALGList", old, value);
                }
            }
        }
        private List<string> _ALGList = new List<string>();

        public List<string> EOList
        {
            get { return _EOList; }
            set
            {
                if (_EOList != value)
                {
                    List<String> old = _EOList;
                    _EOList = value;
                    RaisePropertyChanged("EOList", old, value);
                }
            }
        }
        private List<string> _EOList = new List<string>();

        public String Ref
        {
            get { return _Ref; }
            set
            {
                if (_Ref != value)
                {
                    String old = _Ref;
                    _Ref = value;
                    RaisePropertyChanged("Ref", old, value);
                }
            }
        }
        private String _Ref = "";

        // these property may change dynamically, so they implement change notification

        // implement IChangeDataValue for undo/redo
        public void ChangeDataValue(ModelChangedEventArgs e, bool undo)
        {
            switch (e.PropertyName)
            {
                default: throw new NotImplementedException("Var change: " + e.ToString());
            }
        }
    }

    // the data for each link
    [Serializable]
    public class ECTransition : GraphLinksModelLinkData<String, String>
    {
        public string Comment { get; set; }

        public ECTransition()
        {
            this.Text = "1";
            this.Comment = "";
        }

        // this property remembers the curviness;
        // Double.NaN means let it use a default calculated value
        public double Curviness
        {
            get { return _Curviness; }
            set
            {
                if (_Curviness != value)
                {
                    double old = _Curviness;
                    _Curviness = value;
                    RaisePropertyChanged("Curviness", old, value);
                }
            }
        }

        // default value of NaN causes Route to calculate it
        private double _Curviness = Double.NaN;

        public Point Offset
        {
            get { return _Offset; }
            set
            {
                if (_Offset != value)
                {
                    Point old = _Offset;
                    _Offset = value;
                    RaisePropertyChanged("Offset", old, value);
                }
            }
        }
        private Point _Offset = new Point(0, 0);

        // write the extra property on the link data
        public override XElement MakeXElement(XName n)
        {
            XElement e = base.MakeXElement(n);
            e.Add(XHelper.Attribute("Curviness", this.Curviness, Double.NaN));
            e.Add(XHelper.Attribute("Offset", this.Offset, new Point(0, 0)));
            return e;
        }

        // read the extra property on the link data
        public override void LoadFromXElement(XElement e)
        {
            base.LoadFromXElement(e);
            this.Curviness = XHelper.Read("Curviness", e, Double.NaN);
            this.Offset = XHelper.Read("Offset", e, new Point(0, 0));
        }
    }


    // This tool only works when a Link has a LinkPanel with a single child element
    // that is positioned at the Route.MidPoint plus some Offset.
    public class SimpleLabelDraggingTool : DiagramTool
    {
        public override bool CanStart()
        {
            if (!base.CanStart()) return false;
            Diagram diagram = this.Diagram;
            if (diagram == null) return false;
            // require left button & that it has moved far enough away from the mouse down point, so it isn't a click
            if (!IsLeftButtonDown()) return false;
            if (!IsBeyondDragSize()) return false;
            return FindLabel() != null;
        }

        private FrameworkElement FindLabel()
        {

            var elt = this.Diagram.Panel.FindElementAt<System.Windows.Media.Visual>(this.Diagram.LastMousePointInModel, e => e, null, SearchLayers.Links);
            if (elt == null) return null;
            Link link = Part.FindAncestor<Link>(elt);
            if (link == null) return null;
            var parent = System.Windows.Media.VisualTreeHelper.GetParent(elt) as System.Windows.Media.Visual;
            while (parent != null && parent != link && !(parent is LinkPanel))
            {
                elt = parent;
                parent = System.Windows.Media.VisualTreeHelper.GetParent(elt) as System.Windows.Media.Visual;
            }

            if (parent is LinkPanel)
            {
                FrameworkElement lab = elt as FrameworkElement;
                if (lab == null) return null;
                // needs to be positioned relative to the MidPoint
                if (LinkPanel.GetIndex(lab) != Int32.MinValue) return null;
                // also check for movable-ness?
                return lab;
            }
            return null;
        }

        public override void DoActivate()
        {
            StartTransaction("Shifted Label");
            this.Label = FindLabel();
            if (this.Label != null)
            {
                this.OriginalOffset = LinkPanel.GetOffset(this.Label);
            }
            base.DoActivate();
        }

        public override void DoDeactivate()
        {
            base.DoDeactivate();
            StopTransaction();
        }

        private FrameworkElement Label { get; set; }
        private Point OriginalOffset { get; set; }

        public override void DoStop()
        {
            this.Label = null;
            base.DoStop();
        }

        public override void DoCancel()
        {
            if (this.Label != null)
            {
                LinkPanel.SetOffset(this.Label, this.OriginalOffset);
            }
            base.DoCancel();
        }

        public override void DoMouseMove()
        {
            if (!this.Active) return;
            UpdateLinkPanelProperties();
        }

        public override void DoMouseUp()
        {
            if (!this.Active) return;
            UpdateLinkPanelProperties();
            this.TransactionResult = "Shifted Label";
            StopTool();
        }

        private void UpdateLinkPanelProperties()
        {
            if (this.Label == null) return;
            Link link = Part.FindAncestor<Link>(this.Label);
            if (link == null) return;
            Point last = this.Diagram.LastMousePointInModel;
            Point mid = link.Route.MidPoint;
            // need to rotate this point to account for angle of middle segment
            Point p = new Point(last.X - mid.X, last.Y - mid.Y);
            LinkPanel.SetOffset(this.Label, RotatePoint(p, -link.Route.MidAngle));
        }

        private static Point RotatePoint(Point p, double angle)
        {
            if (angle == 0 || (p.X == 0 && p.Y == 0))
                return p;
            double rad = angle * Math.PI / 180;
            double cosine = Math.Cos(rad);
            double sine = Math.Sin(rad);
            return new Point((cosine * p.X - sine * p.Y),
                             (sine * p.X + cosine * p.Y));
        }
    }
}
