<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE AdapterType SYSTEM "http://www.holobloc.com/xml/LibraryElement.dtd" >
<AdapterType Name="AdapterType" Comment="Adapter Interface" >
  <CompilerInfo />
  <Identification Standard="61499-1" Classification="" ApplicationDomain="" Function="" Type="" Description="" />
  <VersionInfo Organization="DAL Automation" Version="0.1" Author="WD" Date="2014-04-29" Remarks="" />
  <InterfaceList>
    <EventInputs>
      <Event Name="REQ" Comment="Request from Socket" >
        <With Var="REQD" />
      </Event>
      <Event Name="RSP" Comment="Response from Socket" >
        <With Var="RSPD" />
      </Event>
    </EventInputs>
    <EventOutputs>
      <Event Name="CNF" Comment="Confirmation from Plug" >
        <With Var="CNFD" />
      </Event>
      <Event Name="IND" Comment="Indication from Plug" >
        <With Var="INDD" />
      </Event>
    </EventOutputs>
    <InputVars>
      <VarDeclaration Name="REQD" Type="WSTRING" Comment="Request Data from Socket" />
      <VarDeclaration Name="RSPD" Type="WSTRING" Comment="Response Data from Socket" />
    </InputVars>
    <OutputVars>
      <VarDeclaration Name="CNFD" Type="WSTRING" Comment="Confirmation Data from Plug" />
      <VarDeclaration Name="INDD" Type="WSTRING" Comment="Indication Data from Plug" />
    </OutputVars>
  </InterfaceList>
</AdapterType>
