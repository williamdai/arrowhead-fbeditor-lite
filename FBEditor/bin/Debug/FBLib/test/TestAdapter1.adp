﻿<?xml version="1.0" encoding="utf-8"?>
<AdapterType Name="TestAdapter1" Comment="Adapter Interface">
  <CompilerInfo />
  <Identification Standard="61499-1" Classification="" ApplicationDomain="" Function="" Type="" Description="" />
  <VersionInfo Organization="DAL Automation" Version="0.1" Author="WD" Date="2014-04-29" Remarks="" />
  <InterfaceList>
    <EventInputs>
      <Event Name="REQ" Type="EVENT" Comment="Request from Socket">
        <With Var="REQD" />
        <With Var="DI1" />
      </Event>
      <Event Name="RSP" Type="EVENT" Comment="Response from Socket">
        <With Var="RSPD" />
      </Event>
    </EventInputs>
    <EventOutputs>
      <Event Name="CNF" Type="EVENT" Comment="Confirmation from Plug">
        <With Var="CNFD" />
      </Event>
      <Event Name="IND" Type="EVENT" Comment="Indication from Plug">
        <With Var="INDD" />
      </Event>
      <Event Name="EO1" Type="EVENT" Comment="">
        <With Var="DO1" />
      </Event>
    </EventOutputs>
    <InputVars>
      <VarDeclaration Name="REQD" Type="WSTRING" ArraySize="" InitialValue="" Comment="Request Data from Socket" />
      <VarDeclaration Name="RSPD" Type="WSTRING" ArraySize="" InitialValue="" Comment="Response Data from Socket" />
      <VarDeclaration Name="DI1" Type="BOOL" ArraySize="" InitialValue="" Comment="" />
    </InputVars>
    <OutputVars>
      <VarDeclaration Name="CNFD" Type="WSTRING" ArraySize="" InitialValue="" Comment="Confirmation Data from Plug" />
      <VarDeclaration Name="INDD" Type="WSTRING" ArraySize="" InitialValue="" Comment="Indication Data from Plug" />
      <VarDeclaration Name="DO1" Type="BOOL" ArraySize="" InitialValue="" Comment="" />
    </OutputVars>
  </InterfaceList>
</AdapterType>