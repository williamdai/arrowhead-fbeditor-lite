﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Globalization;
using System.ServiceModel;

namespace IDELibrary
{
    public class FBXMLConverter
    {
        public const string eventColor = "#000000";
        public const string dataColor = "#000000";
        public const string adpColor = "#000000";

        public XElement generateFBTFromSCApplication(XElement sDiagramData)
        {
            XElement xFBTFile = new XElement("SubAppNetwork");
            XElement xEventConn = new XElement("EventConnections");
            XElement xDataConn = new XElement("DataConnections");
            XElement xADPConn = new XElement("AdapterConnections");

            //Get All FBs
            foreach (XElement xNode in sDiagramData.Elements("Node").Where(item => item.Attribute("Category").Value == "FBTemplate"))
            {
                string sNodeType = "SubApp";
                if (searchFileByName("FBLib", xNode.Attribute("Type").Value + ".fbt") != "")
                {
                    sNodeType = "FB";
                }
                XElement xNewFB = new XElement(sNodeType,
                    new XAttribute("Name", xNode.Attribute("Instance").Value),
                    new XAttribute("Type", xNode.Attribute("Type").Value),
                    new XAttribute("x", xNode.Attribute("Location").Value.Split(' ')[0]),
                    new XAttribute("y", xNode.Attribute("Location").Value.Split(' ')[1])
                    );

                //Get All Parameters for this FB
                if (sNodeType == "FB")
                {
                    foreach (XElement xDIPConn in sDiagramData.Elements("Conn").Where(item => item.Attribute("To").Value == xNode.Attribute("Key").Value))
                    {
                        XElement xDIP = sDiagramData.Elements("Node").Where(item => item.Attribute("Key").Value == xDIPConn.Attribute("From").Value && item.Attribute("Category").Value == "DIPTemplate").FirstOrDefault();
                        if (xDIP != null)
                        {
                            XElement xPara = new XElement("Parameter",
                                            new XAttribute("Name", xDIPConn.Attribute("ToPort").Value),
                                            new XAttribute("Value", xDIP.Attribute("Instance").Value));
                            xNewFB.Add(xPara);
                        }
                    }
                }

                xFBTFile.Add(xNewFB);

                //Get All Data and Event Connections
                //From this node
                foreach (XElement xFrom in sDiagramData.Elements("Conn").Where(item => item.Attribute("From").Value == xNode.Attribute("Key").Value))
                {
                    string sFrom = xNode.Attribute("Instance").Value;
                    string sFromPort = xFrom.Attribute("FromPort").Value; //EO or DO
                    string sTo = xFrom.Attribute("To").Value;
                    string sToPort = xFrom.Attribute("ToPort").Value;
                    string sConnType = xFrom.Attribute("ConnType").Value;
                    XElement xConn = new XElement("Connection");

                    XAttribute xSource = new XAttribute("Source", sFrom + "." + sFromPort);
                    xConn.Add(xSource);

                    XElement xToNode = sDiagramData.Elements("Node").Where(item => item.Attribute("Key").Value == sTo).FirstOrDefault();
                    string sToType = xToNode.Attribute("Category").Value;
                    if (sToType == "FBTemplate")
                    {
                        XAttribute xDest = new XAttribute("Destination", xToNode.Attribute("Instance").Value + "." + sToPort);
                        int dx1 = 0, dx2 = 0, dy = 0;
                        try
                        {
                            int fx = Convert.ToInt32(xNode.Attribute("Location").Value.Split(' ')[0]);
                            int fy = Convert.ToInt32(xNode.Attribute("Location").Value.Split(' ')[1]);
                            int tx = Convert.ToInt32(xToNode.Attribute("Location").Value.Split(' ')[0]);
                            int ty = Convert.ToInt32(xToNode.Attribute("Location").Value.Split(' ')[1]);
                            dx1 = (tx - fx) / 2;
                            dx2 = dx1;
                            dy = ty - tx;
                        }
                        catch (Exception)
                        { }
                        XAttribute xdx1 = new XAttribute("dx1", dx1.ToString());
                        XAttribute xdx2 = new XAttribute("dx2", dx2.ToString());
                        XAttribute xdy = new XAttribute("dy", dy.ToString());
                        xConn.Add(xDest);
                        xConn.Add(xdx1);
                        xConn.Add(xdx2);
                        xConn.Add(xdy);
                    }
                    if (sConnType == "EventConn")
                    {
                        xEventConn.Add(xConn);
                    }
                    else if (sConnType == "DataConn")
                    {
                        xDataConn.Add(xConn);
                    }
                    else if (sConnType == "AdapterConn")
                    {
                        xADPConn.Add(xConn);
                    }
                }
            }

            xFBTFile.Add(xEventConn, xDataConn, xADPConn);
            
            return xFBTFile;
        }

        public XElement generateAPPFromFBDiagram(XElement sDiagramData)
        {
            XElement xFBTFile = new XElement("SubAppNetwork");
            XElement xEventConn = new XElement("EventConnections");
            XElement xDataConn = new XElement("DataConnections");
            XElement xADPConn = new XElement("AdapterConnections");

            //Get All FBs
            foreach (XElement xNode in sDiagramData.Elements("Node").Where(item => item.Attribute("Category").Value == "FBTemplate"))
            {
                string sNodeType = "SubApp";
                if (searchFileByName("FBLib", xNode.Attribute("Type").Value + ".fbt") != "")
                {
                    sNodeType = "FB";
                }
                XElement xNewFB = new XElement(sNodeType,
                    new XAttribute("Name", xNode.Attribute("Instance").Value),
                    new XAttribute("Type", xNode.Attribute("Type").Value),
                    new XAttribute("x", xNode.Attribute("Location").Value.Split(' ')[0]),
                    new XAttribute("y", xNode.Attribute("Location").Value.Split(' ')[1])
                    );

                //Get All Parameters for this FB
                if (sNodeType == "FB")
                {
                    foreach (XElement xDIPConn in sDiagramData.Elements("Conn").Where(item => item.Attribute("To").Value == xNode.Attribute("Key").Value))
                    {
                        XElement xDIP = sDiagramData.Elements("Node").Where(item => item.Attribute("Key").Value == xDIPConn.Attribute("From").Value && item.Attribute("Category").Value == "DIPTemplate").FirstOrDefault();
                        if (xDIP != null)
                        {
                            XElement xPara = new XElement("Parameter",
                                            new XAttribute("Name", xDIPConn.Attribute("ToPort").Value),
                                            new XAttribute("Value", xDIP.Attribute("Instance").Value));
                            xNewFB.Add(xPara);
                        }
                    }
                }

                xFBTFile.Add(xNewFB);

                //Get All Data and Event Connections
                //From this node
                foreach (XElement xFrom in sDiagramData.Elements("Conn").Where(item => item.Attribute("From").Value == xNode.Attribute("Key").Value))
                {
                    string sFrom = xNode.Attribute("Instance").Value;
                    string sFromPort = xFrom.Attribute("FromPort").Value; //EO or DO
                    string sTo = xFrom.Attribute("To").Value;
                    string sToPort = xFrom.Attribute("ToPort").Value;
                    string sConnType = xFrom.Attribute("ConnType").Value;
                    XElement xConn = new XElement("Connection");

                    XAttribute xSource = new XAttribute("Source", sFrom + "." + sFromPort);
                    xConn.Add(xSource);

                    XElement xToNode = sDiagramData.Elements("Node").Where(item => item.Attribute("Key").Value == sTo).FirstOrDefault();
                    string sToType = xToNode.Attribute("Category").Value;
                    if ((sConnType == "EventConn" && sToType == "EOTemplate") || (sConnType == "DataConn" && sToType == "DOTemplate"))
                    {
                        XAttribute xDest = new XAttribute("Destination", sToPort);
                        XAttribute xdx1 = new XAttribute("dx1", "40");
                        xConn.Add(xDest);
                        xConn.Add(xdx1);
                    }
                    else if (sToType == "FBTemplate")
                    {
                        XAttribute xDest = new XAttribute("Destination", xToNode.Attribute("Instance").Value + "." + sToPort);
                        int dx1 = 0, dx2 = 0, dy = 0;
                        try
                        {
                            int fx = Convert.ToInt32(xNode.Attribute("Location").Value.Split(' ')[0]);
                            int fy = Convert.ToInt32(xNode.Attribute("Location").Value.Split(' ')[1]);
                            int tx = Convert.ToInt32(xToNode.Attribute("Location").Value.Split(' ')[0]);
                            int ty = Convert.ToInt32(xToNode.Attribute("Location").Value.Split(' ')[1]);
                            dx1 = (tx - fx) / 2;
                            dx2 = dx1;
                            dy = ty - tx;
                        }
                        catch (Exception)
                        { }
                        XAttribute xdx1 = new XAttribute("dx1", dx1.ToString());
                        XAttribute xdx2 = new XAttribute("dx2", dx2.ToString());
                        XAttribute xdy = new XAttribute("dy", dy.ToString());
                        xConn.Add(xDest);
                        xConn.Add(xdx1);
                        xConn.Add(xdx2);
                        xConn.Add(xdy);
                    }
                    if (sConnType == "EventConn")
                    {
                        xEventConn.Add(xConn);
                    }
                    else if (sConnType == "DataConn")
                    {
                        xDataConn.Add(xConn);
                    }
                    else if (sConnType == "AdapterConn")
                    {
                        xADPConn.Add(xConn);
                    }
                }
                //To This Node
                foreach (XElement xTo in sDiagramData.Elements("Conn").Where(item => item.Attribute("To").Value == xNode.Attribute("Key").Value))
                {
                    string sFrom = xTo.Attribute("From").Value;
                    string sFromPort = xTo.Attribute("FromPort").Value;
                    string sTo = xNode.Attribute("Instance").Value;
                    string sToPort = xTo.Attribute("ToPort").Value; //EI or DI
                    string sConnType = xTo.Attribute("ConnType").Value;
                    XElement xConn = new XElement("Connection");

                    XElement xFromNode = sDiagramData.Elements("Node").Where(item => item.Attribute("Key").Value == sFrom).FirstOrDefault();
                    string sFromType = xFromNode.Attribute("Category").Value;
                    if ((sConnType == "EventConn" && sFromType == "EITemplate") || (sConnType == "DataConn" && sFromType == "DITemplate"))
                    {
                        XAttribute xSource = new XAttribute("Source", sFromPort);
                        XAttribute xdx1 = new XAttribute("dx1", "20");
                        xConn.Add(xSource);
                        XAttribute xDest = new XAttribute("Destination", sTo + "." + sToPort);
                        xConn.Add(xDest);
                        xConn.Add(xdx1);
                        if (sConnType == "EventConn")
                        {
                            xEventConn.Add(xConn);
                        }
                        else if (sConnType == "DataConn")
                        {
                            xDataConn.Add(xConn);
                        }
                    }
                }
            }

            xFBTFile.Add(xEventConn, xDataConn, xADPConn);

            return xFBTFile;
        }


        public XElement generateFBTFromFBDiagram(XElement sDiagramData)
        {
            XElement xFBTFile = new XElement("FBNetwork");
            XElement xEventConn = new XElement("EventConnections");
            XElement xDataConn = new XElement("DataConnections");
            XElement xADPConn = new XElement("AdapterConnections");
                
            //Get All FBs
            foreach (XElement xNode in sDiagramData.Elements("Node").Where(item => item.Attribute("Category").Value == "FBTemplate"))
            {
                if (searchFileByName("FBLib", xNode.Attribute("Type").Value + ".fbt") == "") continue;

                XElement xNewFB = new XElement("FB",
                    new XAttribute("Name", xNode.Attribute("Instance").Value),
                    new XAttribute("Type", xNode.Attribute("Type").Value),
                    new XAttribute("x", xNode.Attribute("Location").Value.Split(' ')[0]),
                    new XAttribute("y", xNode.Attribute("Location").Value.Split(' ')[1])
                    );

                //Get All Parameters for this FB
                foreach (XElement xDIPConn in sDiagramData.Elements("Conn").Where(item => item.Attribute("To").Value == xNode.Attribute("Key").Value))
                {
                    XElement xDIP = sDiagramData.Elements("Node").Where(item => item.Attribute("Key").Value == xDIPConn.Attribute("From").Value && item.Attribute("Category").Value == "DIPTemplate").FirstOrDefault();
                    if (xDIP != null)
                    {
                        XElement xPara = new XElement("Parameter",
                                        new XAttribute("Name", xDIPConn.Attribute("ToPort").Value),
                                        new XAttribute("Value", xDIP.Attribute("Instance").Value));
                        xNewFB.Add(xPara);
                    }
                }
               
                xFBTFile.Add(xNewFB);

                //Get All Data and Event Connections
                //From this node
                foreach (XElement xFrom in sDiagramData.Elements("Conn").Where(item => item.Attribute("From").Value == xNode.Attribute("Key").Value))
                {
                    string sFrom = xNode.Attribute("Instance").Value;
                    string sFromPort = xFrom.Attribute("FromPort").Value; //EO or DO
                    string sTo = xFrom.Attribute("To").Value;
                    string sToPort = xFrom.Attribute("ToPort").Value;
                    string sConnType = xFrom.Attribute("ConnType").Value;
                    XElement xConn = new XElement("Connection");

                    XAttribute xSource = new XAttribute("Source", sFrom + "." + sFromPort);
                    xConn.Add(xSource);

                    XElement xToNode = sDiagramData.Elements("Node").Where(item => item.Attribute("Key").Value == sTo).FirstOrDefault();
                    string sToType = xToNode.Attribute("Category").Value;
                    if ((sConnType == "EventConn" && sToType == "EOTemplate") || (sConnType == "DataConn" && sToType == "DOTemplate"))
                    {
                        XAttribute xDest = new XAttribute("Destination", sToPort);
                        XAttribute xdx1 = new XAttribute("dx1", "40");
                        xConn.Add(xDest);
                        xConn.Add(xdx1);
                    }
                    else if (sToType == "FBTemplate")
                    {
                        XAttribute xDest = new XAttribute("Destination", xToNode.Attribute("Instance").Value + "." + sToPort);
                        int dx1 = 0, dx2 = 0, dy = 0;
                        try
                        {
                            int fx = Convert.ToInt32(xNode.Attribute("Location").Value.Split(' ')[0]);
                            int fy = Convert.ToInt32(xNode.Attribute("Location").Value.Split(' ')[1]);
                            int tx = Convert.ToInt32(xToNode.Attribute("Location").Value.Split(' ')[0]);
                            int ty = Convert.ToInt32(xToNode.Attribute("Location").Value.Split(' ')[1]);
                            dx1 = (tx - fx) / 2;
                            dx2 = dx1;
                            dy = ty - tx;
                        }
                        catch (Exception)
                        { }
                        XAttribute xdx1 = new XAttribute("dx1", dx1.ToString());
                        XAttribute xdx2 = new XAttribute("dx2", dx2.ToString());
                        XAttribute xdy = new XAttribute("dy", dy.ToString());
                        xConn.Add(xDest);
                        xConn.Add(xdx1);
                        xConn.Add(xdx2);
                        xConn.Add(xdy);
                    }
                    if (sConnType == "EventConn")
                    {
                        xEventConn.Add(xConn);
                    }
                    else if (sConnType == "DataConn")
                    {
                        xDataConn.Add(xConn);
                    }
                    else if (sConnType == "AdapterConn")
                    {
                        xADPConn.Add(xConn);
                    }
                }
                //To This Node
                foreach (XElement xTo in sDiagramData.Elements("Conn").Where(item => item.Attribute("To").Value == xNode.Attribute("Key").Value))
                {
                    string sFrom = xTo.Attribute("From").Value;
                    string sFromPort = xTo.Attribute("FromPort").Value; 
                    string sTo = xNode.Attribute("Instance").Value;
                    string sToPort = xTo.Attribute("ToPort").Value; //EI or DI
                    string sConnType = xTo.Attribute("ConnType").Value;
                    XElement xConn = new XElement("Connection");

                    XElement xFromNode = sDiagramData.Elements("Node").Where(item => item.Attribute("Key").Value == sFrom).FirstOrDefault();
                    string sFromType = xFromNode.Attribute("Category").Value;
                    if ((sConnType == "EventConn" && sFromType == "EITemplate") || (sConnType == "DataConn" && sFromType == "DITemplate"))
                    {
                        XAttribute xSource = new XAttribute("Source", sFromPort);
                        XAttribute xdx1 = new XAttribute("dx1", "20");
                        xConn.Add(xSource);
                        XAttribute xDest = new XAttribute("Destination", sTo + "." + sToPort);
                        xConn.Add(xDest);
                        xConn.Add(xdx1);
                        if (sConnType == "EventConn")
                        {
                            xEventConn.Add(xConn);
                        }
                        else if (sConnType == "DataConn")
                        {
                            xDataConn.Add(xConn);
                        }
                    } 
                }
            }

            xFBTFile.Add(xEventConn, xDataConn, xADPConn);

            return xFBTFile;
        }

        public XElement generateBFBECCFromXML(XElement xFBType)
        {
            XElement xDiagramData = new XElement("ECC");

            int iKey = 0;

            //Create all EC States and EC Actions
            bool bFirst = true;
            foreach (XElement xECState in xFBType.Descendants("ECState"))
            {
                XElement xNewECState = new XElement("ECState");
                createXElementAttr("Key", "ECS" + (iKey++).ToString(), ref xNewECState);
                createXElementAttr("Location", xECState.Attribute("x").Value + " " + xECState.Attribute("y").Value, ref xNewECState);
                createXElementAttr("Name", xECState.Attribute("Name").Value, ref xNewECState);
                string sComment = "";
                if (xECState.Attribute("Comment") != null)
                {
                    sComment = xECState.Attribute("Comment").Value;
                }
                createXElementAttr("Comment", sComment, ref xNewECState);
                createXElementAttr("Category", bFirst ? "InitStateTemplate" : "StateTemplate", ref xNewECState);

                int Index = 0;
                foreach (XElement xECAction in xECState.Elements())
                {
                    XAttribute xIndex = new XAttribute("Index", (Index++).ToString());
                    xECAction.Add(xIndex);
                    xNewECState.Add(xECAction);
                }

                bFirst = false;
                xDiagramData.Add(xNewECState);
            }

            foreach (XElement xECTrans in xFBType.Descendants("ECTransition"))
            {
                XElement xNewECTrans = new XElement("ECTransition");
                createXElementAttr("From", getDiagramKeyIDFromStateName(ref xDiagramData, xECTrans.Attribute("Source").Value), ref xNewECTrans);
                createXElementAttr("To", getDiagramKeyIDFromStateName(ref xDiagramData, xECTrans.Attribute("Destination").Value), ref xNewECTrans);
                createXElementAttr("Text", xECTrans.Attribute("Condition").Value, ref xNewECTrans);
                createXElementAttr("Location", xECTrans.Attribute("x").Value + " " + xECTrans.Attribute("y").Value, ref xNewECTrans);
                createXElementAttr("Curviness", "30", ref xNewECTrans);
                xDiagramData.Add(xNewECTrans);
            }

            return xDiagramData;
        }

        public XElement generateBFBECCFromFile(string sFile)
        {
            XElement xDiagramData = new XElement("ECC");

            if (File.Exists(sFile))
            {
                XDocument xDoc = XDocument.Load(sFile);
                int iKey = 0;
                    
                //Create all EC States and EC Actions
                bool bFirst = true;
                foreach (XElement xECState in xDoc.Root.Descendants("ECState"))
                {
                    XElement xNewECState = new XElement("ECState");
                    createXElementAttr("Key", "ECS" + (iKey++).ToString(), ref xNewECState);
                    createXElementAttr("Location", xECState.Attribute("x").Value + " " + xECState.Attribute("y").Value, ref xNewECState);
                    createXElementAttr("Name", xECState.Attribute("Name").Value, ref xNewECState);
                    string sComment = "";
                    if (xECState.Attribute("Comment") != null)
                    {
                        sComment = xECState.Attribute("Comment").Value;
                    }
                    createXElementAttr("Comment", sComment, ref xNewECState);
                    createXElementAttr("Category", bFirst ? "InitStateTemplate" : "StateTemplate", ref xNewECState);

                    int Index = 0;
                    foreach (XElement xECAction in xECState.Elements())
                    {
                        XAttribute xIndex = new XAttribute("Index", (Index++).ToString());
                        xECAction.Add(xIndex);
                        xNewECState.Add(xECAction);
                    }

                    bFirst = false;
                    xDiagramData.Add(xNewECState);
                }

                foreach (XElement xECTrans in xDoc.Root.Descendants("ECTransition"))
                {
                    XElement xNewECTrans = new XElement("ECTransition");
                    createXElementAttr("From", getDiagramKeyIDFromStateName(ref xDiagramData, xECTrans.Attribute("Source").Value), ref xNewECTrans);
                    createXElementAttr("To", getDiagramKeyIDFromStateName(ref xDiagramData, xECTrans.Attribute("Destination").Value), ref xNewECTrans);
                    createXElementAttr("Text", xECTrans.Attribute("Condition").Value, ref xNewECTrans);
                    createXElementAttr("Location", xECTrans.Attribute("x").Value + " " + xECTrans.Attribute("y").Value, ref xNewECTrans);
                    createXElementAttr("Curviness", "30", ref xNewECTrans);
                    xDiagramData.Add(xNewECTrans);
                }
            }

            return xDiagramData;
        }

        public XElement generateFBInterfaceFromXML(XElement xFBType)
        {
            XElement xDiagramData = new XElement("Diagram");

            //For Composite Function Block
            if (xFBType.Element("FBNetwork") != null)
            {
                generateFBInterfaceFromXElement(ref xDiagramData, xFBType, false, true);
            }
            else if (xFBType.Element("BasicFB") != null)
            {
                generateFBInterfaceFromXElement(ref xDiagramData, xFBType, true, false);
            }
            else if (xFBType.Element("Service") != null)
            {
                generateFBInterfaceFromXElement(ref xDiagramData, xFBType, true, false);
            }
            else if (xFBType.Element("SubAppNetwork") != null)
            {
                generateSubAppInterfaceFromXElement(ref xDiagramData, xFBType);
            }
            else if (xFBType.Name == "AdapterType")
            {
                generateFBInterfaceFromXElement(ref xDiagramData, xFBType, false, false);
            }
            
            return xDiagramData;
        }

        public XElement generateFBInterfaceFromFile(string sFile)
        {
            XElement xDiagramData = new XElement("Diagram");

            if (File.Exists(sFile))
            {
                XDocument xDoc = XDocument.Load(sFile);
                //For Composite Function Block
                if (sFile.ToLower().EndsWith(".fbt"))
                {
                    if (xDoc.Root.Element("FBNetwork") != null)
                    {
                        generateFBInterfaceFromXElement(ref xDiagramData, xDoc.Root, false, true);
                    }
                    else if (xDoc.Root.Element("BasicFB") != null)
                    {
                        generateFBInterfaceFromXElement(ref xDiagramData, xDoc.Root, true, false);
                    }
                    else if (xDoc.Root.Element("Service") != null)
                    {
                        generateFBInterfaceFromXElement(ref xDiagramData, xDoc.Root, true, false);
                    }
                }
                else if (sFile.ToLower().EndsWith(".app"))
                {
                    if (xDoc.Root.Element("SubAppNetwork") != null)
                    {
                        generateSubAppInterfaceFromXElement(ref xDiagramData, xDoc.Root);
                    }
                }
                else if (sFile.ToLower().EndsWith(".adp"))
                {
                    generateFBInterfaceFromXElement(ref xDiagramData, xDoc.Root, false, false);
                }
            }

            return xDiagramData;
        }

        public void generateSubAppInterfaceFromXElement(ref XElement xDiagramData, XElement xFBType)
        {
            XElement xNewFB = new XElement("Node");
            createXElementAttr("Key", xFBType.Attribute("Name").Value, ref xNewFB);
            createXElementAttr("Category", "FBTemplate", ref xNewFB);
            createXElementAttr("Location", "0 0", ref xNewFB);
            createXElementAttr("Instance", xFBType.Attribute("Name").Value, ref xNewFB);
            createXElementAttr("Type", xFBType.Attribute("Name").Value, ref xNewFB);

            int EIIndex = 0;
            int EOIndex = 0;
            int DIIndex = 0;
            int DOIndex = 0;

            int iTotalIVar = 0;
            int iTotalOVar = 0;

            try
            {
                foreach (XElement xIO in xFBType.Descendants("SubAppEventInputs").FirstOrDefault().Elements())
                {
                    createSubAppPort("SubAppEventInputs", xIO, ref EIIndex, ref xNewFB);
                    iTotalIVar++;
                }
            }
            catch (Exception)
            { }

            try
            {
                foreach (XElement xIO in xFBType.Descendants("SubAppEventOutputs").FirstOrDefault().Elements())
                {
                    createSubAppPort("SubAppEventOutputs", xIO, ref EOIndex, ref xNewFB);
                    iTotalOVar++;
                }
            }
            catch (Exception)
            { }

            try
            {
                foreach (XElement xIO in xFBType.Descendants("InputVars").FirstOrDefault().Elements())
                {
                    createSubAppPort("InputVars", xIO, ref DIIndex, ref xNewFB);
                    iTotalIVar++;
                }
            }
            catch (Exception)
            { }

            try
            {
                foreach (XElement xIO in xFBType.Descendants("OutputVars").FirstOrDefault().Elements())
                {
                    createSubAppPort("OutputVars", xIO, ref DOIndex, ref xNewFB);
                    iTotalOVar++;
                }
            }
            catch (Exception)
            { }

            xDiagramData.Add(xNewFB);
        }


        public void generateFBInterfaceFromXElement(ref XElement xDiagramData, XElement xFBType, bool bInternals, bool bADP)
        {
            XElement xNewFB = new XElement("Node");
            createXElementAttr("Key", xFBType.Attribute("Name").Value, ref xNewFB);
            createXElementAttr("Category", "FBTemplate", ref xNewFB);
            createXElementAttr("Location", "0 0", ref xNewFB);
            createXElementAttr("Instance", xFBType.Attribute("Name").Value, ref xNewFB);
            createXElementAttr("Type", xFBType.Attribute("Name").Value, ref xNewFB);

            int EIIndex = 0;
            int EOIndex = 0;
            int DIIndex = 0;
            int DOIndex = 0;
            int IVIndex = 0;
            int SOCKETIndex = 0;
            int PLUGIndex = 0;

            int iTotalIVar = 0;
            int iTotalOVar = 0;

            try
            {
                foreach (XElement xIO in xFBType.Descendants("EventInputs").FirstOrDefault().Elements())
                {
                    createPort("EventInputs", xIO, ref EIIndex, ref xNewFB);
                    iTotalIVar++;
                }
            }
            catch (Exception)
            { }

            try
            {
                foreach (XElement xIO in xFBType.Descendants("EventOutputs").FirstOrDefault().Elements())
                {
                    createPort("EventOutputs", xIO, ref EOIndex, ref xNewFB);
                    iTotalOVar++;
                }
            }
            catch (Exception)
            { }

            try
            {
                foreach (XElement xIO in xFBType.Descendants("InputVars").FirstOrDefault().Elements())
                {
                    createPort("InputVars", xIO, ref DIIndex, ref xNewFB);
                    iTotalIVar++;
                }
            }
            catch (Exception)
            { }

            try
            {
                foreach (XElement xIO in xFBType.Descendants("OutputVars").FirstOrDefault().Elements())
                {
                    createPort("OutputVars", xIO, ref DOIndex, ref xNewFB);
                    iTotalOVar++;
                }
            }
            catch (Exception)
            { }

            if (bInternals)
            {
                try
                {
                    foreach (XElement xIV in xFBType.Descendants("InternalVars").FirstOrDefault().Elements())
                    {
                        createPort("InternalVars", xIV, ref IVIndex, ref xNewFB);
                    }
                }
                catch (Exception)
                { }
            }

            if (bADP)
            {
                try
                {
                    foreach (XElement xIO in xFBType.Descendants("Sockets").FirstOrDefault().Elements())
                    {
                        createPort("Sockets", xIO, ref SOCKETIndex, ref xNewFB);
                        iTotalIVar++;
                    }
                }
                catch (Exception)
                { }

                try
                {
                    foreach (XElement xIO in xFBType.Descendants("Plugs").FirstOrDefault().Elements())
                    {
                        createPort("Plugs", xIO, ref PLUGIndex, ref xNewFB);
                        iTotalIVar++;
                    }
                }
                catch (Exception)
                { }
            }

            xDiagramData.Add(xNewFB);
        }

        private string searchFileByName(string sDIR, string sFile)
        {
            string sFilePath = "";
            //Find Source File in the Root Folder
            //Search through Library
            string[] fileList = Directory.GetFiles(sDIR, sFile);
            if (fileList.Count() > 0)
            {
                sFilePath = fileList[0];
            }
            else
            {
                foreach (string SubDIR in Directory.GetDirectories(sDIR))
                {
                    sFilePath = searchFileByName(SubDIR, sFile);
                    if (sFilePath != "")
                    {
                        break;
                    }
                    
                }
            }
            return sFilePath;
        }

        //For System Configuration Application
        public void generateSCApplicationFromXElement(ref XElement xDiagramData, XElement xFBType)
        {
            xDiagramData = new XElement("Diagram");

            int iKey = 0;
            int iDIPKey = 0;
            double iXMax = 0;
            double iYMax = 0;

            //Create all SubApplications
            foreach (XElement xFB in xFBType.Descendants("SubApp"))
            {
                double iX = 0;
                double iY = 0;

                XElement xNewSubApp = new XElement("Node");
                createXElementAttr("Key", "FBI" + (iKey++).ToString(), ref xNewSubApp);
                createXElementAttr("Category", "FBTemplate", ref xNewSubApp);
                createXElementAttr("Location", xFB.Attribute("x").Value + " " + xFB.Attribute("y").Value, ref xNewSubApp);
                try
                {
                    iX = Convert.ToDouble(xFB.Attribute("x").Value);
                    iY = Convert.ToDouble(xFB.Attribute("y").Value);
                    if (iX > iXMax) iXMax = iX;
                    if (iY > iYMax) iYMax = iY;
                }
                catch (Exception)
                { }
                createXElementAttr("Instance", xFB.Attribute("Name").Value, ref xNewSubApp);
                createXElementAttr("Type", xFB.Attribute("Type").Value, ref xNewSubApp);

                string sFilename = searchFileByName("FBLib", xFB.Attribute("Type").Value + ".app");
                //Will use the first one
                if (sFilename != "")
                {
                    int EIIndex = 0;
                    int EOIndex = 0;
                    int DIIndex = 0;
                    int DOIndex = 0;

                    XDocument xFBFile = XDocument.Load(sFilename);

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("SubAppEventInputs").FirstOrDefault().Elements())
                        {
                            createSubAppPort("SubAppEventInputs", xIO, ref EIIndex, ref xNewSubApp);
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("SubAppEventOutputs").FirstOrDefault().Elements())
                        {
                            createSubAppPort("SubAppEventOutputs", xIO, ref EOIndex, ref xNewSubApp);
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("InputVars").FirstOrDefault().Elements())
                        {
                            createSubAppPort("InputVars", xIO, ref DIIndex, ref xNewSubApp);
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("OutputVars").FirstOrDefault().Elements())
                        {
                            createSubAppPort("OutputVars", xIO, ref DOIndex, ref xNewSubApp);
                        }
                    }
                    catch (Exception)
                    { }
                }

                xDiagramData.Add(xNewSubApp);
            }

            //Create all FBs
            foreach (XElement xFB in xFBType.Descendants("FB"))
            {
                double iX = 0;
                double iY = 0;
                int iTotalVar = 0;

                XElement xNewFB = new XElement("Node");
                createXElementAttr("Key", "FBI" + (iKey++).ToString(), ref xNewFB);
                createXElementAttr("Category", "FBTemplate", ref xNewFB);
                createXElementAttr("Location", xFB.Attribute("x").Value + " " + xFB.Attribute("y").Value, ref xNewFB);
                try
                {
                    iX = Convert.ToDouble(xFB.Attribute("x").Value);
                    iY = Convert.ToDouble(xFB.Attribute("y").Value);
                    if (iX > iXMax) iXMax = iX;
                    if (iY > iYMax) iYMax = iY;
                }
                catch (Exception)
                { }
                createXElementAttr("Instance", xFB.Attribute("Name").Value, ref xNewFB);
                createXElementAttr("Type", xFB.Attribute("Type").Value, ref xNewFB);

                string sFilename = searchFileByName("FBLib", xFB.Attribute("Type").Value + ".fbt");
                if (sFilename != "")
                {
                    int EIIndex = 0;
                    int EOIndex = 0;
                    int DIIndex = 0;
                    int DOIndex = 0;
                    int SOCKETIndex = 0;
                    int PLUGIndex = 0;

                    int iTotalIVar = 0;
                    int iTotalOVar = 0;

                    XDocument xFBFile = XDocument.Load(sFilename);

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("EventInputs").FirstOrDefault().Elements())
                        {
                            createPort("EventInputs", xIO, ref EIIndex, ref xNewFB);
                            iTotalIVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("EventOutputs").FirstOrDefault().Elements())
                        {
                            createPort("EventOutputs", xIO, ref EOIndex, ref xNewFB);
                            iTotalOVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("InputVars").FirstOrDefault().Elements())
                        {
                            createPort("InputVars", xIO, ref DIIndex, ref xNewFB);
                            iTotalIVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("OutputVars").FirstOrDefault().Elements())
                        {
                            createPort("OutputVars", xIO, ref DOIndex, ref xNewFB);
                            iTotalOVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("Sockets").FirstOrDefault().Elements())
                        {
                            createPort("Sockets", xIO, ref SOCKETIndex, ref xNewFB);
                            iTotalIVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("Plugs").FirstOrDefault().Elements())
                        {
                            createPort("Plugs", xIO, ref PLUGIndex, ref xNewFB);
                            iTotalIVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    if (iTotalIVar >= iTotalOVar)
                    {
                        iTotalVar = iTotalIVar;
                    }
                    else
                    {
                        iTotalVar = iTotalOVar;
                    }
                }

                xDiagramData.Add(xNewFB);

                //create all parameters
                foreach (XElement xDIP in xFB.Descendants("Parameter"))
                {
                    XElement xNewDIP = new XElement("Node");
                    createXElementAttr("Key", "DIP" + (iDIPKey++).ToString(), ref xNewDIP);
                    createXElementAttr("Category", "DIPTemplate", ref xNewDIP);
                    createXElementAttr("Location", (iX - 100).ToString() + " " + (iY + iTotalVar * 10 + iDIPKey * 25).ToString(), ref xNewDIP);
                    createXElementAttr("Instance", xDIP.Attribute("Value").Value, ref xNewDIP);
                    createXElementAttr("Type", xFB.Attribute("Name").Value, ref xNewDIP);

                    XElement xNewDIPVar = new XElement("Var");
                    createXElementAttr("Name", xDIP.Attribute("Name").Value, ref xNewDIPVar);
                    createXElementAttr("Type", "DI", ref xNewDIPVar);
                    createXElementAttr("Index", "0", ref xNewDIPVar);
                    createXElementAttr("Color", dataColor, ref xNewDIPVar);

                    xNewDIP.Add(xNewDIPVar);
                    xDiagramData.Add(xNewDIP);
                }
            }

            //Create all connections
            foreach (XElement xConn in xFBType.Descendants("Connection"))
            {
                string sSourceID = "";
                string sDestID = "";
                XElement xNewConn = new XElement("Conn");
                if (xConn.Attribute("Source").Value.IndexOf(".") > 0)
                {
                    sSourceID = getDiagramInstanceIDFromName(ref xDiagramData, xConn.Attribute("Source").Value.Split('.')[0]);
                    createXElementAttr("From", sSourceID, ref xNewConn);
                    createXElementAttr("FromPort", xConn.Attribute("Source").Value.Split('.')[1], ref xNewConn);
                }
               
                if (xConn.Attribute("Destination").Value.IndexOf(".") > 0)
                {
                    sDestID = getDiagramInstanceIDFromName(ref xDiagramData, xConn.Attribute("Destination").Value.Split('.')[0]);
                    createXElementAttr("To", sDestID, ref xNewConn);
                    createXElementAttr("ToPort", xConn.Attribute("Destination").Value.Split('.')[1], ref xNewConn);
                }
               
                createXElementAttr("Points", "", ref xNewConn);
                string sConnType = "EventConn";
                if (xConn.Parent.Name == "DataConnections") sConnType = "DataConn";
                if (xConn.Parent.Name == "AdapterConnections") sConnType = "AdapterConn";
                createXElementAttr("ConnType", sConnType, ref xNewConn);

                if (sSourceID != "" && sDestID != "") xDiagramData.Add(xNewConn);
            }

            //Create All Parameter Connections
            foreach (XElement xPNode in xDiagramData.Descendants("Node").Where(item => item.Attribute("Category").Value == "DIPTemplate"))
            {
                XElement xNewConn = new XElement("Conn");

                string sDestID = getDiagramInstanceIDFromName(ref xDiagramData, xPNode.Attribute("Type").Value);

                createXElementAttr("From", xPNode.Attribute("Key").Value, ref xNewConn);
                //createXElementAttr("FromPort", xPNode.Element("Var").Attribute("Name").Value, ref xNewConn);
                createXElementAttr("FromPort", "DIP", ref xNewConn);
                createXElementAttr("To", sDestID, ref xNewConn);
                createXElementAttr("ToPort", xPNode.Element("Var").Attribute("Name").Value, ref xNewConn);
                xPNode.Element("Var").Attribute("Name").Value = "DIP";

                createXElementAttr("Points", "", ref xNewConn);
                createXElementAttr("ConnType", "DataConn", ref xNewConn);

                xDiagramData.Add(xNewConn);
            }
        }

        //For SubApplication
        public void generateSubAppDiagramFromXElement(ref XElement xDiagramData, XElement xFBType)
        {
            xDiagramData = new XElement("Diagram");

            int iKey = 0;
            int iDIPKey = 0;
            double iXMax = 0;
            double iYMax = 0;

            //Create all SubApps
            foreach (XElement xFB in xFBType.Descendants("SubApp"))
            {
                double iX = 0;
                double iY = 0;
                
                XElement xNewSubApp = new XElement("Node");
                createXElementAttr("Key", "FBI" + (iKey++).ToString(), ref xNewSubApp);
                createXElementAttr("Category", "FBTemplate", ref xNewSubApp);
                createXElementAttr("Location", xFB.Attribute("x").Value + " " + xFB.Attribute("y").Value, ref xNewSubApp);
                try
                {
                    iX = Convert.ToDouble(xFB.Attribute("x").Value);
                    iY = Convert.ToDouble(xFB.Attribute("y").Value);
                    if (iX > iXMax) iXMax = iX;
                    if (iY > iYMax) iYMax = iY;
                }
                catch (Exception)
                { }
                createXElementAttr("Instance", xFB.Attribute("Name").Value, ref xNewSubApp);
                createXElementAttr("Type", xFB.Attribute("Type").Value, ref xNewSubApp);

                string sFilename = searchFileByName("FBLib", xFB.Attribute("Type").Value + ".app");
                //Will use the first one
                //TODO - When save a file should check if existing file is stored in the library
                if (sFilename != "")
                {
                    int EIIndex = 0;
                    int EOIndex = 0;
                    int DIIndex = 0;
                    int DOIndex = 0;
                    
                    XDocument xFBFile = XDocument.Load(sFilename);

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("SubAppEventInputs").FirstOrDefault().Elements())
                        {
                            createSubAppPort("SubAppEventInputs", xIO, ref EIIndex, ref xNewSubApp);
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("SubAppEventOutputs").FirstOrDefault().Elements())
                        {
                            createSubAppPort("SubAppEventOutputs", xIO, ref EOIndex, ref xNewSubApp);
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("InputVars").FirstOrDefault().Elements())
                        {
                            createSubAppPort("InputVars", xIO, ref DIIndex, ref xNewSubApp);
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("OutputVars").FirstOrDefault().Elements())
                        {
                            createSubAppPort("OutputVars", xIO, ref DOIndex, ref xNewSubApp);
                        }
                    }
                    catch (Exception)
                    { }
                }

                xDiagramData.Add(xNewSubApp);
            }

            //Create all FBs
            foreach (XElement xFB in xFBType.Descendants("FB"))
            {
                double iX = 0;
                double iY = 0;
                int iTotalVar = 0;

                XElement xNewFB = new XElement("Node");
                createXElementAttr("Key", "FBI" + (iKey++).ToString(), ref xNewFB);
                createXElementAttr("Category", "FBTemplate", ref xNewFB);
                createXElementAttr("Location", xFB.Attribute("x").Value + " " + xFB.Attribute("y").Value, ref xNewFB);
                try
                {
                    iX = Convert.ToDouble(xFB.Attribute("x").Value);
                    iY = Convert.ToDouble(xFB.Attribute("y").Value);
                    if (iX > iXMax) iXMax = iX;
                    if (iY > iYMax) iYMax = iY;
                }
                catch (Exception)
                { }
                createXElementAttr("Instance", xFB.Attribute("Name").Value, ref xNewFB);
                createXElementAttr("Type", xFB.Attribute("Type").Value, ref xNewFB);

                string sFilename = searchFileByName("FBLib", xFB.Attribute("Type").Value + ".fbt");
                //Will use the first one
                //TODO - When save a file should check if existing file is stored in the library
                if (sFilename != "")
                {
                    int EIIndex = 0;
                    int EOIndex = 0;
                    int DIIndex = 0;
                    int DOIndex = 0;
                    int SOCKETIndex = 0;
                    int PLUGIndex = 0;

                    int iTotalIVar = 0;
                    int iTotalOVar = 0;

                    XDocument xFBFile = XDocument.Load(sFilename);

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("EventInputs").FirstOrDefault().Elements())
                        {
                            createPort("EventInputs", xIO, ref EIIndex, ref xNewFB);
                            iTotalIVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("EventOutputs").FirstOrDefault().Elements())
                        {
                            createPort("EventOutputs", xIO, ref EOIndex, ref xNewFB);
                            iTotalOVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("InputVars").FirstOrDefault().Elements())
                        {
                            createPort("InputVars", xIO, ref DIIndex, ref xNewFB);
                            iTotalIVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("OutputVars").FirstOrDefault().Elements())
                        {
                            createPort("OutputVars", xIO, ref DOIndex, ref xNewFB);
                            iTotalOVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("Sockets").FirstOrDefault().Elements())
                        {
                            createPort("Sockets", xIO, ref SOCKETIndex, ref xNewFB);
                            iTotalIVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("Plugs").FirstOrDefault().Elements())
                        {
                            createPort("Plugs", xIO, ref PLUGIndex, ref xNewFB);
                            iTotalIVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    if (iTotalIVar >= iTotalOVar)
                    {
                        iTotalVar = iTotalIVar;
                    }
                    else
                    {
                        iTotalVar = iTotalOVar;
                    }
                }

                xDiagramData.Add(xNewFB);

                //create all parameters
                foreach (XElement xDIP in xFB.Descendants("Parameter"))
                {
                    XElement xNewDIP = new XElement("Node");
                    createXElementAttr("Key", "DIP" + (iDIPKey++).ToString(), ref xNewDIP);
                    createXElementAttr("Category", "DIPTemplate", ref xNewDIP);
                    createXElementAttr("Location", (iX - 100).ToString() + " " + (iY + iTotalVar * 10 + iDIPKey * 25).ToString(), ref xNewDIP);
                    createXElementAttr("Instance", xDIP.Attribute("Value").Value, ref xNewDIP);
                    createXElementAttr("Type", xFB.Attribute("Name").Value, ref xNewDIP);

                    XElement xNewDIPVar = new XElement("Var");
                    createXElementAttr("Name", xDIP.Attribute("Name").Value, ref xNewDIPVar);
                    createXElementAttr("Type", "DI", ref xNewDIPVar);
                    createXElementAttr("Index", "0", ref xNewDIPVar);
                    createXElementAttr("Color", dataColor, ref xNewDIPVar);

                    xNewDIP.Add(xNewDIPVar);
                    xDiagramData.Add(xNewDIP);
                }
            }

            //create all external I/Os
            iKey = 0;
            try
            {
                foreach (XElement xEI in xFBType.Descendants("SubAppEventInputs").FirstOrDefault().Elements("SubAppEvent"))
                {
                    XElement xNewEI = new XElement("Node");
                    createXElementAttr("Key", "EI" + (iKey++).ToString(), ref xNewEI);
                    createXElementAttr("Category", "EITemplate", ref xNewEI);
                    createXElementAttr("Location", "5 " + (iKey * 25 + 25).ToString(), ref xNewEI);
                    createXElementAttr("Instance", xEI.Attribute("Name").Value, ref xNewEI);
                    createXElementAttr("Type", "EI", ref xNewEI);

                    XElement xNewEIVar = new XElement("Var");
                    createXElementAttr("Name", xEI.Attribute("Name").Value, ref xNewEIVar);
                    createXElementAttr("Type", "EI", ref xNewEIVar);
                    createXElementAttr("Index", "0", ref xNewEIVar);
                    createXElementAttr("Color", eventColor, ref xNewEIVar);

                    xNewEI.Add(xNewEIVar);
                    xDiagramData.Add(xNewEI);
                }
            }
            catch (Exception)
            { }

            iKey = 0;
            try
            {
                foreach (XElement xEO in xFBType.Descendants("SubAppEventOutputs").FirstOrDefault().Elements("SubAppEvent"))
                {
                    XElement xNewEO = new XElement("Node");
                    createXElementAttr("Key", "EO" + (iKey++).ToString(), ref xNewEO);
                    createXElementAttr("Category", "EOTemplate", ref xNewEO);
                    createXElementAttr("Location", (iXMax + 300).ToString() + " " + (iKey * 25 + 25).ToString(), ref xNewEO);
                    createXElementAttr("Instance", xEO.Attribute("Name").Value, ref xNewEO);
                    createXElementAttr("Type", "EO", ref xNewEO);

                    XElement xNewEOVar = new XElement("Var");
                    createXElementAttr("Name", xEO.Attribute("Name").Value, ref xNewEOVar);
                    createXElementAttr("Type", "EO", ref xNewEOVar);
                    createXElementAttr("Index", "0", ref xNewEOVar);
                    createXElementAttr("Color", eventColor, ref xNewEOVar);

                    xNewEO.Add(xNewEOVar);
                    xDiagramData.Add(xNewEO);
                }
            }
            catch (Exception)
            { }

            iKey = 0;
            try
            {
                foreach (XElement xDI in xFBType.Descendants("InputVars").FirstOrDefault().Elements("VarDeclaration"))
                {
                    XElement xNewDI = new XElement("Node");
                    createXElementAttr("Key", "DI" + (iKey++).ToString(), ref xNewDI);
                    createXElementAttr("Category", "DITemplate", ref xNewDI);
                    createXElementAttr("Location", "55 " + (iYMax + iKey * 25 + 100).ToString(), ref xNewDI);
                    createXElementAttr("Instance", xDI.Attribute("Name").Value, ref xNewDI);
                    createXElementAttr("Type", "DI", ref xNewDI);

                    XElement xNewDIVar = new XElement("Var");
                    createXElementAttr("Name", xDI.Attribute("Name").Value, ref xNewDIVar);
                    createXElementAttr("Type", "DI", ref xNewDIVar);
                    createXElementAttr("Index", "0", ref xNewDIVar);
                    createXElementAttr("Color", eventColor, ref xNewDIVar);

                    xNewDI.Add(xNewDIVar);
                    xDiagramData.Add(xNewDI);
                }
            }
            catch (Exception)
            { }

            iKey = 0;
            try
            {
                foreach (XElement xDO in xFBType.Descendants("OutputVars").FirstOrDefault().Elements("VarDeclaration"))
                {
                    XElement xNewDO = new XElement("Node");
                    createXElementAttr("Key", "DO" + (iKey++).ToString(), ref xNewDO);
                    createXElementAttr("Category", "DOTemplate", ref xNewDO);
                    createXElementAttr("Location", (iXMax + 300).ToString() + " " + (iYMax + iKey * 25 + 100).ToString(), ref xNewDO);
                    createXElementAttr("Instance", xDO.Attribute("Name").Value, ref xNewDO);
                    createXElementAttr("Type", "DO", ref xNewDO);

                    XElement xNewDOVar = new XElement("Var");
                    createXElementAttr("Name", xDO.Attribute("Name").Value, ref xNewDOVar);
                    createXElementAttr("Type", "DO", ref xNewDOVar);
                    createXElementAttr("Index", "0", ref xNewDOVar);
                    createXElementAttr("Color", eventColor, ref xNewDOVar);

                    xNewDO.Add(xNewDOVar);
                    xDiagramData.Add(xNewDO);
                }
            }
            catch (Exception)
            { }

            //Create all connections
            foreach (XElement xConn in xFBType.Descendants("Connection"))
            {
                string sSourceID = "";
                string sDestID = "";
                XElement xNewConn = new XElement("Conn");
                if (xConn.Attribute("Source").Value.IndexOf(".") > 0)
                {
                    sSourceID = getDiagramInstanceIDFromName(ref xDiagramData, xConn.Attribute("Source").Value.Split('.')[0]);
                    createXElementAttr("From", sSourceID, ref xNewConn);
                    createXElementAttr("FromPort", xConn.Attribute("Source").Value.Split('.')[1], ref xNewConn);
                }
                else
                {
                    sSourceID = getDiagramInstanceIDFromName(ref xDiagramData, xConn.Attribute("Source").Value);
                    createXElementAttr("From", sSourceID, ref xNewConn);
                    createXElementAttr("FromPort", xConn.Attribute("Source").Value, ref xNewConn);
                }

                if (xConn.Attribute("Destination").Value.IndexOf(".") > 0)
                {
                    sDestID = getDiagramInstanceIDFromName(ref xDiagramData, xConn.Attribute("Destination").Value.Split('.')[0]);
                    createXElementAttr("To", sDestID, ref xNewConn);
                    createXElementAttr("ToPort", xConn.Attribute("Destination").Value.Split('.')[1], ref xNewConn);
                }
                else
                {
                    sDestID = getDiagramInstanceIDFromName(ref xDiagramData, xConn.Attribute("Destination").Value);
                    createXElementAttr("To", sDestID, ref xNewConn);
                    createXElementAttr("ToPort", xConn.Attribute("Destination").Value, ref xNewConn);
                }

                createXElementAttr("Points", "", ref xNewConn);
                string sConnType = "EventConn";
                if (xConn.Parent.Name == "DataConnections") sConnType = "DataConn";
                if (xConn.Parent.Name == "AdapterConnections") sConnType = "AdapterConn";
                createXElementAttr("ConnType", sConnType, ref xNewConn);

                if (sSourceID != "" && sDestID != "") xDiagramData.Add(xNewConn);
            }

            //Create All Parameter Connections
            foreach (XElement xPNode in xDiagramData.Descendants("Node").Where(item => item.Attribute("Category").Value == "DIPTemplate"))
            {
                XElement xNewConn = new XElement("Conn");

                string sDestID = getDiagramInstanceIDFromName(ref xDiagramData, xPNode.Attribute("Type").Value);

                createXElementAttr("From", xPNode.Attribute("Key").Value, ref xNewConn);
                createXElementAttr("FromPort", "DIP", ref xNewConn);
                createXElementAttr("To", sDestID, ref xNewConn);
                createXElementAttr("ToPort", xPNode.Element("Var").Attribute("Name").Value, ref xNewConn);
                xPNode.Element("Var").Attribute("Name").Value = "DIP";

                createXElementAttr("Points", "", ref xNewConn);
                createXElementAttr("ConnType", "DataConn", ref xNewConn);

                xDiagramData.Add(xNewConn);
            }
        }


        //For Composite Function Block
        public void generateFBDiagramFromXElement(ref XElement xDiagramData, XElement xFBType)
        {
            xDiagramData = new XElement("Diagram");

            int iKey = 0;
            int iDIPKey = 0;
            double iXMax = 0;
            double iYMax = 0;
            
            //Create all FBs
            foreach (XElement xFB in xFBType.Descendants("FB"))
            {
                double iX = 0;
                double iY = 0;
                int iTotalVar = 0;
                
                XElement xNewFB = new XElement("Node");
                createXElementAttr("Key", "FBI" + (iKey++).ToString(), ref xNewFB);
                createXElementAttr("Category", "FBTemplate", ref xNewFB);
                createXElementAttr("Location", xFB.Attribute("x").Value + " " + xFB.Attribute("y").Value, ref xNewFB);
                try
                {
                    iX = Convert.ToDouble(xFB.Attribute("x").Value);
                    iY = Convert.ToDouble(xFB.Attribute("y").Value);
                    if (iX > iXMax) iXMax = iX;
                    if (iY > iYMax) iYMax = iY;
                }
                catch (Exception)
                { }
                createXElementAttr("Instance", xFB.Attribute("Name").Value, ref xNewFB);
                createXElementAttr("Type", xFB.Attribute("Type").Value, ref xNewFB);

                string sFilename = searchFileByName("FBLib", xFB.Attribute("Type").Value + ".fbt");
                //Will use the first one
                //TODO - When save a file should check if existing file is stored in the library
                if (sFilename != "")
                {
                    int EIIndex = 0;
                    int EOIndex = 0;
                    int DIIndex = 0;
                    int DOIndex = 0;
                    int SOCKETIndex = 0;
                    int PLUGIndex = 0;

                    int iTotalIVar = 0;
                    int iTotalOVar = 0;

                    XDocument xFBFile = XDocument.Load(sFilename);

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("EventInputs").FirstOrDefault().Elements())
                        {
                            createPort("EventInputs", xIO, ref EIIndex, ref xNewFB);
                            iTotalIVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("EventOutputs").FirstOrDefault().Elements())
                        {
                            createPort("EventOutputs", xIO, ref EOIndex, ref xNewFB);
                            iTotalOVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("InputVars").FirstOrDefault().Elements())
                        {
                            createPort("InputVars", xIO, ref DIIndex, ref xNewFB);
                            iTotalIVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("OutputVars").FirstOrDefault().Elements())
                        {
                            createPort("OutputVars", xIO, ref DOIndex, ref xNewFB);
                            iTotalOVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("Sockets").FirstOrDefault().Elements())
                        {
                            createPort("Sockets", xIO, ref SOCKETIndex, ref xNewFB);
                            iTotalIVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    try
                    {
                        foreach (XElement xIO in xFBFile.Root.Descendants("Plugs").FirstOrDefault().Elements())
                        {
                            createPort("Plugs", xIO, ref PLUGIndex, ref xNewFB);
                            iTotalIVar++;
                        }
                    }
                    catch (Exception)
                    { }

                    if (iTotalIVar >= iTotalOVar)
                    {
                        iTotalVar = iTotalIVar;
                    }
                    else
                    {
                        iTotalVar = iTotalOVar;
                    }
                }
                
                xDiagramData.Add(xNewFB);

                //create all parameters
                foreach (XElement xDIP in xFB.Descendants("Parameter"))
                {
                    XElement xNewDIP = new XElement("Node");
                    createXElementAttr("Key", "DIP" + (iDIPKey++).ToString(), ref xNewDIP);
                    createXElementAttr("Category", "DIPTemplate", ref xNewDIP);
                    createXElementAttr("Location", (iX - 100).ToString() + " " + (iY + iTotalVar * 10 + iDIPKey * 25).ToString(), ref xNewDIP);
                    createXElementAttr("Instance", xDIP.Attribute("Value").Value, ref xNewDIP);
                    createXElementAttr("Type", xFB.Attribute("Name").Value, ref xNewDIP);
                    
                    XElement xNewDIPVar = new XElement("Var");
                    createXElementAttr("Name", xDIP.Attribute("Name").Value, ref xNewDIPVar);
                    createXElementAttr("Type", "DI", ref xNewDIPVar);
                    createXElementAttr("Index", "0", ref xNewDIPVar);
                    createXElementAttr("Color", dataColor, ref xNewDIPVar);

                    xNewDIP.Add(xNewDIPVar);
                    xDiagramData.Add(xNewDIP);
                }
            }

            //Create all SOCKET FBs
            try
            {
                foreach (XElement xADP in xFBType.Descendants("Sockets").FirstOrDefault().Elements("AdapterDeclaration"))
                {
                    XElement xNewFB = new XElement("Node");
                    createXElementAttr("Key", "FBI" + (iKey++).ToString(), ref xNewFB);
                    createXElementAttr("Category", "FBTemplate", ref xNewFB);
                    createXElementAttr("Location", xADP.Attribute("x").Value + " " + xADP.Attribute("y").Value, ref xNewFB);
                    createXElementAttr("Instance", xADP.Attribute("Name").Value, ref xNewFB);
                    createXElementAttr("Type", xADP.Attribute("Type").Value, ref xNewFB);

                    string sFilename = searchFileByName("FBLib", xADP.Attribute("Type").Value + ".adp");
                    //Will use the first one
                    //TODO - When save a file should check if existing file is stored in the library
                    if (sFilename != "")
                    {
                        int EIIndex = 0;
                        int EOIndex = 0;
                        int DIIndex = 0;
                        int DOIndex = 0;
                        
                        XDocument xFBFile = XDocument.Load(sFilename);

                        try
                        {
                            foreach (XElement xIO in xFBFile.Root.Descendants("EventInputs").FirstOrDefault().Elements())
                            {
                                createPort("EventInputs", xIO, ref EIIndex, ref xNewFB);
                            }
                        }
                        catch (Exception)
                        { }

                        try
                        {
                            foreach (XElement xIO in xFBFile.Root.Descendants("EventOutputs").FirstOrDefault().Elements())
                            {
                                createPort("EventOutputs", xIO, ref EOIndex, ref xNewFB);
                            }
                        }
                        catch (Exception)
                        { }

                        try
                        {
                            foreach (XElement xIO in xFBFile.Root.Descendants("InputVars").FirstOrDefault().Elements())
                            {
                                createPort("InputVars", xIO, ref DIIndex, ref xNewFB);
                            }
                        }
                        catch (Exception)
                        { }

                        try
                        {
                            foreach (XElement xIO in xFBFile.Root.Descendants("OutputVars").FirstOrDefault().Elements())
                            {
                                createPort("OutputVars", xIO, ref DOIndex, ref xNewFB);
                            }
                        }
                        catch (Exception)
                        { }
                    }

                    xDiagramData.Add(xNewFB);
                }
            }
            catch (Exception)
            { }

            //Create all PLUG FBs
            try
            {
                foreach (XElement xADP in xFBType.Descendants("Plugs").FirstOrDefault().Elements("AdapterDeclaration"))
                {
                    XElement xNewFB = new XElement("Node");
                    createXElementAttr("Key", "FBI" + (iKey++).ToString(), ref xNewFB);
                    createXElementAttr("Category", "FBTemplate", ref xNewFB);
                    createXElementAttr("Location", xADP.Attribute("x").Value + " " + xADP.Attribute("y").Value, ref xNewFB);
                    createXElementAttr("Instance", xADP.Attribute("Name").Value, ref xNewFB);
                    createXElementAttr("Type", xADP.Attribute("Type").Value, ref xNewFB);

                    string sFilename = searchFileByName("FBLib", xADP.Attribute("Type").Value + ".adp");
                    //Will use the first one
                    //TODO - When save a file should check if existing file is stored in the library
                    if (sFilename != "")
                    {
                        int EIIndex = 0;
                        int EOIndex = 0;
                        int DIIndex = 0;
                        int DOIndex = 0;

                        XDocument xFBFile = XDocument.Load(sFilename);

                        try
                        {
                            foreach (XElement xIO in xFBFile.Root.Descendants("EventInputs").FirstOrDefault().Elements())
                            {
                                createPort("EventOutputs", xIO, ref EIIndex, ref xNewFB);
                            }
                        }
                        catch (Exception)
                        { }

                        try
                        {
                            foreach (XElement xIO in xFBFile.Root.Descendants("EventOutputs").FirstOrDefault().Elements())
                            {
                                createPort("EventInputs", xIO, ref EOIndex, ref xNewFB);
                            }
                        }
                        catch (Exception)
                        { }

                        try
                        {
                            foreach (XElement xIO in xFBFile.Root.Descendants("InputVars").FirstOrDefault().Elements())
                            {
                                createPort("OutputVars", xIO, ref DIIndex, ref xNewFB);
                            }
                        }
                        catch (Exception)
                        { }

                        try
                        {
                            foreach (XElement xIO in xFBFile.Root.Descendants("OutputVars").FirstOrDefault().Elements())
                            {
                                createPort("InputVars", xIO, ref DOIndex, ref xNewFB);
                            }
                        }
                        catch (Exception)
                        { }
                    }

                    xDiagramData.Add(xNewFB);
                }
            }
            catch (Exception)
            { }

            //create all external I/Os
            iKey = 0;
            try
            {
                foreach (XElement xEI in xFBType.Descendants("EventInputs").FirstOrDefault().Elements("Event"))
                {
                    XElement xNewEI = new XElement("Node");
                    createXElementAttr("Key", "EI" + (iKey++).ToString(), ref xNewEI);
                    createXElementAttr("Category", "EITemplate", ref xNewEI);
                    createXElementAttr("Location", "5 " + (iKey * 25 + 25).ToString(), ref xNewEI);
                    createXElementAttr("Instance", xEI.Attribute("Name").Value, ref xNewEI);
                    createXElementAttr("Type", "EI", ref xNewEI);

                    XElement xNewEIVar = new XElement("Var");
                    createXElementAttr("Name", xEI.Attribute("Name").Value, ref xNewEIVar);
                    createXElementAttr("Type", "EI", ref xNewEIVar);
                    createXElementAttr("Index", "0", ref xNewEIVar);
                    createXElementAttr("Color", eventColor, ref xNewEIVar);

                    xNewEI.Add(xNewEIVar);
                    xDiagramData.Add(xNewEI);
                }
            }
            catch (Exception)
            { }

            iKey = 0;
            try
            {
                foreach (XElement xEO in xFBType.Descendants("EventOutputs").FirstOrDefault().Elements("Event"))
                {
                    XElement xNewEO = new XElement("Node");
                    createXElementAttr("Key", "EO" + (iKey++).ToString(), ref xNewEO);
                    createXElementAttr("Category", "EOTemplate", ref xNewEO);
                    createXElementAttr("Location", (iXMax + 300).ToString() + " " + (iKey * 25 + 25).ToString(), ref xNewEO);
                    createXElementAttr("Instance", xEO.Attribute("Name").Value, ref xNewEO);
                    createXElementAttr("Type", "EO", ref xNewEO);

                    XElement xNewEOVar = new XElement("Var");
                    createXElementAttr("Name", xEO.Attribute("Name").Value, ref xNewEOVar);
                    createXElementAttr("Type", "EO", ref xNewEOVar);
                    createXElementAttr("Index", "0", ref xNewEOVar);
                    createXElementAttr("Color", eventColor, ref xNewEOVar);

                    xNewEO.Add(xNewEOVar);
                    xDiagramData.Add(xNewEO);
                }
            }
            catch (Exception)
            { }

            iKey = 0;
            try
            {
                foreach (XElement xDI in xFBType.Descendants("InputVars").FirstOrDefault().Elements("VarDeclaration"))
                {
                    XElement xNewDI = new XElement("Node");
                    createXElementAttr("Key", "DI" + (iKey++).ToString(), ref xNewDI);
                    createXElementAttr("Category", "DITemplate", ref xNewDI);
                    createXElementAttr("Location", "55 " + (iYMax + iKey * 25 + 100).ToString(), ref xNewDI);
                    createXElementAttr("Instance", xDI.Attribute("Name").Value, ref xNewDI);
                    createXElementAttr("Type", "DI", ref xNewDI);

                    XElement xNewDIVar = new XElement("Var");
                    createXElementAttr("Name", xDI.Attribute("Name").Value, ref xNewDIVar);
                    createXElementAttr("Type", "DI", ref xNewDIVar);
                    createXElementAttr("Index", "0", ref xNewDIVar);
                    createXElementAttr("Color", eventColor, ref xNewDIVar);

                    xNewDI.Add(xNewDIVar);
                    xDiagramData.Add(xNewDI);
                }
            }
            catch (Exception)
            { }

            iKey = 0;
            try
            {
                foreach (XElement xDO in xFBType.Descendants("OutputVars").FirstOrDefault().Elements("VarDeclaration"))
                {
                    XElement xNewDO = new XElement("Node");
                    createXElementAttr("Key", "DO" + (iKey++).ToString(), ref xNewDO);
                    createXElementAttr("Category", "DOTemplate", ref xNewDO);
                    createXElementAttr("Location", (iXMax + 300).ToString() + " " + (iYMax + iKey * 25 + 100).ToString(), ref xNewDO);
                    createXElementAttr("Instance", xDO.Attribute("Name").Value, ref xNewDO);
                    createXElementAttr("Type", "DO", ref xNewDO);

                    XElement xNewDOVar = new XElement("Var");
                    createXElementAttr("Name", xDO.Attribute("Name").Value, ref xNewDOVar);
                    createXElementAttr("Type", "DO", ref xNewDOVar);
                    createXElementAttr("Index", "0", ref xNewDOVar);
                    createXElementAttr("Color", eventColor, ref xNewDOVar);

                    xNewDO.Add(xNewDOVar);
                    xDiagramData.Add(xNewDO);
                }
            }
            catch (Exception)
            { }

            //Create all connections
            foreach (XElement xConn in xFBType.Descendants("Connection"))
            {
                string sSourceID = "";
                string sDestID = "";
                XElement xNewConn = new XElement("Conn");
                if (xConn.Attribute("Source").Value.IndexOf(".") > 0)
                {
                    sSourceID = getDiagramInstanceIDFromName(ref xDiagramData, xConn.Attribute("Source").Value.Split('.')[0]);
                    createXElementAttr("From", sSourceID, ref xNewConn);
                    createXElementAttr("FromPort", xConn.Attribute("Source").Value.Split('.')[1], ref xNewConn);
                }
                else
                {
                    sSourceID = getDiagramInstanceIDFromName(ref xDiagramData, xConn.Attribute("Source").Value);
                    createXElementAttr("From", sSourceID, ref xNewConn);
                    createXElementAttr("FromPort", xConn.Attribute("Source").Value, ref xNewConn);
                }

                if (xConn.Attribute("Destination").Value.IndexOf(".") > 0)
                {
                    sDestID = getDiagramInstanceIDFromName(ref xDiagramData, xConn.Attribute("Destination").Value.Split('.')[0]);
                    createXElementAttr("To", sDestID, ref xNewConn);
                    createXElementAttr("ToPort", xConn.Attribute("Destination").Value.Split('.')[1], ref xNewConn);
                }
                else
                {
                    sDestID = getDiagramInstanceIDFromName(ref xDiagramData, xConn.Attribute("Destination").Value);
                    createXElementAttr("To", sDestID, ref xNewConn);
                    createXElementAttr("ToPort", xConn.Attribute("Destination").Value, ref xNewConn);
                }

                createXElementAttr("Points", "", ref xNewConn);
                string sConnType = "EventConn";
                if (xConn.Parent.Name == "DataConnections") sConnType = "DataConn";
                if (xConn.Parent.Name == "AdapterConnections") sConnType = "AdapterConn";
                createXElementAttr("ConnType", sConnType, ref xNewConn);
                
                if (sSourceID != "" && sDestID != "") xDiagramData.Add(xNewConn);
            }

            //Create All Parameter Connections
            foreach (XElement xPNode in xDiagramData.Descendants("Node").Where(item => item.Attribute("Category").Value == "DIPTemplate"))
            {
                XElement xNewConn = new XElement("Conn");

                string sDestID = getDiagramInstanceIDFromName(ref xDiagramData, xPNode.Attribute("Type").Value);

                createXElementAttr("From", xPNode.Attribute("Key").Value, ref xNewConn);
                //createXElementAttr("FromPort", xPNode.Element("Var").Attribute("Name").Value, ref xNewConn);
                createXElementAttr("FromPort", "DIP", ref xNewConn);
                createXElementAttr("To", sDestID, ref xNewConn);
                createXElementAttr("ToPort", xPNode.Element("Var").Attribute("Name").Value, ref xNewConn);
                xPNode.Element("Var").Attribute("Name").Value = "DIP";

                createXElementAttr("Points", "", ref xNewConn);
                createXElementAttr("ConnType", "DataConn", ref xNewConn);

                xDiagramData.Add(xNewConn);
            }
        }

        private string getDiagramKeyIDFromStateName(ref XElement xDiagram, string sStateName)
        {
            try
            {
                return xDiagram.Descendants("ECState").Where(item => item.Attribute("Name").Value == sStateName).FirstOrDefault().Attribute("Key").Value;
            }
            catch (Exception)
            {
                return "";
            }
        }

        private string getDiagramInstanceIDFromName(ref XElement xDiagram, string sInstanceName)
        {
            try
            {
                return xDiagram.Descendants("Node").Where(item => item.Attribute("Instance").Value == sInstanceName).FirstOrDefault().Attribute("Key").Value;
            }
            catch (Exception)
            {
                return "";
            }
        }

        private void createPort(string sNodeName, XElement xNode, ref int Index, ref XElement XNewFB)
        {
            XElement xNewVar = new XElement("Var");
            createXElementAttr("Name", xNode.Attribute("Name").Value, ref xNewVar);
            if (sNodeName == "EventInputs")
            {
                createXElementAttr("Type", "EI", ref xNewVar);
                createXElementAttr("DataType", "EVENT", ref xNewVar);
                createXElementAttr("Index", (Index++).ToString(), ref xNewVar);
                createXElementAttr("Color", eventColor, ref xNewVar);
                string sWith = "";
                foreach(XElement xWith in xNode.Elements())
                {
                    sWith += xWith.Attribute("Var").Value + ",";
                }
                if (sWith.Length > 0)
                {
                    sWith = sWith.Substring(0, sWith.Length - 1);
                }
                createXElementAttr("With", sWith, ref xNewVar);
                string sComment = "";
                if (xNode.Attribute("Comment") != null)
                {
                    sComment = xNode.Attribute("Comment").Value;
                }
                createXElementAttr("Comment", sComment, ref xNewVar);
                createXElementAttr("ArraySize", "", ref xNewVar);
                createXElementAttr("InitialValue", "", ref xNewVar);
            }
            else if (sNodeName == "EventOutputs")
            {
                createXElementAttr("Type", "EO", ref xNewVar);
                createXElementAttr("DataType", "EVENT", ref xNewVar);
                createXElementAttr("Index", (Index++).ToString(), ref xNewVar);
                createXElementAttr("Color", eventColor, ref xNewVar);
                string sWith = "";
                foreach (XElement xWith in xNode.Elements())
                {
                    sWith += xWith.Attribute("Var").Value + ",";
                }
                if (sWith.Length > 0)
                {
                    sWith = sWith.Substring(0, sWith.Length - 1);
                }
                createXElementAttr("With", sWith, ref xNewVar);
                string sComment = "";
                if (xNode.Attribute("Comment") != null)
                {
                    sComment = xNode.Attribute("Comment").Value;
                }
                createXElementAttr("Comment", sComment, ref xNewVar);
                createXElementAttr("ArraySize", "", ref xNewVar);
                createXElementAttr("InitialValue", "", ref xNewVar);
            }
            else if (sNodeName == "InputVars")
            {
                createXElementAttr("Type", "DI", ref xNewVar);
                createXElementAttr("DataType", xNode.Attribute("Type").Value, ref xNewVar);
                createXElementAttr("Index", (Index++).ToString(), ref xNewVar);
                createXElementAttr("Color", dataColor, ref xNewVar);

                string sWith = "";
                createXElementAttr("With", sWith, ref xNewVar);

                string sComment = "";
                if (xNode.Attribute("Comment") != null)
                {
                    sComment = xNode.Attribute("Comment").Value;
                }
                createXElementAttr("Comment", sComment, ref xNewVar);

                string sArraySize = "";
                if (xNode.Attribute("ArraySize") != null)
                {
                    sArraySize = xNode.Attribute("ArraySize").Value;
                }
                createXElementAttr("ArraySize", sArraySize, ref xNewVar);

                string sInitialValue = "";
                if (xNode.Attribute("InitialValue") != null)
                {
                    sInitialValue = xNode.Attribute("InitialValue").Value;
                }
                createXElementAttr("InitialValue", sInitialValue, ref xNewVar);
            }
            else if (sNodeName == "OutputVars")
            {
                createXElementAttr("Type", "DO", ref xNewVar);
                createXElementAttr("DataType", xNode.Attribute("Type").Value, ref xNewVar);
                createXElementAttr("Index", (Index++).ToString(), ref xNewVar);
                createXElementAttr("Color", dataColor, ref xNewVar);

                string sWith = "";
                createXElementAttr("With", sWith, ref xNewVar);

                string sComment = "";
                if (xNode.Attribute("Comment") != null)
                {
                    sComment = xNode.Attribute("Comment").Value;
                }
                createXElementAttr("Comment", sComment, ref xNewVar);

                string sArraySize = "";
                if (xNode.Attribute("ArraySize") != null)
                {
                    sArraySize = xNode.Attribute("ArraySize").Value;
                }
                createXElementAttr("ArraySize", sArraySize, ref xNewVar);

                string sInitialValue = "";
                if (xNode.Attribute("InitialValue") != null)
                {
                    sInitialValue = xNode.Attribute("InitialValue").Value;
                }
                createXElementAttr("InitialValue", sInitialValue, ref xNewVar);
            }
            else if (sNodeName == "InternalVars")
            {
                createXElementAttr("Type", "IV", ref xNewVar);
                createXElementAttr("DataType", xNode.Attribute("Type").Value, ref xNewVar);
                createXElementAttr("Index", (Index++).ToString(), ref xNewVar);
                createXElementAttr("Color", dataColor, ref xNewVar);
                
                string sWith = "";
                createXElementAttr("With", sWith, ref xNewVar);

                string sComment = "";
                if (xNode.Attribute("Comment") != null)
                {
                    sComment = xNode.Attribute("Comment").Value;
                }
                createXElementAttr("Comment", sComment, ref xNewVar);

                string sArraySize = "";
                if (xNode.Attribute("ArraySize") != null)
                {
                    sArraySize = xNode.Attribute("ArraySize").Value;
                }
                createXElementAttr("ArraySize", sArraySize, ref xNewVar);

                string sInitialValue = "";
                if (xNode.Attribute("InitialValue") != null)
                {
                    sInitialValue = xNode.Attribute("InitialValue").Value;
                }
                createXElementAttr("InitialValue", sInitialValue, ref xNewVar);
            }
            else if (sNodeName == "Sockets")
            {
                createXElementAttr("Type", "SOCKET", ref xNewVar);
                createXElementAttr("DataType", xNode.Attribute("Type").Value, ref xNewVar);
                createXElementAttr("Index", (Index++).ToString(), ref xNewVar);
                createXElementAttr("Color", adpColor, ref xNewVar);
                createXElementAttr("With", "", ref xNewVar);

                string sComment = "";
                if (xNode.Attribute("Comment") != null)
                {
                    sComment = xNode.Attribute("Comment").Value;
                }
                createXElementAttr("Comment", sComment, ref xNewVar);
                createXElementAttr("ArraySize", "", ref xNewVar);
                createXElementAttr("InitialValue", "", ref xNewVar);
            }
            else if (sNodeName == "Plugs")
            {
                createXElementAttr("Type", "PLUG", ref xNewVar);
                createXElementAttr("DataType", xNode.Attribute("Type").Value, ref xNewVar);
                createXElementAttr("Index", (Index++).ToString(), ref xNewVar);
                createXElementAttr("Color", adpColor, ref xNewVar);
                createXElementAttr("With", "", ref xNewVar);

                string sComment = "";
                if (xNode.Attribute("Comment") != null)
                {
                    sComment = xNode.Attribute("Comment").Value;
                }
                createXElementAttr("Comment", sComment, ref xNewVar);
                createXElementAttr("ArraySize", "", ref xNewVar);
                createXElementAttr("InitialValue", "", ref xNewVar);
            }
            XNewFB.Add(xNewVar);
        }

        private void createSubAppPort(string sNodeName, XElement xNode, ref int Index, ref XElement XNewFB)
        {
            XElement xNewVar = new XElement("Var");
            createXElementAttr("Name", xNode.Attribute("Name").Value, ref xNewVar);
            if (sNodeName == "SubAppEventInputs")
            {
                createXElementAttr("Type", "EI", ref xNewVar);
                createXElementAttr("DataType", "EVENT", ref xNewVar);
                createXElementAttr("Index", (Index++).ToString(), ref xNewVar);
                createXElementAttr("Color", eventColor, ref xNewVar);
                createXElementAttr("With", "", ref xNewVar);
                string sComment = "";
                if (xNode.Attribute("Comment") != null)
                {
                    sComment = xNode.Attribute("Comment").Value;
                }
                createXElementAttr("Comment", sComment, ref xNewVar);
                createXElementAttr("ArraySize", "", ref xNewVar);
                createXElementAttr("InitialValue", "", ref xNewVar);
            }
            else if (sNodeName == "SubAppEventOutputs")
            {
                createXElementAttr("Type", "EO", ref xNewVar);
                createXElementAttr("DataType", "EVENT", ref xNewVar);
                createXElementAttr("Index", (Index++).ToString(), ref xNewVar);
                createXElementAttr("Color", eventColor, ref xNewVar);
                createXElementAttr("With", "", ref xNewVar);
                string sComment = "";
                if (xNode.Attribute("Comment") != null)
                {
                    sComment = xNode.Attribute("Comment").Value;
                }
                createXElementAttr("Comment", sComment, ref xNewVar);
                createXElementAttr("ArraySize", "", ref xNewVar);
                createXElementAttr("InitialValue", "", ref xNewVar);
            }
            else if (sNodeName == "InputVars")
            {
                createXElementAttr("Type", "DI", ref xNewVar);
                createXElementAttr("DataType", xNode.Attribute("Type").Value, ref xNewVar);
                createXElementAttr("Index", (Index++).ToString(), ref xNewVar);
                createXElementAttr("Color", dataColor, ref xNewVar);

                string sWith = "";
                createXElementAttr("With", sWith, ref xNewVar);

                string sComment = "";
                if (xNode.Attribute("Comment") != null)
                {
                    sComment = xNode.Attribute("Comment").Value;
                }
                createXElementAttr("Comment", sComment, ref xNewVar);

                string sArraySize = "";
                if (xNode.Attribute("ArraySize") != null)
                {
                    sArraySize = xNode.Attribute("ArraySize").Value;
                }
                createXElementAttr("ArraySize", sArraySize, ref xNewVar);

                string sInitialValue = "";
                if (xNode.Attribute("InitialValue") != null)
                {
                    sInitialValue = xNode.Attribute("InitialValue").Value;
                }
                createXElementAttr("InitialValue", sInitialValue, ref xNewVar);
            }
            else if (sNodeName == "OutputVars")
            {
                createXElementAttr("Type", "DO", ref xNewVar);
                createXElementAttr("DataType", xNode.Attribute("Type").Value, ref xNewVar);
                createXElementAttr("Index", (Index++).ToString(), ref xNewVar);
                createXElementAttr("Color", dataColor, ref xNewVar);

                string sWith = "";
                createXElementAttr("With", sWith, ref xNewVar);

                string sComment = "";
                if (xNode.Attribute("Comment") != null)
                {
                    sComment = xNode.Attribute("Comment").Value;
                }
                createXElementAttr("Comment", sComment, ref xNewVar);

                string sArraySize = "";
                if (xNode.Attribute("ArraySize") != null)
                {
                    sArraySize = xNode.Attribute("ArraySize").Value;
                }
                createXElementAttr("ArraySize", sArraySize, ref xNewVar);

                string sInitialValue = "";
                if (xNode.Attribute("InitialValue") != null)
                {
                    sInitialValue = xNode.Attribute("InitialValue").Value;
                }
                createXElementAttr("InitialValue", sInitialValue, ref xNewVar);
            }
            
            XNewFB.Add(xNewVar);
        }


        private void createXElementAttr(string sName, string sValue, ref XElement xNode)
        {
            if (sName == null) return;
            if (sName == "") return;
            XAttribute xAttr = new XAttribute(sName, sValue);
            xNode.Add(xAttr);
        }
    }
}
