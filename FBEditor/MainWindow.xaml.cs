﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Northwoods.GoXam;
using Northwoods.GoXam.Model;
using System.IO;
using Microsoft.Win32;
using System.Xml.Linq;

namespace FBEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void SaveSingleFB(TabItem nTabItem)
        {
            if (nTabItem.Content is BasicFBEditor)
            {
                (nTabItem.Content as BasicFBEditor).SaveFBTFile();
            }
            else if (nTabItem.Content is CompositeFBEditor)
            {
                (nTabItem.Content as CompositeFBEditor).SaveFBTFile();
            }
            else if (nTabItem.Content is SIFBEditor)
            {
                (nTabItem.Content as SIFBEditor).SaveFBTFile();
            }
            else if (nTabItem.Content is SystemConfigurationEditor)
            {
                (nTabItem.Content as SystemConfigurationEditor).SaveSCFile();
            }
            else if (nTabItem.Content is SubApplicationEditor)
            {
                (nTabItem.Content as SubApplicationEditor).SaveFBTFile();
            }
            else if (nTabItem.Content is AdapterEditor)
            {
                (nTabItem.Content as AdapterEditor).SaveFBTFile();
            }
        }

        private void MenuItem_SC_Click(object sender, RoutedEventArgs e)
        {
            createSCTab(Directory.GetCurrentDirectory() + "\\Template\\SystemConfiguration.sys");
        }

        private void createSCTab(string path)
        {
            SystemConfigurationEditor nSCEditor = new SystemConfigurationEditor();
            nSCEditor.LoadFile(path);
            TabItem nTabItem = new TabItem();
            XDocument xDoc = XDocument.Load(path);
            nTabItem.Header = xDoc.Root.Attribute("Name").Value;
            nTabItem.Content = nSCEditor;
            this.TabControL_List.Items.Add(nTabItem);
            this.TabControL_List.SelectedItem = nTabItem;
        }

        private void CreateBFBTab(string path)
        {
            BasicFBEditor nBFBEditor = new BasicFBEditor();
            nBFBEditor.LoadFile(path);
            TabItem nTabItem = new TabItem();
            XDocument xDoc = XDocument.Load(path);
            nTabItem.Header = xDoc.Root.Attribute("Name").Value;
            nTabItem.Content = nBFBEditor;
            this.TabControL_List.Items.Add(nTabItem);
            this.TabControL_List.SelectedItem = nTabItem;
        }

        private void CreateCFBTab(string path)
        {
            CompositeFBEditor nCFBEditor = new CompositeFBEditor();
            nCFBEditor.LoadFile(path);
            TabItem nTabItem = new TabItem();
            XDocument xDoc = XDocument.Load(path);
            nTabItem.Header = xDoc.Root.Attribute("Name").Value;
            nTabItem.Content = nCFBEditor;
            this.TabControL_List.Items.Add(nTabItem);
            this.TabControL_List.SelectedItem = nTabItem;
        }

        private void CreateSIFBTab(string path)
        {
            SIFBEditor nSIFBEditor = new SIFBEditor();
            nSIFBEditor.LoadFile(path);
            TabItem nTabItem = new TabItem();
            XDocument xDoc = XDocument.Load(path);
            nTabItem.Header = xDoc.Root.Attribute("Name").Value;
            nTabItem.Content = nSIFBEditor;
            this.TabControL_List.Items.Add(nTabItem);
            this.TabControL_List.SelectedItem = nTabItem;
        }

        private void CreateADPTab(string path)
        {
            AdapterEditor nAdpEditor = new AdapterEditor();
            nAdpEditor.LoadFile(path);
            TabItem nTabItem = new TabItem();
            XDocument xDoc = XDocument.Load(path);
            nTabItem.Header = xDoc.Root.Attribute("Name").Value;
            nTabItem.Content = nAdpEditor;
            this.TabControL_List.Items.Add(nTabItem);
            this.TabControL_List.SelectedItem = nTabItem;
        }

        private void CreateSubAppTab(string path)
        {
            SubApplicationEditor nSubAppEditor = new SubApplicationEditor();
            nSubAppEditor.LoadFile(path);
            TabItem nTabItem = new TabItem();
            XDocument xDoc = XDocument.Load(path);
            nTabItem.Header = xDoc.Root.Attribute("Name").Value;
            nTabItem.Content = nSubAppEditor;
            this.TabControL_List.Items.Add(nTabItem);
            this.TabControL_List.SelectedItem = nTabItem;
        }

        private void MenuItem_BFB_Click(object sender, RoutedEventArgs e)
        {
            CreateBFBTab(Directory.GetCurrentDirectory() + "\\Template\\Basic.fbt");
        }

        private void MenuItem_SIFB_Click(object sender, RoutedEventArgs e)
        {
            CreateSIFBTab(Directory.GetCurrentDirectory() + "\\Template\\ServiceInterface.fbt");
        }

        private void MenuItem_CFB_Click(object sender, RoutedEventArgs e)
        {
            CreateCFBTab(Directory.GetCurrentDirectory() + "\\Template\\Composite.fbt");
        }

        private void MenuItem_SubApp_Click(object sender, RoutedEventArgs e)
        {
            CreateSubAppTab(Directory.GetCurrentDirectory() + "\\Template\\SubApp.app");
        }

        private void MenuItem_Adapter_Click(object sender, RoutedEventArgs e)
        {
            CreateADPTab(Directory.GetCurrentDirectory() + "\\Template\\Adapter.adp");
        }

        private void MenuItem_Load_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDLG = new OpenFileDialog();
            openDLG.Filter = "System Configuration Files (.sys)|*.sys|Function Block Files (.fbt)|*.fbt|SubApplication Types (.app)|*.app|Adapter Types (.adp)|*.adp";
            openDLG.RestoreDirectory = true;

            // Call the ShowDialog method to show the dialog box.
            bool? userClickedOK = openDLG.ShowDialog();

            // Process input if the user clicked OK.
            if (userClickedOK == true)
            {
                // Open the selected file to read.
                if (openDLG.FileName.EndsWith(".sys"))
                {
                    createSCTab(openDLG.FileName);
                }
                else if (openDLG.FileName.EndsWith(".fbt"))
                {
                    XDocument xFBDoc = XDocument.Load(openDLG.FileName);
                    if (xFBDoc.Root.Element("BasicFB") != null)
                    {
                        CreateBFBTab(openDLG.FileName);
                    }
                    else if (xFBDoc.Root.Element("FBNetwork") != null)
                    {
                        CreateCFBTab(openDLG.FileName);
                    }
                    else if (xFBDoc.Root.Element("Service") != null)
                    {
                        CreateSIFBTab(openDLG.FileName);
                    }

                }
                else if (openDLG.FileName.EndsWith(".adp"))
                {
                    CreateADPTab(openDLG.FileName);
                }
                else if (openDLG.FileName.EndsWith(".app"))
                {
                    CreateSubAppTab(openDLG.FileName);
                }
            }
        }

        private void MenuItem_Save_Click(object sender, RoutedEventArgs e)
        {
            if (this.TabControL_List.SelectedItem == null) return;

            SaveSingleFB(this.TabControL_List.SelectedItem as TabItem);
        }

        private void MenuItem_Close_Click(object sender, RoutedEventArgs e)
        {
            if (this.TabControL_List.SelectedItem == null) return;

            this.TabControL_List.Items.Remove(this.TabControL_List.SelectedItem);
        }

        private void MenuItem_About_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Design and Developed by William Dai", "Function Block Editor Lite v1.0");
        }
    }
}
