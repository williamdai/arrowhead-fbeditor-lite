﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Linq;
using Northwoods.GoXam;
using Northwoods.GoXam.Model;

namespace FBEditor
{
    /// <summary>
    /// Interaction logic for SIFBInterface.xaml
    /// </summary>
    public partial class SIFBInterface : UserControl
    {
        private ObservableCollection<string> listPredefinedDataTypes = new ObservableCollection<string>() { "BOOL", "SINT", "INT", "DINT", "USINT", "UINT", "UDINT", "REAL", "WSTRING" };
        private ObservableCollection<string> listDataTypes = new ObservableCollection<string>();

        public SIFBInterface()
        {
            InitializeComponent();
            
            commentWindow.SetParent(MainEditor);
            commentWindow.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            commentWindow.VerticalAlignment = System.Windows.VerticalAlignment.Top;
        }

        public void LoadUDT()
        {
            listDataTypes.Clear();

            foreach (string udtfile in listPredefinedDataTypes)
            {
                listDataTypes.Add(udtfile);
            }
        }

        public void LoadXML(XElement xFBType)
        {
            LoadUDT();

            var model = new CustomSIFBInterfaceModel();

            // initialize it from data in an XML file that is an embedded resource
            IDELibrary.FBXMLConverter xmlConverter = new IDELibrary.FBXMLConverter();
            XElement xData = xmlConverter.generateFBInterfaceFromXML(xFBType);

            model.Load<SIFBInterfaceUnit, Wire>(xData, "Node", "Conn");
            model.Modifiable = true;
            model.HasUndoManager = true;

            FBDiagram.Model = model;
            FBDiagram.MouseRightButtonUp += Port_RightButtonUp;
            FBDiagram.MouseDoubleClick += Port_DoubleClick;

            foreach (Node node in FBDiagram.Nodes)
            {
                node.Deletable = false;
                node.Movable = true;
            }
        }

        public void LoadFile(string sFileName)
        {
            LoadUDT();

            var model = new CustomSIFBInterfaceModel();

            // initialize it from data in an XML file that is an embedded resource
            IDELibrary.FBXMLConverter xmlConverter = new IDELibrary.FBXMLConverter();
            XElement xData = xmlConverter.generateFBInterfaceFromFile(sFileName);

            model.Load<SIFBInterfaceUnit, Wire>(xData, "Node", "Conn");
            model.Modifiable = true;
            model.HasUndoManager = true;

            FBDiagram.Model = model;
            FBDiagram.MouseRightButtonUp += Port_RightButtonUp;
            FBDiagram.MouseDoubleClick += Port_DoubleClick;

            foreach (Node node in FBDiagram.Nodes)
            {
                node.Deletable = false;
                node.Movable = false;
            }
        }

        public XElement getXML()
        {
            XElement xInterfaceList = new XElement("InterfaceList");
            SIFBInterfaceUnit u = FBDiagram.Nodes.ElementAt(0).Data as SIFBInterfaceUnit;

            XElement xEIs = new XElement("EventInputs");
            foreach (SIFBInterfaceSocket s in u.EventInputs)
            {
                XElement xEvent = new XElement("Event");
                AddAttributeToNode(ref xEvent, "Name", s.Name);
                AddAttributeToNode(ref xEvent, "Type", "EVENT");
                AddAttributeToNode(ref xEvent, "Comment", s.Comment);
                foreach (string sVar in s.WithVars)
                {
                    XElement xWith = new XElement("With");
                    XAttribute xVar = new XAttribute("Var", sVar);
                    xWith.Add(xVar);
                    xEvent.Add(xWith);
                }
                xEIs.Add(xEvent);
            }
            xInterfaceList.Add(xEIs);

            XElement xEOs = new XElement("EventOutputs");
            foreach (SIFBInterfaceSocket s in u.EventOutputs)
            {
                XElement xEvent = new XElement("Event");
                AddAttributeToNode(ref xEvent, "Name", s.Name);
                AddAttributeToNode(ref xEvent, "Type", "EVENT");
                AddAttributeToNode(ref xEvent, "Comment", s.Comment);
                foreach (string sVar in s.WithVars)
                {
                    XElement xWith = new XElement("With");
                    XAttribute xVar = new XAttribute("Var", sVar);
                    xWith.Add(xVar);
                    xEvent.Add(xWith);
                }
                xEOs.Add(xEvent);
            }
            xInterfaceList.Add(xEOs);

            XElement xDIs = new XElement("InputVars");
            foreach (SIFBInterfaceSocket s in u.DataInputs)
            {
                XElement xVarDef = new XElement("VarDeclaration");
                AddAttributeToNode(ref xVarDef, "Name", s.Name);
                AddAttributeToNode(ref xVarDef, "Type", s.DataType);
                AddAttributeToNode(ref xVarDef, "ArraySize", s.ArraySize);
                AddAttributeToNode(ref xVarDef, "InitialValue", GetInitialValue(s.ArraySize, s.InitValue));
                AddAttributeToNode(ref xVarDef, "Comment", s.Comment);
                xDIs.Add(xVarDef);
            }
            xInterfaceList.Add(xDIs);

            XElement xDOs = new XElement("OutputVars");
            foreach (SIFBInterfaceSocket s in u.DataOutputs)
            {
                XElement xVarDef = new XElement("VarDeclaration");
                AddAttributeToNode(ref xVarDef, "Name", s.Name);
                AddAttributeToNode(ref xVarDef, "Type", s.DataType);
                AddAttributeToNode(ref xVarDef, "ArraySize", s.ArraySize);
                AddAttributeToNode(ref xVarDef, "InitialValue", GetInitialValue(s.ArraySize, s.InitValue));
                AddAttributeToNode(ref xVarDef, "Comment", s.Comment);
                xDOs.Add(xVarDef);
            }
            xInterfaceList.Add(xDOs);

            /*XElement xSockets = new XElement("Sockets");

            xInterfaceList.Add(xSockets);

            XElement xPlugs = new XElement("Plugs");

            xInterfaceList.Add(xPlugs);*/

            return xInterfaceList;
        }

        private string GetInitialValue(string arraysize, string oInitvalue)
        {
            string sInitValue = "";
            if (arraysize != null && oInitvalue != null)
            {
                string[] allValues = oInitvalue.Split(',');
                for (int i = 0; i < GetArraySizeFromString(arraysize); i++)
                {
                    if (i < allValues.Length)
                    {
                        sInitValue += allValues[i] + ",";
                    }
                    else
                    {
                        sInitValue += "0,";
                    }
                }
                if (sInitValue.Length > 0)
                {
                    sInitValue = sInitValue.Substring(0, sInitValue.Length - 1);
                }
            }
            return sInitValue;
        }

        private int GetArraySizeFromString(string strSize)
        {
            int ArraySize = 1;
            try
            {
                ArraySize = Convert.ToInt32(strSize);
                if (ArraySize < 1)
                {
                    ArraySize = 1;
                }
            }
            catch (Exception)
            { }

            return ArraySize;
        }

        public XElement getXMLInternalVars()
        {
            XElement xInternalVars = new XElement("InternalVars");
            SIFBInterfaceUnit u = FBDiagram.Nodes.ElementAt(0).Data as SIFBInterfaceUnit;

            foreach (SIFBInterfaceSocket s in u.DataInternals)
            {
                XElement xVarDef = new XElement("VarDeclaration");
                AddAttributeToNode(ref xVarDef, "Name", s.Name);
                AddAttributeToNode(ref xVarDef, "Type", s.DataType);
                AddAttributeToNode(ref xVarDef, "ArraySize", s.ArraySize);
                AddAttributeToNode(ref xVarDef, "InitialValue", GetInitialValue(s.ArraySize, s.InitValue));
                AddAttributeToNode(ref xVarDef, "Comment", s.Comment);
                xInternalVars.Add(xVarDef);
            }
            
            return xInternalVars;
        }

        private void AddAttributeToNode(ref XElement node, string sAttr, string sValue)
        {
            if (node.Attribute(sAttr) != null)
            {
                node.Attribute(sAttr).Value = sValue;
            }
            else
            {
                XAttribute xAttr = new XAttribute(sAttr, sValue == null ? "" : sValue);
                node.Add(xAttr);
            }
        }

        private void TextBlock_EventInput_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            string sEvent = (sender as TextBlock).Tag.ToString();
            foreach (Node node in FBDiagram.Nodes)
            {
                SIFBInterfaceUnit u = node.Data as SIFBInterfaceUnit;

                //Save Changes
                SIFBInterfaceSocket ssEI = u.EventInputs.Where(item => item.Name == u.SelectedEI).FirstOrDefault();
                if (ssEI != null)
                {
                    ssEI.WithVars = new ObservableCollection<string>();
                    foreach (SIFBInterfaceSocket s in u.DataInputs)
                    {
                        if (s.Associated)
                        {
                            ssEI.WithVars.Add(s.Name);
                        }
                    }
                }

                if (u.SelectedEI != sEvent)
                {
                    u.SelectedEI = sEvent;
                }
                else
                {
                    u.SelectedEI = "";
                }

                SIFBInterfaceSocket sEI = u.EventInputs.Where(item => item.Name == sEvent).FirstOrDefault();

                foreach (SIFBInterfaceSocket s in u.DataInputs)
                {
                    if (u.SelectedEI == null || sEI == null)
                    {
                        s.Visible = false;
                    }
                    else if (u.SelectedEI == "")
                    {
                        s.Visible = false;
                    }
                    else
                    {
                        s.Visible = true;
                        if (sEI.WithVars.IndexOf(s.Name) >= 0)
                        {
                            s.Associated = true;
                        }
                        else
                        {
                            s.Associated = false;
                        }
                    }
                }
                
            }
        }

        private void TextBlock_EventOutput_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            string sEvent = (sender as TextBlock).Tag.ToString();
            foreach (Node node in FBDiagram.Nodes)
            {
                SIFBInterfaceUnit u = node.Data as SIFBInterfaceUnit;

                //Save Changes
                SIFBInterfaceSocket ssEO = u.EventOutputs.Where(item => item.Name == u.SelectedEO).FirstOrDefault();
                if (ssEO != null)
                {
                    ssEO.WithVars = new ObservableCollection<string>();
                    foreach (SIFBInterfaceSocket s in u.DataOutputs)
                    {
                        if (s.Associated)
                        {
                            ssEO.WithVars.Add(s.Name);
                        }
                    }
                }

                if (u.SelectedEO != sEvent)
                {
                    u.SelectedEO = sEvent;
                }
                else
                {
                    u.SelectedEO = "";
                }

                SIFBInterfaceSocket sEO = u.EventOutputs.Where(item => item.Name == sEvent).FirstOrDefault();

                foreach (SIFBInterfaceSocket s in u.DataOutputs)
                {
                    if (u.SelectedEO == null || sEO == null)
                    {
                        s.Visible = false;
                    }
                    else if (u.SelectedEO == "")
                    {
                        s.Visible = false;
                    }
                    else
                    {
                        s.Visible = true;
                        if (sEO.WithVars.IndexOf(s.Name) >= 0)
                        {
                            s.Associated = true;
                        }
                        else
                        {
                            s.Associated = false;
                        }
                    }
                }

            }
        }

        private void comboBox_DataInputs_Initialized(object sender, EventArgs e)
        {
            (sender as ComboBox).ItemsSource = listDataTypes;
        }

        private void comboBox_DataOutputs_Initialized(object sender, EventArgs e)
        {
            (sender as ComboBox).ItemsSource = listDataTypes;
        }

        private void TextBoxI_GotFocus(object sender, RoutedEventArgs e)
        {
            hideInputAssociationWindow();
        }

        private void TextBoxO_GotFocus(object sender, RoutedEventArgs e)
        {
            hideOutputAssociationWindow();
        }

        private void RefreshDataModel()
        {
            var model = FBDiagram.Model;
            FBDiagram.Model = null;
            FBDiagram.Model = model;
        }

        private void hideInputAssociationWindow()
        {
            foreach (Node node in FBDiagram.Nodes)
            {
                SIFBInterfaceUnit u = node.Data as SIFBInterfaceUnit;

                //Save Changes
                SIFBInterfaceSocket ssEI = u.EventInputs.Where(item => item.Name == u.SelectedEI).FirstOrDefault();
                if (ssEI != null)
                {
                    ssEI.WithVars = new ObservableCollection<string>();
                    foreach (SIFBInterfaceSocket s in u.DataInputs)
                    {
                        if (s.Associated)
                        {
                            ssEI.WithVars.Add(s.Name);
                        }
                    }
                }


                u.SelectedEI = "";

                foreach (SIFBInterfaceSocket s in u.DataInputs)
                {
                    s.Visible = false;
                }
            }
        }

        private void hideOutputAssociationWindow()
        {
            foreach (Node node in FBDiagram.Nodes)
            {
                SIFBInterfaceUnit u = node.Data as SIFBInterfaceUnit;

                //Save Changes
                SIFBInterfaceSocket ssEO = u.EventOutputs.Where(item => item.Name == u.SelectedEO).FirstOrDefault();
                if (ssEO != null)
                {
                    ssEO.WithVars = new ObservableCollection<string>();
                    foreach (SIFBInterfaceSocket s in u.DataOutputs)
                    {
                        if (s.Associated)
                        {
                            ssEO.WithVars.Add(s.Name);
                        }
                    }
                }

                u.SelectedEO = "";

                foreach (SIFBInterfaceSocket s in u.DataOutputs)
                {
                    s.Visible = false;
                }
            }
        }

        private void newEIButton_Click(object sender, RoutedEventArgs e)
        {
            hideInputAssociationWindow();

            foreach (Node node in FBDiagram.Nodes)
            {
                SIFBInterfaceUnit unit = node.Data as SIFBInterfaceUnit;
                if (unit != null)
                {
                    // find unique socket name for the given side
                    int i = 1;
                    while (unit.FindSocket("EI" + i.ToString()) != null) i++;
                    // modify the model
                    FBDiagram.StartTransaction("Add Socket");
                    unit.AddSocket("EI", "EI" + i.ToString(), "Black", "EVENT");
                    FBDiagram.CommitTransaction("Add Socket");
                }
            }
        }

        private void newEOButton_Click(object sender, RoutedEventArgs e)
        {
            hideOutputAssociationWindow();

            foreach (Node node in FBDiagram.Nodes)
            {
                SIFBInterfaceUnit unit = node.Data as SIFBInterfaceUnit;
                if (unit != null)
                {
                    // find unique socket name for the given side
                    int i = 1;
                    while (unit.FindSocket("EO" + i.ToString()) != null) i++;
                    // modify the model
                    FBDiagram.StartTransaction("Add Socket");
                    unit.AddSocket("EO", "EO" + i.ToString(), "Black", "EVENT");
                    FBDiagram.CommitTransaction("Add Socket");
                }
            }
        }

        // If the element at the mouse point is a port, remove it from its Node
        private void Port_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement elt = FBDiagram.Panel.FindElementAt<FrameworkElement>(FBDiagram.LastMousePointInModel,
                                                      x => x as FrameworkElement, x => true, SearchLayers.Nodes);
            if (elt == null) return;
            String name = Node.GetPortId(elt);
            if (name == null) return;
            String side = elt.Tag as String;
            if (side == null) return;
            Node node = Part.FindAncestor<Node>(elt);
            if (node != null)
            {
                SIFBInterfaceUnit u = node.Data as SIFBInterfaceUnit;
                if (u != null)
                {
                    SIFBInterfaceSocket s = u.FindSocket(name);
                    if (s != null)
                    {
                        double iX = e.GetPosition(MainEditor).X - 150;
                        if (iX < 0)
                        {
                            iX = 20;
                        }
                        commentWindow.Margin = new Thickness(iX, e.GetPosition(MainEditor).Y, 0, 0);
                        s.Comment = commentWindow.ShowHandlerDialog(s.Name, s.Comment);
                    }
                }
            }
        }

        // If the element at the mouse point is a port, remove it from its Node
        private void Port_RightButtonUp(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement elt = FBDiagram.Panel.FindElementAt<FrameworkElement>(FBDiagram.LastMousePointInModel,
                                                      x => x as FrameworkElement, x => true, SearchLayers.Nodes);
            if (elt == null) return;
            String name = Node.GetPortId(elt);
            if (name == null) return;
            String side = elt.Tag as String;
            if (side == null) return;
            Node node = Part.FindAncestor<Node>(elt);
            if (node != null)
            {
                SIFBInterfaceUnit u = node.Data as SIFBInterfaceUnit;
                if (u != null)
                {
                    hideInputAssociationWindow();
                    hideOutputAssociationWindow();
                    FBDiagram.StartTransaction("Remove Socket");
                    u.RemoveSocket(u.FindSocket(name));
                    FBDiagram.CommitTransaction("Remove Socket");
                }
            }
        }

        private void newDIButton_Click(object sender, RoutedEventArgs e)
        {
            hideInputAssociationWindow();

            foreach (Node node in FBDiagram.Nodes)
            {
                SIFBInterfaceUnit unit = node.Data as SIFBInterfaceUnit;
                if (unit != null)
                {
                    // find unique socket name for the given side
                    int i = 1;
                    while (unit.FindSocket("DI" + i.ToString()) != null) i++;
                    // modify the model
                    FBDiagram.StartTransaction("Add Socket");
                    string sLastDT = unit.GetLastDataType("DI");
                    unit.AddSocket("DI", "DI" + i.ToString(), "Black", sLastDT);
                    FBDiagram.CommitTransaction("Add Socket");
                }
            }
        }

        private void newDOButton_Click(object sender, RoutedEventArgs e)
        {
            hideOutputAssociationWindow();

            foreach (Node node in FBDiagram.Nodes)
            {
                SIFBInterfaceUnit unit = node.Data as SIFBInterfaceUnit;
                if (unit != null)
                {
                    // find unique socket name for the given side
                    int i = 1;
                    while (unit.FindSocket("DO" + i.ToString()) != null) i++;
                    // modify the model
                    FBDiagram.StartTransaction("Add Socket");
                    string sLastDT = unit.GetLastDataType("DO");
                    unit.AddSocket("DO", "DO" + i.ToString(), "Black", sLastDT);
                    FBDiagram.CommitTransaction("Add Socket");
                }
            }
        }

        private void newIVButton_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            foreach (Node node in FBDiagram.Nodes)
            {
                SIFBInterfaceUnit unit = node.Data as SIFBInterfaceUnit;
                if (unit != null)
                {
                    // find unique socket name for the given side
                    int i = 1;
                    while (unit.FindSocket("IV" + i.ToString()) != null) i++;
                    // modify the model
                    FBDiagram.StartTransaction("Add Socket");
                    string sLastDT = unit.GetLastDataType("IV");
                    unit.AddSocket("IV", "IV" + i.ToString(), "#ff8a2e", sLastDT);
                    FBDiagram.CommitTransaction("Add Socket");
                }
            }
        }

        private void comboBox_DataInternals_Initialized(object sender, EventArgs e)
        {
            (sender as ComboBox).ItemsSource = listDataTypes;
        }
    }

    public class CustomSIFBInterfaceModel : GraphLinksModel<SIFBInterfaceUnit, String, String, Wire>
    {
        // When a Unit gets an extra Socket or when a Socket is removed,
        // tell the Diagram.PartManager that some (or all) of the port FrameworkElements
        // in the Node corresponding to a unit may have moved or changed size.
        protected override void HandleNodePropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.HandleNodePropertyChanged(sender, e);
            SIFBInterfaceUnit unit = sender as SIFBInterfaceUnit;
            if (unit != null && (e.PropertyName == "AddedSocket" || e.PropertyName == "RemovedSocket"))
            {
                RaiseChanged(new ModelChangedEventArgs() { Model = this, Change = ModelChange.InvalidateRelationships, Data = unit });
            }
        }
    }


    // Each set of "Sockets" has to be a property that is an ObservableCollection
    [Serializable]
    public class SIFBInterfaceUnit : GraphLinksModelNodeData<String>
    {
        //Extra Field For FB Type
        public string Type { get; set; }

        public string SelectedEI
        {
            get { return _SelectedEI; }
            set
            {
                if (_SelectedEI != value)
                {
                    string old = _SelectedEI;
                    _SelectedEI = value;
                    RaisePropertyChanged("SelectedEI", old, value);
                }
            }
        }
        private string _SelectedEI = "";

        public string SelectedEO
        {
            get { return _SelectedEO; }
            set
            {
                if (_SelectedEO != value)
                {
                    string old = _SelectedEO;
                    _SelectedEO = value;
                    RaisePropertyChanged("SelectedEO", old, value);
                }
            }
        }
        private string _SelectedEO = "";

        // When a Unit is copied, it needs separate collections of Sockets
        public override object Clone()
        {
            SIFBInterfaceUnit unit = (SIFBInterfaceUnit)base.Clone();
            unit._EventInputs = new ObservableCollection<SIFBInterfaceSocket>();
            foreach (SIFBInterfaceSocket s in this.EventInputs) unit._EventInputs.Add((SIFBInterfaceSocket)s.Clone());
            unit._EventOutputs = new ObservableCollection<SIFBInterfaceSocket>();
            foreach (SIFBInterfaceSocket s in this.EventOutputs) unit._EventOutputs.Add((SIFBInterfaceSocket)s.Clone());
            unit._DataInputs = new ObservableCollection<SIFBInterfaceSocket>();
            foreach (SIFBInterfaceSocket s in this.DataInputs) unit._DataInputs.Add((SIFBInterfaceSocket)s.Clone());
            unit._DataOutputs = new ObservableCollection<SIFBInterfaceSocket>();
            foreach (SIFBInterfaceSocket s in this.DataOutputs) unit._DataOutputs.Add((SIFBInterfaceSocket)s.Clone());
            unit._DataInternals = new ObservableCollection<SIFBInterfaceSocket>();
            foreach (SIFBInterfaceSocket s in this.DataInternals) unit._DataInternals.Add((SIFBInterfaceSocket)s.Clone());
            // if you add properties that are not supposed to be shared, deal with them here
            return unit;
        }

        // Property change for undo/redo:
        // We treat adding and removing socket as property changes, and
        // there are settable properties of Socket for each socket to handle.
        public override void ChangeDataValue(ModelChangedEventArgs e, bool undo)
        {
            // Data might be either a Unit or a Socket
            SIFBInterfaceSocket sock = e.Data as SIFBInterfaceSocket;
            if (sock != null)
            {  // if it's a Socket, let it handle undo/redo changes
                sock.ChangeDataValue(e, undo);
            }
            else
            {
                // assume we're dealing with a change to this Unit
                switch (e.PropertyName)
                {
                    case "AddedSocket":
                        sock = e.OldValue as SIFBInterfaceSocket;
                        if (undo)
                            RemoveSocket(sock);
                        else
                            InsertSocket(sock);
                        break;
                    case "RemovedSocket":
                        sock = e.OldValue as SIFBInterfaceSocket;
                        if (undo)
                            InsertSocket(sock);
                        else
                            RemoveSocket(sock);
                        break;
                    // if you add undo-able properties to Unit, handle them here
                    default:
                        base.ChangeDataValue(e, undo);
                        break;
                }
            }
        }

        // write the base element
        public XElement baseMakeXElement(XName n)
        {
            XElement e = new XElement("Node");
            e.Add(XHelper.Attribute("Key", base.Key, ""));
            e.Add(XHelper.Attribute("Category", base.Category, ""));
            e.Add(XHelper.Attribute("Location", base.Location, new Point()));
            e.Add(XHelper.Attribute("Instance", base.Text, ""));
            e.Add(XHelper.Attribute("Type", this.Type, ""));
            return e;
        }

        // load the base element
        public void baseLoadFromXElement(XElement e)
        {
            if (e == null) return;
            base.Key = XHelper.Read("Key", e, "");
            base.Category = XHelper.Read("Category", e, "");
            base.Location = XHelper.Read("Location", e, new Point());
            base.Text = XHelper.Read("Instance", e, "");
            this.Type = XHelper.Read("Type", e, "");
        }

        // write the Sockets as child elements
        public override XElement MakeXElement(XName n)
        {
            //XElement e = base.MakeXElement(n);
            XElement e = baseMakeXElement(n);
            e.Add(this.EventInputs.Select(s => s.MakeXElement()));
            e.Add(this.EventOutputs.Select(s => s.MakeXElement()));
            e.Add(this.DataInputs.Select(s => s.MakeXElement()));
            e.Add(this.DataOutputs.Select(s => s.MakeXElement()));
            e.Add(this.DataInternals.Select(s => s.MakeXElement()));
            return e;
        }

        // read the child elements as Sockets
        public override void LoadFromXElement(XElement e)
        {
            //base.LoadFromXElement(e);
            baseLoadFromXElement(e);
            foreach (XElement c in e.Elements("Var"))
            {
                InsertSocket(new SIFBInterfaceSocket().LoadFromXElement(c));
            }
        }

        public IEnumerable<SIFBInterfaceSocket> EventInputs
        {
            get { return _EventInputs; }
        }
        private ObservableCollection<SIFBInterfaceSocket> _EventInputs = new ObservableCollection<SIFBInterfaceSocket>();

        public IEnumerable<SIFBInterfaceSocket> EventOutputs
        {
            get { return _EventOutputs; }
        }
        private ObservableCollection<SIFBInterfaceSocket> _EventOutputs = new ObservableCollection<SIFBInterfaceSocket>();

        public IEnumerable<SIFBInterfaceSocket> DataInputs
        {
            get { return _DataInputs; }
        }
        private ObservableCollection<SIFBInterfaceSocket> _DataInputs = new ObservableCollection<SIFBInterfaceSocket>();

        public IEnumerable<SIFBInterfaceSocket> DataOutputs
        {
            get { return _DataOutputs; }
        }
        private ObservableCollection<SIFBInterfaceSocket> _DataOutputs = new ObservableCollection<SIFBInterfaceSocket>();
        
        public IEnumerable<SIFBInterfaceSocket> DataInternals
        {
            get { return _DataInternals; }
        }
        private ObservableCollection<SIFBInterfaceSocket> _DataInternals = new ObservableCollection<SIFBInterfaceSocket>();


        // used to find whether a Socket exists for a name
        public SIFBInterfaceSocket FindSocket(String name)
        {
            int i = IndexOf(_EventInputs, name);
            if (i >= 0) return _EventInputs[i];
            i = IndexOf(_EventOutputs, name);
            if (i >= 0) return _EventOutputs[i];
            i = IndexOf(_DataInputs, name);
            if (i >= 0) return _DataInputs[i];
            i = IndexOf(_DataOutputs, name);
            if (i >= 0) return _DataOutputs[i];
            i = IndexOf(_DataInternals, name);
            if (i >= 0) return _DataInternals[i];
            return null;
        }

        public string GetLastDataType(String side)
        {
            string sDT = "BOOL";

            switch (side)
            {
                case "DI":
                    if (_DataInputs.Count > 0)
                        sDT = _DataInputs[_DataInputs.Count - 1].DataType;
                    break;
                case "DO":
                    if (_DataOutputs.Count > 0)
                        sDT = _DataOutputs[_DataOutputs.Count - 1].DataType;
                    break;
                case "IV":
                    if (_DataInternals.Count > 0)
                        sDT = _DataInternals[_DataInternals.Count - 1].DataType;
                    break;
            }

            return sDT;
        }

        // create a new Socket
        public void AddSocket(String side, String name, String color, string datatype)
        {
            SIFBInterfaceSocket s = new SIFBInterfaceSocket() { Unit = this, Side = side, Index = Find(side).Count, Name = name, Color = color };
            s.DataType = datatype;
            Add(Find(side), s);
        }

        // insert an existing Socket
        public void InsertSocket(SIFBInterfaceSocket sock)
        {
            Add(Find(sock.Side), sock);
        }

        // remove an existing Socket
        public void RemoveSocket(SIFBInterfaceSocket sock)
        {
            Remove(Find(sock.Side), sock.Name);
        }

        public ObservableCollection<SIFBInterfaceSocket> Find(String side)
        {
            switch (side)
            {
                case "EI": return _EventInputs;
                case "EO": return _EventOutputs;
                case "DI": return _DataInputs;
                case "DO": return _DataOutputs;
                case "IV": return _DataInternals;
            }
            return null;
        }

        private void Add(ObservableCollection<SIFBInterfaceSocket> socks, SIFBInterfaceSocket s)
        {
            // don't do anything if it's already there
            if (socks.Contains(s)) return;
            // update the collection
            socks.Insert(s.Index, s);
            int n = socks.Count;
            for (int j = 0; j < n; j++)
            {
                socks[j].Index = j;  // always update the Socket.Index
            }
            // notify about the change
            RaisePropertyChanged("AddedSocket", s, null);
        }

        private int IndexOf(ObservableCollection<SIFBInterfaceSocket> socks, String name)
        {
            for (int i = 0; i < socks.Count; i++)
            {
                if (socks[i].Name == name) return i;
            }
            return -1;
        }

        private void Remove(ObservableCollection<SIFBInterfaceSocket> socks, String name)
        {
            int i = IndexOf(socks, name);
            if (i >= 0)
            {  // don't do anything unless it's actually removed
                SIFBInterfaceSocket s = socks[i];
                // update the collection
                socks.RemoveAt(i);
                // always update the Socket.Index
                for (int j = 0; j < socks.Count; j++) socks[j].Index = j;
                // notify about the change
                RaisePropertyChanged("RemovedSocket", s, null);
            }
        }
    }

    [Serializable]
    public class SIFBInterfaceSocket : ICloneable, INotifyPropertyChanged, IChangeDataValue
    {
        // implement ICloneable for copying
        public Object Clone()
        {
            return MemberwiseClone() as Socket;
        }

        // implement INotifyPropertyChanged for data-binding
        [field: NonSerializedAttribute()]
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(String pname, Object oldval, Object newval)
        {
            ModelChangedEventArgs e = new ModelChangedEventArgs(pname, this, oldval, newval);
            // implement INotifyPropertyChanged:
            if (this.PropertyChanged != null) this.PropertyChanged(this, e);
            // implement support for model and undo/redo:
            if (this.Unit != null) this.Unit.OnPropertyChanged(e);
        }

        // implement IChangeDataValue for undo/redo
        public void ChangeDataValue(ModelChangedEventArgs e, bool undo)
        {
            switch (e.PropertyName)
            {
                case "Color": this.Color = (String)e.GetValue(undo); break;
                default: throw new NotImplementedException("Var change: " + e.ToString());
            }
        }

        public XElement MakeXElement()
        {
            XElement e = new XElement("Var");
            e.Add(XHelper.Attribute("Name", this.Name, ""));
            e.Add(XHelper.Attribute("Type", this.Side, ""));
            e.Add(XHelper.Attribute("DataType", this.DataType, ""));
            e.Add(XHelper.Attribute("Index", this.Index, 0));
            e.Add(XHelper.Attribute("Color", this.Color, "Black"));
            string sWith = "";
            foreach(string sWithVar in WithVars)
            {
                sWith += sWithVar + ",";
            }
            if (sWith.Length > 0)
            {
                sWith = sWith.Substring(0, sWith.Length - 1);
            }
            e.Add(XHelper.Attribute("With", sWith, ""));
            e.Add(XHelper.Attribute("Comment", this.Comment, ""));
            e.Add(XHelper.Attribute("InitialValue", InitValue, ""));
            e.Add(XHelper.Attribute("ArraySize", ArraySize, ""));
            return e;
        }

        public SIFBInterfaceSocket LoadFromXElement(XElement e)
        {
            if (e == null) return this;
            this.Name = XHelper.Read("Name", e, "");
            this.Side = XHelper.Read("Type", e, "");
            this.DataType = XHelper.Read("DataType", e, "");
            this.Index = XHelper.Read("Index", e, 0);
            this.Color = XHelper.Read("Color", e, "Black");
            foreach (string sWith in XHelper.Read("With", e, "").Split(','))
            {
                if (sWith != "")
                    this.WithVars.Add(sWith);
            }
            this.ConnPort = "";
            this.Comment = XHelper.Read("Comment", e, "");
            this.InitValue = XHelper.Read("InitialValue", e, "");
            this.ArraySize = XHelper.Read("ArraySize", e, "");

            return this;
        }

        // these properties aren't expected to change after initialization
        public SIFBInterfaceUnit Unit { get; set; }  // parent pointer
        public String Name { get; set; }
        public String Side { get; set; }
        public String DataType { get; set; }
        public String ConnPort { get; set; }
        public String Comment { get; set; }
        public String InitValue { get; set; }
        public String ArraySize { get; set; }
        public int Index { get; set; }
        
        // these property may change dynamically, so they implement change notification        
        public String Color
        {
            get { return _Color; }
            set
            {
                if (_Color != value)
                {
                    String old = _Color;
                    _Color = value;
                    RaisePropertyChanged("Color", old, value);
                }
            }
        }
        private String _Color = "Black";
        
        public bool Visible
        {
            get { return _Visible; }
            set
            {
                if (_Visible != value)
                {
                    bool old = _Visible;
                    _Visible = value;
                    RaisePropertyChanged("Visible", old, value);
                }
            }
        }
        private bool _Visible = false;

        public bool Associated
        {
            get { return _Associated; }
            set
            {
                if (_Associated != value)
                {
                    bool old = _Associated;
                    _Associated = value;
                    RaisePropertyChanged("Associated", old, value);
                }
            }
        }
        private bool _Associated = false;

        public ObservableCollection<string> WithVars
        {
            get { return _WithVars; }
            set
            {
                if (_WithVars != value)
                {
                    ObservableCollection<string> old = _WithVars;
                    _WithVars = value;
                    RaisePropertyChanged("WithVars", old, value);
                }
            }
        }
        private ObservableCollection<string> _WithVars = new ObservableCollection<string>();
    }

}
