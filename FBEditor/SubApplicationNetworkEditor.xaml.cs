﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Linq;
using Northwoods.GoXam;
using Northwoods.GoXam.Model;

namespace FBEditor
{
    /// <summary>
    /// Interaction logic for SubApplicationNetworkEditor.xaml
    /// </summary>
    public partial class SubApplicationNetworkEditor : UserControl
    {
        public SubApplicationNetworkEditor()
        {
            InitializeComponent();

            newInstanceWindow.SetParent(FBDiagram);
            newInstanceWindow.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            newInstanceWindow.VerticalAlignment = System.Windows.VerticalAlignment.Top;
        }

        public void ReloadIOList(XElement xFBType)
        {
            //GetXML() then Replace Interface part from xFBType
            XElement xData = GetXML();

            xFBType.Element("SubAppNetwork").Remove();
            xFBType.Add(xData);

            LoadXML(xFBType);
        }

        // only use the saved route points after the layout has completed,
        // because links will get the default routing
        private void UpdateRoutes(object sender, DiagramEventArgs e)
        {
            // just set the Route points once per Load
            FBDiagram.LayoutCompleted -= UpdateRoutes;
            foreach (Link link in FBDiagram.Links)
            {
                Wire wire = link.Data as Wire;
                if (wire != null && wire.Points != null && wire.Points.Count() > 1)
                {
                    link.Route.Points = (IList<Point>)wire.Points;
                }
            }

            FBDiagram.PartManager.UpdatesRouteDataPoints = true;  // OK for CustomPartManager to update Transition.Points automatically
        }

        public void LoadXML(XElement xFBType)
        {
            // because we cannot data-bind the Route.Points property,
            // we use a custom PartManager to read/write the Wire.Points data property
            FBDiagram.PartManager = new CustomPartManager();
           
            var model = new CustomModel();
            
            // set the Route.Points after nodes have been built and the layout has finished
            FBDiagram.LayoutCompleted += UpdateRoutes;
            IDELibrary.FBXMLConverter converter = new IDELibrary.FBXMLConverter();
            XElement xData = null;
            converter.generateSubAppDiagramFromXElement(ref xData, xFBType);
            model.Load<Unit, Wire>(xData, "Node", "Conn");
            model.Modifiable = true;
            model.HasUndoManager = true;

            FBDiagram.Model = model;
        }

        public XElement GetXML()
        {
            var model = FBDiagram.Model as CustomModel;
            if (model == null) return null;
            // copy the Route.Points into each Transition data
            foreach (Link link in FBDiagram.Links)
            {
                Wire wire = link.Data as Wire;
                if (wire != null)
                {
                    wire.Points = new List<Point>(link.Route.Points);
                }
            }
            XElement root = model.Save<Unit, Wire>("Diagram", "Node", "Conn");
            model.IsModified = false;
            IDELibrary.FBXMLConverter converter = new IDELibrary.FBXMLConverter();
            return converter.generateAPPFromFBDiagram(root);
        }

        private void Button_CreateNewFBI_Click(object sender, RoutedEventArgs e)
        {
            newInstanceWindow.Margin = new Thickness(150, 150, 0, 0);
            string str = newInstanceWindow.ShowHandlerDialog();
            if (str == "") return;
            string sFBType = str.Split('/')[0];
            string sFBI = str.Split('/')[1];
            Unit nUnit = new Unit();
            Random rnd = new Random();
            nUnit.Key = "FBI" + rnd.Next(5000, 100000).ToString();
            nUnit.Text = sFBI;
            nUnit.Category = "FBTemplate";
            nUnit.Type = sFBType;
            nUnit.Location = new Point(150, 150);
            //Load File and Add all I/Os
            string sFileToLoad = searchFileByName("FBLib\\", sFBType + ".fbt");
            if (sFileToLoad != "")
            {
                XDocument xDoc = XDocument.Load(sFileToLoad);
                if (xDoc.Root.Descendants("EventInputs").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("EventInputs").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("EI", xIO.Attribute("Name").Value, "Black");
                    }
                }
                if (xDoc.Root.Descendants("EventOutputs").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("EventOutputs").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("EO", xIO.Attribute("Name").Value, "Black");
                    }
                }
                if (xDoc.Root.Descendants("InputVars").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("InputVars").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("DI", xIO.Attribute("Name").Value, "Black");
                    }
                }
                if (xDoc.Root.Descendants("OutputVars").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("OutputVars").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("DO", xIO.Attribute("Name").Value, "Black");
                    }
                }
                if (xDoc.Root.Descendants("Sockets").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("Sockets").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("SOCKET", xIO.Attribute("Name").Value, "Black");
                    }
                }
                if (xDoc.Root.Descendants("Plugs").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("Plugs").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("PLUG", xIO.Attribute("Name").Value, "Black");
                    }
                }
            }
            else
            {
                sFileToLoad = searchFileByName("FBLib\\", sFBType + ".app");
                if (sFileToLoad == "") return;
                XDocument xDoc = XDocument.Load(sFileToLoad);
                if (xDoc.Root.Descendants("SubAppEventInputs").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("SubAppEventInputs").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("EI", xIO.Attribute("Name").Value, "Black");
                    }
                }
                if (xDoc.Root.Descendants("SubAppEventOutputs").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("SubAppEventOutputs").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("EO", xIO.Attribute("Name").Value, "Black");
                    }
                }
                if (xDoc.Root.Descendants("InputVars").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("InputVars").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("DI", xIO.Attribute("Name").Value, "Black");
                    }
                }
                if (xDoc.Root.Descendants("OutputVars").FirstOrDefault() != null)
                {
                    foreach (XElement xIO in xDoc.Root.Descendants("OutputVars").FirstOrDefault().Elements())
                    {
                        nUnit.AddSocket("DO", xIO.Attribute("Name").Value, "Black");
                    }
                }
            }
            var model = FBDiagram.Model as CustomModel;
            if (model == null) return;
            model.AddNode(nUnit);
        }

        private string searchFileByName(string sDIR, string sFile)
        {
            string sFilePath = "";
            //Find Source File in the Root Folder
            //Search through Library
            string[] fileList = Directory.GetFiles(sDIR, sFile);
            if (fileList.Count() > 0)
            {
                foreach (string sfile in fileList)
                {
                    if (!sfile.EndsWith(".cpp") && !sfile.EndsWith(".dll"))
                    {
                        sFilePath = sfile;
                        break;
                    }
                }
            }
            else
            {
                foreach (string SubDIR in Directory.GetDirectories(sDIR))
                {
                    sFilePath = searchFileByName(SubDIR, sFile);
                    if (sFilePath != "")
                    {
                        break;
                    }

                }
            }
            return sFilePath;
        }
    }
}