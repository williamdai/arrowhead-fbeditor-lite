﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Xml.Linq;

namespace FBEditor
{
    /// <summary>
    /// Interaction logic for SIFBServiceSequenceDiagram.xaml
    /// </summary>
    public partial class SIFBServiceSequenceDiagram : UserControl
    {
        private XElement xInterface;

        public SIFBServiceSequenceDiagram()
        {
            InitializeComponent();

            nVM = new SIFBSSViewModel();
        }

        public void LoadXML(XElement xFBType)
        {
            if (xFBType.Element("InterfaceList") != null)
            {
                xInterface = xFBType.Element("InterfaceList");
            }

            if (xFBType.Element("Service") != null)
            {
                nVM.index = 0;
                nVM.RightInterface = "Resource";
                nVM.LeftInterface = "FB";
                if (xFBType.Element("Service").Attribute("Comment") != null)
                    nVM.Comment = xFBType.Element("Service").Attribute("Comment").Value;

                nVM.xServices = xFBType.Element("Service");
                nVM.xServices.Attribute("RightInterface").Value = "Resource";
                nVM.xServices.Attribute("LeftInterface").Value = "FB";
                //Add ID for each Service Sequences Transaction
                foreach(XElement xSST in nVM.xServices.Descendants("ServiceTransaction"))
                {
                    xSST.Add(new XAttribute("Name", "Transaction" + (nVM.index++).ToString()));
                }

                //Load All Service Sequences
                foreach (XElement xSS in nVM.xServices.Elements("ServiceSequence"))
                {
                    SSTClass nSS = new SSTClass();
                    nSS.Name = xSS.Attribute("Name").Value;
                    nSS.OriginName = xSS.Attribute("Name").Value;
                    nSS.SSTList = new List<string>();
                    foreach(XElement xSST in xSS.Elements("ServiceTransaction"))
                    {
                        nSS.SSTList.Add(xSST.Attribute("Name").Value);
                    }
                    nVM.SSList.Add(nSS);
                }

                nVM.ALGList = new List<string>();
                foreach (XElement nALG in xFBType.Element("Service").Elements("Algorithm"))
                {
                    nVM.ALGList.Add(nALG.Attribute("Name").Value);
                }

                this.DataContext = null;
                this.DataContext = nVM;
            }
        }

        public XElement getXML()
        {
            //Update SST if open
            if (nVM.ActiveService != "")
            {
                bool bFound = false;
                for (int i = 0; i < nVM.xServices.Elements("ServiceSequence").Count(); i++)
                {
                    for (int j = 0; j < nVM.xServices.Elements("ServiceSequence").ElementAt(i).Elements("ServiceTransaction").Count(); j++)
                    {
                        if (nVM.xServices.Elements("ServiceSequence").ElementAt(i).Elements("ServiceTransaction").ElementAt(j).Attribute("Name").Value == nVM.ActiveService)
                        {
                            nVM.xServices.Elements("ServiceSequence").ElementAt(i).Elements("ServiceTransaction").ElementAt(j).ReplaceWith(STList.GetXML());
                            bFound = true;
                            break;
                        }
                    }

                    if (bFound) break;
                }
            }

            if (nVM.xServices.Attribute("Comment") != null)
                nVM.xServices.Attribute("Comment").Value = nVM.Comment;
            else
                nVM.xServices.Add(new XAttribute("Comment", nVM.Comment));

            foreach (SSTClass nSST in nVM.SSList)
            {
                if (nSST.OriginName != nSST.Name)
                {
                    //Update Name
                    nVM.xServices.Elements("ServiceSequence").Where(item => item.Attribute("Name").Value == nSST.OriginName).FirstOrDefault().Attribute("Name").Value = nSST.Name;
                    nSST.OriginName = nSST.Name;
                }
                //Update Comment
                try
                {
                    nVM.xServices.Elements("ServiceSequence").Where(item => item.Attribute("Name").Value == nSST.Name).FirstOrDefault().Attribute("Comment").Value = nSST.Comment;
                }
                catch (Exception)
                { }
            }

            return nVM.xServices;
        }

        public void UpdateEventList(XElement xFBType)
        {
            if (xFBType.Element("InterfaceList") != null)
            {
                xInterface = xFBType.Element("InterfaceList");
                STList.ReloadInterface(xInterface);
            }
        }

        public void UpdateServiceList(XElement xServices)
        {
            nVM.ALGList = new List<string>();
            foreach (XElement nALG in xServices.Elements("Algorithm"))
            {
                nVM.ALGList.Add(nALG.Attribute("Name").Value);
            }
            STList.ReloadALG(xServices);
        }

        private void Image_EditSST_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            string sSSTName = (sender as Label).Tag.ToString();

            bool bFound = false;
            if (sSSTName != nVM.ActiveService)
            {
                for (int i = 0; i < nVM.xServices.Elements("ServiceSequence").Count(); i++)
                {
                    for (int j = 0; j < nVM.xServices.Elements("ServiceSequence").ElementAt(i).Elements("ServiceTransaction").Count(); j++)
                    {
                        if (nVM.xServices.Elements("ServiceSequence").ElementAt(i).Elements("ServiceTransaction").ElementAt(j).Attribute("Name").Value == nVM.ActiveService)
                        {
                            nVM.xServices.Elements("ServiceSequence").ElementAt(i).Elements("ServiceTransaction").ElementAt(j).ReplaceWith(STList.GetXML());
                            bFound = true;
                            break;
                        }
                    }

                    if (bFound) break;
                }
            }

            nVM.ActiveService = sSSTName;
            STList.LoadXML(nVM.xServices, xInterface, nVM.ActiveService);
            STList.Visibility = System.Windows.Visibility.Visible;
        }

        private void Image_DeleteSST_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            string sSSTName = (sender as Label).Tag.ToString();

            int indexI = -1;
            int indexJ = -1;
            for (int i = 0; i < nVM.SSList.Count; i++)
            {
                for(int j = 0; j < nVM.SSList.ElementAt(i).SSTList.Count; j++)
                {
                    if (nVM.SSList.ElementAt(i).SSTList[j] == sSSTName)
                    {
                        indexI = i;
                        indexJ = j;
                        break;
                    }
                }
            }

            if (indexI > -1 && indexJ > -1)
            {
                nVM.SSList.ElementAt(indexI).SSTList.RemoveAt(indexJ);

                foreach(XElement xSST in nVM.xServices.Descendants("ServiceTransaction"))
                {
                    if (xSST.Attribute("Name").Value == sSSTName)
                    {
                        xSST.Remove();
                        break;
                    }
                }
            }

            if (nVM.ActiveService == sSSTName)
            {
                nVM.ActiveService = "";
                STList.Visibility = System.Windows.Visibility.Hidden;
            }

            this.DataContext = null;
            this.DataContext = nVM;
        }

        private void Image_AddSST_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            string sSSName = (sender as Label).Tag.ToString();
            for (int i = 0; i < nVM.SSList.Count; i++)
            {
                if (nVM.SSList.ElementAt(i).Name == sSSName)
                {
                    nVM.SSList.ElementAt(i).SSTList.Add("Transaction" + (nVM.index++).ToString());
                    foreach(XElement xSS in nVM.xServices.Elements("ServiceSequence"))
                    {
                        if (xSS.Attribute("Name").Value == sSSName)
                        {
                            XElement xSST = new XElement("ServiceTransaction", new XAttribute("Name",  "Transaction" + (nVM.index - 1).ToString()));
                            xSS.Add(xSST);
                            break;
                        }
                    }
                    break;
                }
            }

            this.DataContext = null;
            this.DataContext = nVM;
        }

        private void Image_Delete_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            string sSSName = (sender as Label).Tag.ToString();
            
            //Remove this node and reload EC Algorithm Editor
            foreach (XElement xSS in nVM.xServices.Elements("ServiceSequence"))
            {
                if (xSS.Attribute("Name").Value == sSSName)
                {
                    xSS.Remove();
                    break;
                }
            }

            int index = -1;
            for (int i = 0; i < nVM.SSList.Count; i++)
            {
                if (nVM.SSList.ElementAt(i).Name == sSSName)
                {
                    index = i;
                    break;
                }
            }

            if (index > -1)
            {
                for (int j = 0; j < nVM.SSList.ElementAt(index).SSTList.Count; j++)
                {
                    if (nVM.SSList.ElementAt(index).SSTList[j] == nVM.ActiveService)
                    {
                        nVM.ActiveService = "";
                        STList.Visibility = System.Windows.Visibility.Hidden;
                        break;
                    }
                }
                nVM.SSList.RemoveAt(index);
            }

            this.DataContext = null;
            this.DataContext = nVM;
        }

        private void Image_New_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            XElement xSS = new XElement("ServiceSequence");
            XAttribute xSSName = new XAttribute("Name", "ServiceSequence" + (nVM.xServices.Elements("ServiceSequence").Count() + 1).ToString());
            XAttribute xSSComment = new XAttribute("Comment", "");
            xSS.Add(xSSName, xSSComment);
            nVM.xServices.Add(xSS);
            SSTClass nSST = new SSTClass();
            nSST.Name = xSSName.Value;
            nVM.SSList.Add(nSST);
            this.DataContext = null;
            this.DataContext = nVM;
        }

    }

    public class SIFBSSViewModel : INotifyPropertyChanged
    {
        public SIFBSSViewModel()
        { }

        public XElement xServices = new XElement("Service");
        public string ActiveService = "";
        public string RightInterface = "Resource";
        public string LeftInterface = "FB";
        public int index = 0;

        public List<string> ALGList
        {
            get { return _ALGList; }
            set
            {
                if (_ALGList != value)
                {
                    List<string> old = _ALGList;
                    _ALGList = value;
                    RaisePropertyChangedEvent("ALGList");
                }
            }
        }
        private List<string> _ALGList = new List<string>();

        public List<SSTClass> SSList
        {
            get { return _SSList; }
            set
            {
                if (_SSList != value)
                {
                    List<SSTClass> old = _SSList;
                    _SSList = value;
                    RaisePropertyChangedEvent("SSList");
                }
            }
        }
        private List<SSTClass> _SSList = new List<SSTClass>();

        public string Comment
        {
            get { return _Comment; }
            set
            {
                if (_Comment != value)
                {
                    string old = _Comment;
                    _Comment = value;
                    RaisePropertyChangedEvent("Comment");
                }
            }
        }
        private string _Comment = "";

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChangedEvent(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class SSTClass : INotifyPropertyChanged
    {
        public SSTClass()
        {
            OriginName = "";
            Name = "";
            Comment = "";
            SSTList = new List<string>();
        }

        public string OriginName { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        
        public List<string> SSTList
        {
            get { return _SSTList; }
            set
            {
                if (_SSTList != value)
                {
                    List<string> old = _SSTList;
                    _SSTList = value;
                    RaisePropertyChangedEvent("SSTList");
                }
            }
        }
        private List<string> _SSTList = new List<string>();

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChangedEvent(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
