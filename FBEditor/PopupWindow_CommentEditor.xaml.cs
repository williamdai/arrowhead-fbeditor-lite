﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading;

namespace FBEditor
{
    /// <summary>
    /// Interaction logic for PopupWindow_CommentEditor.xaml
    /// </summary>
    public partial class PopupWindow_CommentEditor : UserControl
    {
        private bool _hideRequest = false;
        private bool bChanged = false;
        private UIElement _parent;
        private string sOriginalComment;

        public PopupWindow_CommentEditor()
        {
            InitializeComponent();

            Visibility = Visibility.Hidden;
        }

        public void SetParent(UIElement parent)
        {
            _parent = parent;
        }

        public string ShowHandlerDialog(string sTitle, string sComment)
        {
            textblock_Title.Text = "Edit Comment for " + sTitle;
            textbox_Comment.Text = sComment;
            this.sOriginalComment = sComment;
            Visibility = Visibility.Visible;

            _parent.IsEnabled = false;

            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }

                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            if (bChanged)
                return textbox_Comment.Text;

            return sOriginalComment;
        }
    
        private void HideHandlerDialog()
        {
            _hideRequest = true;
            Visibility = Visibility.Hidden;
            _parent.IsEnabled = true;
        }

        private void button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            bChanged = false;
            HideHandlerDialog();
        }

        private void button_OK_Click(object sender, RoutedEventArgs e)
        {
            bChanged = true;
            HideHandlerDialog();
        }
    }
}
