﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Northwoods.GoXam;
using Northwoods.GoXam.Model;
using Northwoods.GoXam.Tool;

namespace FBEditor
{
    /// <summary>
    /// Interaction logic for SIFBServiceTransactionsList.xaml
    /// </summary>
    public partial class SIFBServiceTransactionsList : UserControl
    {
        public SIFBServiceTransactionsList()
        {
            InitializeComponent();
        }

        public XElement GetXML()
        {
            XElement xSST = new XElement("ServiceTransaction");
            XAttribute xSSName = new XAttribute("Name", nVM.Name);
            xSST.Add(xSSName);

            if (nVM.InputPrimitiveInterface != "" || nVM.InputPrimitiveEvent != "" || nVM.InputPrimitiveParameters != "")
            {
                XElement xIP = new XElement("InputPrimitive",
                    new XAttribute("Interface", nVM.InputPrimitiveInterface),
                    new XAttribute("Event", nVM.InputPrimitiveEvent),
                    new XAttribute("Parameters", nVM.InputPrimitiveParameters));
                xSST.Add(xIP);
            }

            foreach (OutputPrimitive nOP in nVM.OutputPrimitives)
            {
                XElement xOP = new XElement("OutputPrimitive",
                    new XAttribute("Interface", nOP.Interface),
                    new XAttribute("Event", nOP.Event),
                    new XAttribute("Parameters", nOP.Parameters));
                xSST.Add(xOP);
            }

            return xSST;
        }

        public void ReloadInterface(XElement xInterface)
        {
            nVM.EIList = new List<string>();
            nVM.EIList.Add("");
            foreach (XElement nItem in xInterface.Element("EventInputs").Elements("Event"))
            {
                nVM.EIList.Add(nItem.Attribute("Name").Value);
            }

            nVM.EOList = new List<string>();
            nVM.EOList.Add("");
            foreach (XElement nItem in xInterface.Element("EventOutputs").Elements("Event"))
            {
                nVM.EOList.Add(nItem.Attribute("Name").Value);
            }

            nVM.DIList = new List<string>();
            nVM.DIList.Add("");
            foreach (XElement nItem in xInterface.Element("InputVars").Elements("VarDeclaration"))
            {
                nVM.DIList.Add(nItem.Attribute("Name").Value);
            }

            nVM.DOList = new List<string>();
            nVM.DOList.Add("");
            foreach (XElement nItem in xInterface.Element("OutputVars").Elements("VarDeclaration"))
            {
                nVM.DOList.Add(nItem.Attribute("Name").Value);
            }

            this.DataContext = null;
            this.DataContext = nVM;
        }

        public void ReloadALG(XElement xService)
        {
            nVM.ALGList = new List<string>();
            nVM.ALGList.Add("");
            foreach (XElement nItem in xService.Elements("Algorithm"))
            {
                nVM.ALGList.Add(nItem.Attribute("Name").Value);
            }

            this.DataContext = null;
            this.DataContext = nVM;
        }

        public void LoadXML(XElement xService, XElement xInterface, string activeSS)
        {
            nVM = new ServiceSequenceViewModel();

            if (xInterface != null)
            {
                ReloadInterface(xInterface);
            }

            if (xService != null)
            {
                ReloadALG(xService);
            }

            nVM.Name = activeSS;
            if (xService.Descendants("ServiceTransaction") != null)
            {
                foreach (XElement xSST in xService.Descendants("ServiceTransaction"))
                {
                    if (xSST.Attribute("Name").Value == activeSS)
                    {
                        if (xSST.Element("InputPrimitive") != null)
                        {
                            nVM.InputPrimitiveInterface = xSST.Element("InputPrimitive").Attribute("Interface").Value;
                            nVM.InputPrimitiveEvent = xSST.Element("InputPrimitive").Attribute("Event").Value;
                            if (xSST.Element("InputPrimitive").Attribute("Parameters") != null)
                                nVM.InputPrimitiveParameters = xSST.Element("InputPrimitive").Attribute("Parameters").Value;
                        }

                        if (xSST.Element("OutputPrimitive") != null)
                        {
                            foreach (XElement nOP in xSST.Elements("OutputPrimitive"))
                            {
                                OutputPrimitive nOPM = new OutputPrimitive();
                                nOPM.Index = nVM.index;
                                nVM.index++;
                                nOPM.Interface = nOP.Attribute("Interface").Value;
                                nOPM.Event = nOP.Attribute("Event").Value;
                                if (nOP.Attribute("Parameters") != null)
                                    nOPM.Parameters = nOP.Attribute("Parameters").Value;
                                nVM.OutputPrimitives.Add(nOPM);
                            }
                        }
                        break;
                    }
                }
            }

            nVM.ListInterfaces = new List<string>() { "", "FB", "Resource" };
                    
            this.DataContext = null;
            this.DataContext = nVM;
            LoadDataForSSD();
        }

        private void LoadDataForSSD()
        {
            var model = new GraphLinksModel<NodeData, String, String, LinkData>();
            model.NodesSource = new ObservableCollection<NodeData>() {
            new NodeData() { Key="FB", Text="FBNetwork", IsSubGraph=true, Location=new Point(0, 0) },
            new NodeData() { Key="SIFB", Text="SIFB", IsSubGraph=true, Location=new Point(70, 0) },
            new NodeData() { Key="Resource", Text="Resource", IsSubGraph=true, Location=new Point(140, 0) },
            };
            
            ObservableCollection<LinkData> nList = new ObservableCollection<LinkData>();
            if (nVM.InputPrimitiveInterface != "")
            {
                LinkData nLData = new LinkData();
                nLData.From = nVM.InputPrimitiveInterface;
                nLData.To = "SIFB";
                nLData.Text = nVM.InputPrimitiveEvent;
                nLData.Time = 1;
                nLData.Duration = 2;
                nList.Add(nLData);
            }

           int i = 1;
           foreach (OutputPrimitive nOP in nVM.OutputPrimitives)
            {
                if (nOP.Interface != "")
                {
                    LinkData nLData = new LinkData();
                    nLData.From = "SIFB";
                    nLData.To = nOP.Interface;
                    nLData.Text = nOP.Event;
                    nLData.Time = 1 + i * 2;
                    nLData.Duration = 2;
                    nList.Add(nLData);
                    i++;
                }
            }
            model.LinksSource = nList;
            
            SSDDiagram.Model = model;

            // add an Activity node for each Message recipient
            model.Modifiable = true;
            double max = 0;
            foreach (LinkData d in model.LinksSource)
            {
                var grp = model.FindNodeByKey(d.To);
                var act = new NodeData()
                {
                    SubGraphKey = d.To,
                    Location = new Point(grp.Location.X, BarRoute.ConvertTimeToY(d.Time) - BarRoute.ActivityInset),
                    Length = d.Duration * BarRoute.MessageSpacing + BarRoute.ActivityInset * 2,
                };
                model.AddNode(act);
                max = Math.Max(max, act.Location.Y + act.Length);
            }
            // now make sure all of the lifelines are long enough
            foreach (NodeData d in model.NodesSource)
            {
                if (d.IsSubGraph) d.Length = max - BarRoute.LineStart + BarRoute.LineTrail;
            }
            model.Modifiable = false;
        }

        private void Image_OPNew_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            OutputPrimitive nOP = new OutputPrimitive();
            nOP.Interface = "";
            nOP.Event = "";
            nOP.Parameters = "";
            nOP.Index = nVM.index;
            nVM.index++;
            nVM.OutputPrimitives.Add(nOP);
            this.DataContext = null;
            this.DataContext = nVM;
        }

        private void Image_OPDelete_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            int sIndex = (int)(sender as Label).Tag;
           
            int index = -1;
            for(int i = 0; i < nVM.OutputPrimitives.Count; i++)
            {
                if (nVM.OutputPrimitives[i].Index == sIndex)
                {
                    index = i;
                    break;
                }
            }
            if (index > -1)
            {
                nVM.OutputPrimitives.RemoveAt(index);
                this.DataContext = null;
                this.DataContext = nVM;
                LoadDataForSSD();
            }
        }

        private void combobox_Interface_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((ComboBox)sender).SelectedItem == null) return;
            string strValue = ((ComboBox)sender).SelectedItem.ToString();
            string sIndex = ((ComboBox)sender).Tag.ToString();
            ComboBox foundTextBox = FindChild<ComboBox>(this, "combobox_OutputEvent", sIndex);
            if (foundTextBox == null) return;
            if (strValue == "FB")
            {
                foundTextBox.ItemsSource = nVM.EOList;
            }
            else if (strValue == "Resource")
            {
                foundTextBox.ItemsSource = nVM.ALGList;
            }
            LoadDataForSSD();
        }

        private void combobox_IInterface_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((ComboBox)sender).SelectedItem == null) return;
            string strValue = ((ComboBox)sender).SelectedItem.ToString();
            if (strValue == "FB")
            {
                combobox_InputEvent.ItemsSource = nVM.EIList;
            }
            else if (strValue == "Resource")
            {
                combobox_InputEvent.ItemsSource = nVM.ALGList;
            }
            LoadDataForSSD();
        }

        public static T FindChild<T>(DependencyObject parent, string Name, string childName) where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindChild<T>(child, Name, childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Tag != null && frameworkElement.Name != null)
                    {
                        if (frameworkElement.Tag.ToString() == childName && frameworkElement.Name == Name)
                        {
                            // if the child's name is of the request name
                            foundChild = (T)child;
                            break;
                        }
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }

        private void combobox_InputEvent_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LoadDataForSSD();
        }

        private void combobox_OutputEvent_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LoadDataForSSD();
        }
    }

    public class ServiceSequenceViewModel : INotifyPropertyChanged
    {
        public string Name { get; set; }
        public string InputPrimitiveInterface { get; set; }
        public string InputPrimitiveEvent { get; set; }
        public string InputPrimitiveParameters { get; set; }
        public int index { get; set; }

        public ServiceSequenceViewModel()
        {
            Name = "";
            InputPrimitiveInterface = "";
            InputPrimitiveEvent = "";
            InputPrimitiveParameters = "";
            index = -1;
        }

        public List<string> STList
        {
            get { return _STList; }
            set
            {
                if (_STList != value)
                {
                    List<string> old = _STList;
                    _STList = value;
                    RaisePropertyChangedEvent("STList");
                }
            }
        }
        private List<string> _STList = new List<string>();

        public List<string> EIList
        {
            get { return _EIList; }
            set
            {
                if (_EIList != value)
                {
                    List<string> old = _EIList;
                    _EIList = value;
                    RaisePropertyChangedEvent("EIList");
                }
            }
        }
        private List<string> _EIList = new List<string>();

        public List<string> EOList
        {
            get { return _EOList; }
            set
            {
                if (_EOList != value)
                {
                    List<string> old = _EOList;
                    _EOList = value;
                    RaisePropertyChangedEvent("EOList");
                }
            }
        }
        private List<string> _EOList = new List<string>();

        public List<string> DIList
        {
            get { return _DIList; }
            set
            {
                if (_DIList != value)
                {
                    List<string> old = _DIList;
                    _DIList = value;
                    RaisePropertyChangedEvent("DIList");
                }
            }
        }
        private List<string> _DIList = new List<string>();

        public List<string> DOList
        {
            get { return _DOList; }
            set
            {
                if (_DOList != value)
                {
                    List<string> old = _DOList;
                    _DOList = value;
                    RaisePropertyChangedEvent("DOList");
                }
            }
        }
        private List<string> _DOList = new List<string>();

        public List<string> ALGList
        {
            get { return _ALGList; }
            set
            {
                if (_ALGList != value)
                {
                    List<string> old = _ALGList;
                    _ALGList = value;
                    RaisePropertyChangedEvent("ALGList");
                }
            }
        }
        private List<string> _ALGList = new List<string>();

        public List<string> ListInterfaces
        {
            get { return _ListInterfaces; }
            set
            {
                if (_ListInterfaces != value)
                {
                    List<string> old = _ListInterfaces;
                    _ListInterfaces = value;
                    RaisePropertyChangedEvent("ListInterfaces");
                }
            }
        }
        private List<string> _ListInterfaces = new List<string>();

        public List<OutputPrimitive> OutputPrimitives
        {
            get { return _OutputPrimitives; }
            set
            {
                if (_OutputPrimitives != value)
                {
                    List<OutputPrimitive> old = _OutputPrimitives;
                    _OutputPrimitives = value;
                    RaisePropertyChangedEvent("OutputPrimitives");
                }
            }
        }
        private List<OutputPrimitive> _OutputPrimitives = new List<OutputPrimitive>();

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChangedEvent(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class OutputPrimitive
    {
        public string Interface { get; set; }
        public string Event { get; set; }
        public string Parameters { get; set; }
        public int Index { get; set; }
    }

    // Customize the Route so that it always goes straight horizontally.
    public class BarRoute : Route
    {
        public override Point GetLinkPoint(Node node, FrameworkElement port, Spot spot, bool from,
                                           bool ortho, Node othernode, FrameworkElement otherport)
        {
            Point p = node.GetElementPoint(port, Spot.Center);
            Rect r = new Rect(node.GetElementPoint(port, Spot.TopLeft),
                              node.GetElementPoint(port, Spot.BottomRight));
            Point op = othernode.GetElementPoint(otherport, Spot.Center);

            LinkData data = this.Link.Data as LinkData;
            double y = (data != null ? ConvertTimeToY(data.Time) : 0);

            bool right = op.X > p.X;
            double dx = ActivityWidth / 2;
            if (from)
            {
                Group grp = node as Group;
                if (grp != null)
                {
                    // see if there is an Activity Node at this point -- if not, connect the link directly with the Group's lifeline
                    bool found = false;
                    foreach (Node mem in grp.MemberNodes)
                    {
                        NodeData d = mem.Data as NodeData;
                        if (d != null && d.Location.Y <= y && y <= d.Location.Y + d.Length)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found) dx = 0;
                }
            }
            double x = right ? p.X + dx : p.X - dx;
            return new Point(x, y);
        }

        protected override double GetLinkDirection(Node node, FrameworkElement port, Point linkpoint, Spot spot,
                                                   bool from, bool ortho, Node othernode, FrameworkElement otherport)
        {
            Point p = node.GetElementPoint(port, Spot.Center);
            Point op = othernode.GetElementPoint(otherport, Spot.Center);
            bool right = op.X > p.X;
            return right ? 0 : 180;
        }

        public static double ConvertTimeToY(double t)
        {
            return t * MessageSpacing + LineStart;
        }

        // some parameters
        public static double LineStart = 30;  // vertical starting point in document for all Messages and Activations
        public static double LineTrail = 40;  // vertical extension of lifeline beyond all Activities
        public static double MessageSpacing = 20;  // vertical distance between Messages at different steps
        public static double ActivityInset = 3;  // vertical distance from the top or bottom that the links connect at
        public static double ActivityWidth = 15;  // width of each vertical activity bar
    }

    [Serializable]
    public class NodeData : GraphLinksModelNodeData<String>
    {
        public double Length
        {
            get { return _Length; }
            set
            {
                if (_Length != value)
                {
                    double old = _Length;
                    _Length = value;
                    RaisePropertyChanged("Length", old, value);
                }
            }
        }
        private double _Length = 100.0;

        // write the extra property
        public override XElement MakeXElement(XName n)
        {
            XElement e = base.MakeXElement(n);
            e.Add(XHelper.Attribute("Length", this.Length, 100.0));
            return e;
        }

        // read the extra property
        public override void LoadFromXElement(XElement e)
        {
            base.LoadFromXElement(e);
            this.Length = XHelper.Read("Length", e, 100.0);
        }
    }

    [Serializable]
    public class LinkData : GraphLinksModelLinkData<String, String>
    {
        public LinkData()
        {
            Time = 1;
            Duration = 3;
        }

        public double Time
        {
            get { return _Time; }
            set
            {
                if (_Time != value)
                {
                    double old = _Time;
                    _Time = value;
                    RaisePropertyChanged("Time", old, value);
                }
            }
        }
        private double _Time = 0.0;

        public double Duration
        {
            get { return _Duration; }
            set
            {
                if (_Duration != value)
                {
                    double old = _Duration;
                    _Duration = value;
                    RaisePropertyChanged("Duration", old, value);
                }
            }
        }
        private double _Duration = 1.0;

        // write the extra property
        public override XElement MakeXElement(XName n)
        {
            XElement e = base.MakeXElement(n);
            e.Add(XHelper.Attribute("Time", this.Time, 0.0));
            e.Add(XHelper.Attribute("Duration", this.Duration, 1.0));
            return e;
        }

        // read the extra property
        public override void LoadFromXElement(XElement e)
        {
            base.LoadFromXElement(e);
            this.Time = XHelper.Read("Time", e, 0.0);
            this.Duration = XHelper.Read("Duration", e, 1.0);
        }
    }
}
