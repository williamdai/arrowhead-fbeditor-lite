﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.ComponentModel;
using System.Xml.Linq;
using System.IO;
using Microsoft.Win32;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Discovery;
using System.ServiceModel.Description;
using System.Threading;

namespace FBEditor
{
    /// <summary>
    /// Interaction logic for SystemConfigurationEditor.xaml
    /// </summary>
    public partial class SystemConfigurationEditor : UserControl
    {
        private XElement xFBType;
        private string sFilename;
        private List<string> listIdentification = new List<string>() { "Standard", "Classification", "ApplicationDomain", "Function", "Type", "Description" };
        private List<string> listIdentificationDefaultValue = new List<string>() { "61499-2", "", "", "", "", "" };
        
        public SystemConfigurationEditor()
        {
            InitializeComponent();

            nVM = new SCViewModel();
            sFilename = "";

            this.IsVisibleChanged += new DependencyPropertyChangedEventHandler(UserControl_IsVisibleChanged); 
        }

        public void LoadFile(string sFile)
        {
            sFilename = sFile;
            XDocument xDoc = XDocument.Load(sFile);
            xFBType = xDoc.Root;

            ReloadData();
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                Dispatcher.BeginInvoke(
                DispatcherPriority.ContextIdle,
                new Action(delegate()
                {
                    this.textbox_SCName.Focus();
                }));
            }
        } 

        private void ReloadData()
        {
            nVM = new SCViewModel();

            nVM.Name = xFBType.Attribute("Name").Value;
            if (xFBType.Attribute("Comment") != null)
            {
                nVM.Comment = xFBType.Attribute("Comment").Value;
            }

            nVM.MenuList = new List<FBMenuList>();
            addMenuItem("Properties");
            addMenuItem("Application");

            //Load Identification
            nVM.IdentificationList = new List<IdentificationListItem>();
            int i = 0;
            foreach (string sIDItem in listIdentification)
            {
                if (xFBType.Element("Identification") != null)
                {
                    if (xFBType.Element("Identification").Attribute(sIDItem) != null)
                    {
                        addIdentificationItem(sIDItem, xFBType.Element("Identification").Attribute(sIDItem).Value);
                    }
                    else
                    {
                        addIdentificationItem(sIDItem, listIdentificationDefaultValue[i]);
                    }
                }
                else
                {
                    addIdentificationItem(sIDItem, listIdentificationDefaultValue[i]);
                }

                i++;
            }

            //Load Version Info
            nVM.VersionInfoList = new List<VersionInfoItem>();
            if (xFBType.Elements("VersionInfo").Count() > 0)
            {
                foreach (XElement xVersionInfo in xFBType.Elements("VersionInfo"))
                {
                    string Organization = "";
                    string Version = "";
                    string Author = "";
                    string Date = "";
                    string Remarks = "";
                    if (xVersionInfo.Attribute("Organization") != null)
                    {
                        Organization = xVersionInfo.Attribute("Organization").Value;
                    }
                    if (xVersionInfo.Attribute("Version") != null)
                    {
                        Version = xVersionInfo.Attribute("Version").Value;
                    }
                    if (xVersionInfo.Attribute("Author") != null)
                    {
                        Author = xVersionInfo.Attribute("Author").Value;
                    }
                    if (xVersionInfo.Attribute("Date") != null)
                    {
                        Date = xVersionInfo.Attribute("Date").Value;
                    }
                    if (xVersionInfo.Attribute("Remarks") != null)
                    {
                        Remarks = xVersionInfo.Attribute("Remarks").Value;
                    }

                    VersionInfoItem nItem = new VersionInfoItem((nVM.VersionInfoList.Count() + 1).ToString(), Organization, Version, Author, Date, Remarks);
                    nVM.VersionInfoList.Add(nItem);
                }
            }

            //Load Application
            nVM.ApplicationList = new List<ApplicationItem>();
            foreach (XElement xApp in xFBType.Elements("Application"))
            {
                ApplicationItem nItem = new ApplicationItem();
                nItem.Name = xApp.Attribute("Name").Value;
                if (xApp.Attribute("Comment") != null)
                    nItem.Comment = xApp.Attribute("Comment").Value;
                nItem.xSubAppNetwork = xApp.Element("SubAppNetwork");
                nVM.ApplicationList.Add(nItem);
            }

            this.DataContext = nVM;
        }

        private void addIdentificationItem(string sAttr, string sValue)
        {
            IdentificationListItem nItem = new IdentificationListItem(sAttr, sValue);
            nVM.IdentificationList.Add(nItem);
        }

        private void addMenuItem(string sName)
        {
            FBMenuList nMenuList = new FBMenuList(sName, "", "");
            nVM.MenuList.Add(nMenuList);
        }

        public void SaveSCFile()
        {
            SaveFileDialog saveDLG = new SaveFileDialog();
            saveDLG.Filter = "System Configuration Files (.sys)|*.sys";
            saveDLG.FileName = nVM.Name + ".sys";
            saveDLG.RestoreDirectory = true;

            // Call the ShowDialog method to show the dialog box.
            bool? userClickedOK = saveDLG.ShowDialog();

            // Process input if the user clicked OK.
            if (userClickedOK == true)
            {
                //Update Name and Comment
                xFBType.Attribute("Name").Value = nVM.Name;
                if (xFBType.Attribute("Comment") == null)
                {
                    xFBType.Add(new XAttribute("Comment", nVM.Comment));
                }
                else
                {
                    xFBType.Attribute("Comment").Value = nVM.Comment;
                }

                
                //Update Properties
                UpdateXMLFromProperties();

                //Save Applications
                if (nVM.ActiveApplication != "")
                {
                    ApplicationItem nApp = nVM.ApplicationList.Where(item => item.Name == nVM.ActiveApplication).FirstOrDefault();
                    if (nApp != null)
                    {
                        nApp.xSubAppNetwork = SCAView.GetXML();
                    }
                }

                if (xFBType.Elements("Application") != null)
                {
                    xFBType.Elements("Application").Remove();
                }
                foreach (ApplicationItem nItem in nVM.ApplicationList)
                {
                    XElement xApp = new XElement("Application");
                    XAttribute xName = new XAttribute("Name", nItem.Name);
                    XAttribute xComment = new XAttribute("Comment", nItem.Comment);
                    xApp.Add(xName, xComment, nItem.xSubAppNetwork);
                    xFBType.Add(xApp);
                }

                //Save Deployment
                xFBType.Elements("Segment").Remove();
                xFBType.Elements("Link").Remove();
                xFBType.Elements("Device").Remove();

                //Save File
                XDocument xDoc = new XDocument(xFBType);
                xDoc.Save(saveDLG.FileName);
                sFilename = saveDLG.FileName;
            }
        }

        private void UpdateXMLFromProperties()
        {
            //Update Identification
            if (xFBType.Element("Identification") != null)
            {
                xFBType.Element("Identification").Remove();
            }

            XElement xIdentification = new XElement("Identification");
            foreach (IdentificationListItem sIDItem in nVM.IdentificationList)
            {
                XAttribute xAttr = new XAttribute(sIDItem.Attribute, sIDItem.Value);
                xIdentification.Add(xAttr);
            }

            xFBType.Add(xIdentification);

            //Load Version Info
            if (xFBType.Elements("VersionInfo").Count() > 0)
            {
                xFBType.Elements("VersionInfo").Remove();
            }

            foreach (VersionInfoItem sIDItem in nVM.VersionInfoList)
            {
                XElement xIDItem = new XElement("VersionInfo");
                AddAttributeToNode(ref xIDItem, "Organization", sIDItem.Organization);
                AddAttributeToNode(ref xIDItem, "Version", sIDItem.Version);
                AddAttributeToNode(ref xIDItem, "Author", sIDItem.Author);
                AddAttributeToNode(ref xIDItem, "Date", sIDItem.Date);
                AddAttributeToNode(ref xIDItem, "Remarks", sIDItem.Remarks);
                xFBType.Add(xIDItem);
            }
        }

        private void AddAttributeToNode(ref XElement node, string sAttr, string sValue)
        {
            if (node.Attribute(sAttr) != null)
            {
                node.Attribute(sAttr).Value = sValue;
            }
            else
            {
                XAttribute xAttr = new XAttribute(sAttr, sValue);
                node.Add(xAttr);
            }
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listbox_Menu.SelectedIndex < 0) return;

            if (listbox_Menu.SelectedIndex == 0)
            {
                PropertyList.Visibility = System.Windows.Visibility.Visible;
                ApplicationList.Visibility = System.Windows.Visibility.Hidden;
                SCAView.Visibility = System.Windows.Visibility.Hidden;
            }
            else if (listbox_Menu.SelectedIndex == 1)
            {
                PropertyList.Visibility = System.Windows.Visibility.Hidden;
                ApplicationList.Visibility = System.Windows.Visibility.Visible;
                SCAView.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void Image_NewVI_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            VersionInfoItem nItem = new VersionInfoItem((nVM.VersionInfoList.Count() + 1).ToString(), "DAL", "1", "WD", System.DateTime.Now.ToShortDateString(), "");
            nVM.VersionInfoList.Add(nItem);
            this.DataContext = null;
            this.DataContext = nVM;
        }

        private void Image_DeleteVI_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            string sIndex = (sender as Label).Tag.ToString();
            int index = Convert.ToInt32(sIndex) - 1;
            if (index >= 0 && index < nVM.VersionInfoList.Count)
            {
                nVM.VersionInfoList.RemoveAt(index);
                int i = 1;
                foreach (VersionInfoItem nItem in nVM.VersionInfoList)
                {
                    nItem.Index = i.ToString();
                    i++;
                }
                this.DataContext = null;
                this.DataContext = nVM;
            }
        }

        private void textbox_SCName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.Parent != null)
                (this.Parent as TabItem).Header = textbox_SCName.Text;
        }

        private void Image_NewApp_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationItem nItem = new ApplicationItem();
            nItem.Name = "Application" + (nVM.ApplicationList.Count + 1).ToString();
            nItem.Comment = "";
            nItem.xSubAppNetwork = new XElement("SubAppNetwork");
            nVM.ApplicationList.Add(nItem);

            this.DataContext = null;
            this.DataContext = nVM;
        }

        private void Image_EditApp_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationItem nApp = nVM.ApplicationList.Where(item => item.Name == (sender as Label).Tag.ToString()).FirstOrDefault();
            if (nApp != null)
            {
                nVM.ActiveApplication = nApp.Name;
                XElement xMappings = new XElement("Mappings");
                foreach(XElement xMap in xFBType.Elements("Mapping"))
                {
                    string scAPP = xMap.Attribute("From").Value.Split('.')[0];
                    string sFBI = xMap.Attribute("From").Value.Split('.')[1];
                    if (scAPP == nApp.Name)
                    {
                        xMappings.Add(xMap);
                    }
                }
                SCAView.LoadXML(nApp.xSubAppNetwork, nApp.Name, xMappings);
                SCAView.Visibility = System.Windows.Visibility.Visible;
                ApplicationList.Visibility = System.Windows.Visibility.Hidden;

                listbox_Menu.SelectedIndex = -1;
            }
        }

        private void Image_DeleteApp_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (nVM.ActiveApplication == (sender as Label).Tag.ToString())
            {
                nVM.ActiveApplication = "";
                SCAView.Visibility = System.Windows.Visibility.Hidden;
            }
            ApplicationItem nApp = nVM.ApplicationList.Where(item => item.Name == (sender as Image).Tag.ToString()).FirstOrDefault();
            if (nApp != null)
            {
                nVM.ApplicationList.Remove(nApp);

                this.DataContext = null;
                this.DataContext = nVM;
            }
        }

        private void TextBox_AppName_TextChanged(object sender, TextChangedEventArgs e)
        {
            nVM.ActiveApplication = (sender as TextBox).Text;
        }

        private void SCAView_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == false)
            {
                ApplicationItem nApp = nVM.ApplicationList.Where(item => item.Name == nVM.ActiveApplication).FirstOrDefault();
                nVM.ActiveApplication = "";
            }
        }
    }

    public class SCViewModel : INotifyPropertyChanged
    {
        public SCViewModel()
        { }

        public string ActiveApplication { get; set; }

        public string Name
        {
            get { return _Name; }
            set
            {
                if (_Name != value)
                {
                    string old = _Name;
                    _Name = value;
                    RaisePropertyChangedEvent("Name");
                }
            }
        }
        private string _Name = "";

        public string Comment
        {
            get { return _Comment; }
            set
            {
                if (_Comment != value)
                {
                    string old = _Comment;
                    _Comment = value;
                    RaisePropertyChangedEvent("Comment");
                }
            }
        }
        private string _Comment = "";

        public List<FBMenuList> MenuList
        {
            get { return _MenuList; }
            set
            {
                if (_MenuList != value)
                {
                    List<FBMenuList> old = _MenuList;
                    _MenuList = value;
                    RaisePropertyChangedEvent("MenuList");
                }
            }
        }
        private List<FBMenuList> _MenuList = new List<FBMenuList>();

        public List<IdentificationListItem> IdentificationList
        {
            get { return _IdentificationList; }
            set
            {
                if (_IdentificationList != value)
                {
                    List<IdentificationListItem> old = _IdentificationList;
                    _IdentificationList = value;
                    RaisePropertyChangedEvent("IdentificationList");
                }
            }
        }
        private List<IdentificationListItem> _IdentificationList = new List<IdentificationListItem>();

        public List<VersionInfoItem> VersionInfoList
        {
            get { return _VersionInfoList; }
            set
            {
                if (_VersionInfoList != value)
                {
                    List<VersionInfoItem> old = _VersionInfoList;
                    _VersionInfoList = value;
                    RaisePropertyChangedEvent("VersionInfoList");
                }
            }
        }
        private List<VersionInfoItem> _VersionInfoList = new List<VersionInfoItem>();

        public List<SystemComponentList> ComponentList
        {
            get { return _ComponentList; }
            set
            {
                if (_ComponentList != value)
                {
                    List<SystemComponentList> old = _ComponentList;
                    _ComponentList = value;
                    RaisePropertyChangedEvent("ComponentList");
                }
            }
        }
        private List<SystemComponentList> _ComponentList = new List<SystemComponentList>();

        public List<ApplicationItem> ApplicationList
        {
            get { return _ApplicationList; }
            set
            {
                if (_ApplicationList != value)
                {
                    List<ApplicationItem> old = _ApplicationList;
                    _ApplicationList = value;
                    RaisePropertyChangedEvent("ApplicationList");
                }
            }
        }
        private List<ApplicationItem> _ApplicationList = new List<ApplicationItem>();

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChangedEvent(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ApplicationItem : INotifyPropertyChanged
    {
        public XElement xSubAppNetwork { get; set; }

        public string Name
        {
            get { return _Name; }
            set
            {
                if (_Name != value)
                {
                    string old = _Name;
                    _Name = value;
                    RaisePropertyChangedEvent("Name");
                }
            }
        }
        private string _Name = "";

        public string Comment
        {
            get { return _Comment; }
            set
            {
                if (_Comment != value)
                {
                    string old = _Comment;
                    _Comment = value;
                    RaisePropertyChangedEvent("Comment");
                }
            }
        }
        private string _Comment = "";

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChangedEvent(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class SystemComponentList : INotifyPropertyChanged
    {
        public string Name
        {
            get { return _Name; }
            set
            {
                if (_Name != value)
                {
                    string old = _Name;
                    _Name = value;
                    RaisePropertyChangedEvent("Name");
                }
            }
        }
        private string _Name = "";

        public string Type
        {
            get { return _Type; }
            set
            {
                if (_Type != value)
                {
                    string old = _Type;
                    _Type = value;
                    RaisePropertyChangedEvent("Type");
                }
            }
        }
        private string _Type = "";

        public string Path
        {
            get { return _Path; }
            set
            {
                if (_Path != value)
                {
                    string old = _Path;
                    _Path = value;
                    RaisePropertyChangedEvent("Path");
                }
            }
        }
        private string _Path = "";

        public ImageSource ImgSource
        {
            get { return _ImgSource; }
            set
            {
                if (_ImgSource != value)
                {
                    ImageSource old = _ImgSource;
                    _ImgSource = value;
                    RaisePropertyChangedEvent("ImgSource");
                }
            }
        }
        private ImageSource _ImgSource;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChangedEvent(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

}