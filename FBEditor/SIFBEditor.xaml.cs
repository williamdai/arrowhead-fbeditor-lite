﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.ComponentModel;
using System.Xml.Linq;
using System.IO;
using Microsoft.Win32;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Discovery;
using System.ServiceModel.Description;
using System.Threading;

namespace FBEditor
{
    /// <summary>
    /// Interaction logic for SIFBEditor.xaml
    /// </summary>
    public partial class SIFBEditor : UserControl
    {
        private XElement xFBType;
        private string sFilename;
        private List<string> listIdentification = new List<string>() { "Standard", "Classification", "ApplicationDomain", "Function", "Type", "Description" };
        private List<string> listIdentificationDefaultValue = new List<string>() { "61499-2", "", "", "", "", "" };
        private List<string> listCompilerInfo = new List<string>() { "header", "classdef" };
        
        public SIFBEditor()
        {
            InitializeComponent();

            nVM = new SIFBViewModel();
            sFilename = "";

            this.IsVisibleChanged += new DependencyPropertyChangedEventHandler(UserControl_IsVisibleChanged); 
        }

        public void LoadFile(string sFile)
        {
            sFilename = sFile;

            XDocument xDoc = XDocument.Load(sFile);
            xFBType = xDoc.Root;

            ReloadData();
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                Dispatcher.BeginInvoke(
                DispatcherPriority.ContextIdle,
                new Action(delegate()
                {
                    this.textbox_BFBName.Focus();
                }));
            }
        } 

        private void ReloadData()
        {
            nVM = new SIFBViewModel();

            nVM.Name = xFBType.Attribute("Name").Value;
            if (xFBType.Attribute("Comment") != null)
            {
                nVM.Comment = xFBType.Attribute("Comment").Value;
            }

            nVM.MenuList = new List<FBMenuList>();
            addMenuItem("Properties");
            addMenuItem("Interface");
            addMenuItem("Service");
            addMenuItem("Algorithm");

            //Load Service List
            if (xFBType.Element("Service") != null)
            {
                nVM.ServiceList = new List<string>();
                foreach (XElement xALG in xFBType.Element("Service").Elements("Algorithm"))
                {
                    string nItem = xALG.Attribute("Name").Value;
                    nVM.ServiceList.Add(nItem);
                }
            }

            //Load Identification
            nVM.IdentificationList = new List<IdentificationListItem>();
            int i = 0;
            foreach (string sIDItem in listIdentification)
            {
                if (xFBType.Element("Identification") != null)
                {
                    if (xFBType.Element("Identification").Attribute(sIDItem) != null)
                    {
                        addIdentificationItem(sIDItem, xFBType.Element("Identification").Attribute(sIDItem).Value);
                    }
                    else
                    {
                        addIdentificationItem(sIDItem, listIdentificationDefaultValue[i]);
                    }
                }
                else
                {
                    addIdentificationItem(sIDItem, listIdentificationDefaultValue[i]);
                }

                i++;
            }

            //Load Compiler Info
            nVM.CompilerInfoList = new List<CompilerInfoItem>();
            if (xFBType.Element("CompilerInfo") != null)
            {
                if (xFBType.Element("CompilerInfo").Elements().Count() > 0)
                {
                    foreach (XElement xCompilerInfo in xFBType.Element("CompilerInfo").Elements())
                    {
                        string header = "";
                        string classdef = "";
                        if (xCompilerInfo.Attribute("header") != null)
                        {
                            header = xCompilerInfo.Attribute("header").Value;
                        }
                        if (xCompilerInfo.Attribute("classdef") != null)
                        {
                            classdef = xCompilerInfo.Attribute("classdef").Value;
                        }

                        CompilerInfoItem nItem = new CompilerInfoItem((nVM.CompilerInfoList.Count() + 1).ToString(), header, classdef);
                        nVM.CompilerInfoList.Add(nItem);
                    }
                }
            }

            //Load Version Info
            nVM.VersionInfoList = new List<VersionInfoItem>();
            if (xFBType.Elements("VersionInfo").Count() > 0)
            {
                foreach (XElement xVersionInfo in xFBType.Elements("VersionInfo"))
                {
                    string Organization = "";
                    string Version = "";
                    string Author = "";
                    string Date = "";
                    string Remarks = "";
                    if (xVersionInfo.Attribute("Organization") != null)
                    {
                        Organization = xVersionInfo.Attribute("Organization").Value;
                    }
                    if (xVersionInfo.Attribute("Version") != null)
                    {
                        Version = xVersionInfo.Attribute("Version").Value;
                    }
                    if (xVersionInfo.Attribute("Author") != null)
                    {
                        Author = xVersionInfo.Attribute("Author").Value;
                    }
                    if (xVersionInfo.Attribute("Date") != null)
                    {
                        Date = xVersionInfo.Attribute("Date").Value;
                    }
                    if (xVersionInfo.Attribute("Remarks") != null)
                    {
                        Remarks = xVersionInfo.Attribute("Remarks").Value;
                    }

                    VersionInfoItem nItem = new VersionInfoItem((nVM.VersionInfoList.Count() + 1).ToString(), Organization, Version, Author, Date, Remarks);
                    nVM.VersionInfoList.Add(nItem);
                }
            }

            //Store Algorithms
            foreach (XElement xALG in xFBType.Descendants("Algorithm"))
            {
                nVM.xServices.Add(xALG);
            }

            this.DataContext = nVM;

            SIFBInterfaceView.LoadXML(xFBType);
            SIFBSSDView.LoadXML(xFBType);
        }

        private void addIdentificationItem(string sAttr, string sValue)
        {
            IdentificationListItem nItem = new IdentificationListItem(sAttr, sValue);
            nVM.IdentificationList.Add(nItem);
        }

        private void addMenuItem(string sName)
        {
            FBMenuList nMenuList = new FBMenuList(sName, "", "");
            nVM.MenuList.Add(nMenuList);
        }

        public void SaveFBTFile()
        {
            SaveFileDialog saveDLG = new SaveFileDialog();
            saveDLG.Filter = "Function Block Files (.fbt)|*.fbt";
            saveDLG.FileName = nVM.Name + ".fbt";
            saveDLG.RestoreDirectory = true;

            // Call the ShowDialog method to show the dialog box.
            bool? userClickedOK = saveDLG.ShowDialog();

            // Process input if the user clicked OK.
            if (userClickedOK == true)
            {
                //Update Name and Comment
                xFBType.Attribute("Name").Value = nVM.Name;
                if (xFBType.Attribute("Comment") == null)
                {
                    xFBType.Add(new XAttribute("Comment", nVM.Comment));
                }
                else
                {
                    xFBType.Attribute("Comment").Value = nVM.Comment;
                }

                //Update Properties
                UpdateXMLFromProperties();

                //Update Interface
                updateXMLInterface();

                if (xFBType.Element("Service") != null)
                    xFBType.Element("Service").Remove();
                XElement xService = new XElement("Service");

                //Update Internal Vars
                xService.Add(SIFBInterfaceView.getXMLInternalVars());

                //Update Service Sequence
                XElement xNew = SIFBSSDView.getXML();
                xService.Add(xNew.Attribute("RightInterface"), xNew.Attribute("LeftInterface"), xNew.Attribute("Comment"));
                foreach (XElement xSS in xNew.Elements("ServiceSequence"))
                {
                    xService.Add(xSS);
                }
                //Remove Name Attribute for Servive Transaction
                foreach (XElement xSST in xService.Descendants("ServiceTransaction"))
                {
                    xSST.Attribute("Name").Remove();
                }

                //Update Algorithms
                updateXMLALG();
                foreach (XElement xALG in nVM.xServices.Elements())
                {
                    xService.Add(xALG);
                }

                xFBType.Add(xService);

                //Validate Duplicated Service Sequence Name and Algorithm Name
                if (xFBType.Descendants("ServiceSequence").Count() > 1)
                {
                    foreach (XElement node in xFBType.Descendants("ServiceSequence"))
                    {
                        int iTotal = xFBType.Descendants("ServiceSequence").Where(item => item.Attribute("Name").Value == node.Attribute("Name").Value).Count();

                        if (iTotal > 1)
                        {
                            MessageBox.Show("Duplicated Service Sequence Name: " + node.Attribute("Name").Value, "ERROR");
                            return;
                        }
                    }
                }

                if (xFBType.Descendants("Algorithm").Count() > 1)
                {
                    foreach (XElement node in xFBType.Descendants("Algorithm"))
                    {
                        int iTotal = xFBType.Descendants("Algorithm").Where(item => item.Attribute("Name").Value == node.Attribute("Name").Value).Count();

                        if (iTotal > 1)
                        {
                            MessageBox.Show("Duplicated Algorithm Name: " + node.Attribute("Name").Value, "ERROR");
                            return;
                        }
                    }
                }

                //Save File
                XDocument xDoc = new XDocument(xFBType);
                xDoc.Save(saveDLG.FileName);
                sFilename = saveDLG.FileName;
            }
        }

        private void UpdateXMLFromProperties()
        {
            //Update Identification
            if (xFBType.Element("Identification") != null)
            {
                xFBType.Element("Identification").Remove();
            }

            XElement xIdentification = new XElement("Identification");
            foreach (IdentificationListItem sIDItem in nVM.IdentificationList)
            {
                XAttribute xAttr = new XAttribute(sIDItem.Attribute, sIDItem.Value);
                xIdentification.Add(xAttr);
            }

            xFBType.Add(xIdentification);

            //Update Compiler Info
            if (xFBType.Element("CompilerInfo") != null)
            {
                xFBType.Element("CompilerInfo").RemoveNodes();
            }
            else
            {
                XElement xCI = new XElement("CompilerInfo");
                xFBType.Add(xCI);
            }
            
            foreach (CompilerInfoItem sIDItem in nVM.CompilerInfoList)
            {
                XElement xIDItem = new XElement("Compiler");
                AddAttributeToNode(ref xIDItem, "header", sIDItem.header);
                AddAttributeToNode(ref xIDItem, "classdef", sIDItem.classdef);
                xFBType.Element("CompilerInfo").Add(xIDItem);
            }
            
            //Load Version Info
            if (xFBType.Elements("VersionInfo").Count() > 0)
            {
                xFBType.Elements("VersionInfo").Remove();
            }

            foreach (VersionInfoItem sIDItem in nVM.VersionInfoList)
            {
                XElement xIDItem = new XElement("VersionInfo");
                AddAttributeToNode(ref xIDItem, "Organization", sIDItem.Organization);
                AddAttributeToNode(ref xIDItem, "Version", sIDItem.Version);
                AddAttributeToNode(ref xIDItem, "Author", sIDItem.Author);
                AddAttributeToNode(ref xIDItem, "Date", sIDItem.Date);
                AddAttributeToNode(ref xIDItem, "Remarks", sIDItem.Remarks);
                xFBType.Add(xIDItem);
            }
        }

        private void AddAttributeToNode(ref XElement node, string sAttr, string sValue)
        {
            if (node.Attribute(sAttr) != null)
            {
                node.Attribute(sAttr).Value = sValue;
            }
            else
            {
                XAttribute xAttr = new XAttribute(sAttr, sValue);
                node.Add(xAttr);
            }
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listbox_Menu.SelectedIndex < 0) return;

            if (listbox_Menu.SelectedIndex == 0)
            {
                PropertyList.Visibility = System.Windows.Visibility.Visible;
                SIFBInterfaceView.Visibility = System.Windows.Visibility.Hidden;
                SIFBSSDView.Visibility = System.Windows.Visibility.Hidden;
                ServiceList.Visibility = System.Windows.Visibility.Hidden;
                SIFBServiceWindow.Visibility = System.Windows.Visibility.Hidden;
            }
            else if (listbox_Menu.SelectedIndex == 1)
            {
                PropertyList.Visibility = System.Windows.Visibility.Hidden;
                SIFBInterfaceView.Visibility = System.Windows.Visibility.Visible;
                SIFBSSDView.Visibility = System.Windows.Visibility.Hidden;
                ServiceList.Visibility = System.Windows.Visibility.Hidden;
                SIFBServiceWindow.Visibility = System.Windows.Visibility.Hidden;
            }
            else if (listbox_Menu.SelectedIndex == 2)
            {
                PropertyList.Visibility = System.Windows.Visibility.Hidden;
                SIFBInterfaceView.Visibility = System.Windows.Visibility.Hidden;
                SIFBSSDView.Visibility = System.Windows.Visibility.Visible;
                ServiceList.Visibility = System.Windows.Visibility.Hidden;
                SIFBServiceWindow.Visibility = System.Windows.Visibility.Hidden;
            }
            else if (listbox_Menu.SelectedIndex == 3)
            {
                PropertyList.Visibility = System.Windows.Visibility.Hidden;
                SIFBInterfaceView.Visibility = System.Windows.Visibility.Hidden;
                SIFBSSDView.Visibility = System.Windows.Visibility.Hidden;
                ServiceList.Visibility = System.Windows.Visibility.Visible;
                SIFBServiceWindow.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void Image_New_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            XElement xNewALG = new XElement("Algorithm");
            int iCount = xFBType.Element("Service").Elements("Algorithm").Count();
            XAttribute xALGName = new XAttribute("Name", "New" + (iCount + 1).ToString());
            xNewALG.Add(xALGName);
            XElement xCode = new XElement("Other");
            XAttribute xCodelanguage = new XAttribute("Language", "CPP");
            XAttribute xCodeText = new XAttribute("Text", "");
            xCode.Add(xCodelanguage, xCodeText);
            xNewALG.Add(xCode);
            if (nVM.xServices != null)
            {
                nVM.xServices.Add(xNewALG);
                ReloadServiceList();
            }
        }

        private void Image_NewCI_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            CompilerInfoItem nItem = new CompilerInfoItem((nVM.CompilerInfoList.Count() + 1).ToString(), "", "");
            nVM.CompilerInfoList.Add(nItem);
            this.DataContext = null;
            this.DataContext = nVM;
        }

        private void Image_NewVI_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            VersionInfoItem nItem = new VersionInfoItem((nVM.VersionInfoList.Count() + 1).ToString(), "DAL", "1", "WD", System.DateTime.Now.ToShortDateString(), "");
            nVM.VersionInfoList.Add(nItem);
            this.DataContext = null;
            this.DataContext = nVM;
        }

        private void Image_DeleteCI_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            string sIndex = (sender as Label).Tag.ToString();
            int index = Convert.ToInt32(sIndex) - 1;
            if (index >= 0 && index < nVM.CompilerInfoList.Count)
            {
                nVM.CompilerInfoList.RemoveAt(index);
                int i = 1;
                foreach (CompilerInfoItem nItem in nVM.CompilerInfoList)
                {
                    nItem.Index = i.ToString();
                    i++;
                }
                this.DataContext = null;
                this.DataContext = nVM;
            }
        }

        private void Image_DeleteVI_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            string sIndex = (sender as Label).Tag.ToString();
            int index = Convert.ToInt32(sIndex) - 1;
            if (index >= 0 && index < nVM.VersionInfoList.Count)
            {
                nVM.VersionInfoList.RemoveAt(index);
                int i = 1;
                foreach (VersionInfoItem nItem in nVM.VersionInfoList)
                {
                    nItem.Index = i.ToString();
                    i++;
                }
                this.DataContext = null;
                this.DataContext = nVM;
            }
        }

        private void Image_Edit_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            string sALGName = (sender as Label).Tag.ToString();

            SIFBServiceWindow.Visibility = System.Windows.Visibility.Visible;
            ServiceList.Visibility = System.Windows.Visibility.Hidden;
            SIFBServiceWindow.LoadXML(nVM.xServices, xFBType, sALGName);
            nVM.ActiveService = sALGName;

            listbox_Menu.SelectedIndex = -1;
        }

        private void Image_Delete_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            string sALGName = (sender as Label).Tag.ToString();
            XElement xALGNode = nVM.xServices.Elements("Algorithm").Where(item => item.Attribute("Name").Value == sALGName).FirstOrDefault();

            //Remove this node and reload EC Algorithm Editor
            if (xALGNode != null)
            {
                nVM.xServices.Elements("Algorithm").Where(item => item.Attribute("Name").Value == sALGName).FirstOrDefault().Remove();

                ReloadServiceList();
            }
        }

        private void ReloadServiceList()
        {
            this.DataContext = null;

            nVM.ServiceList = new List<string>();
            foreach (XElement xALG in nVM.xServices.Elements("Algorithm"))
            {
                string nItem = xALG.Attribute("Name").Value;
                nVM.ServiceList.Add(nItem);
            }

            SIFBSSDView.UpdateServiceList(nVM.xServices);

            this.DataContext = nVM;
        }

        private void SIFBServiceWindow_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == false)
            {
                updateXMLALG();
                nVM.ActiveService = "";
                ReloadServiceList();
            }
        }

        private void updateXMLALG()
        {
            if (nVM.ActiveService != "")
            {
                XElement xNewALG = SIFBServiceWindow.getXML();
                nVM.xServices.Elements().Where(item => item.Attribute("Name").Value == nVM.ActiveService).Remove();
                nVM.xServices.Add(xNewALG);
                ReloadServiceList();
            }
        }

        private void SIFBInterfaceView_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == false)
            {
                updateXMLInterface();
                SIFBSSDView.UpdateEventList(xFBType);
            }
        }

        private void updateXMLInterface()
        {
            if (xFBType.Element("InterfaceList") != null)
                xFBType.Element("InterfaceList").Remove();
            xFBType.Add(SIFBInterfaceView.getXML());

            if (xFBType.Element("Service") != null)
            {
                if (xFBType.Element("Service").Element("InternalVars") != null)
                {
                    xFBType.Element("Service").Element("InternalVars").Remove();
                }
                xFBType.Element("Service").Add(SIFBInterfaceView.getXMLInternalVars());
            }
        }

        private void textbox_BFBName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.Parent != null)
                (this.Parent as TabItem).Header = textbox_BFBName.Text;
        }
    }

    public class SIFBViewModel : INotifyPropertyChanged
    {
        public SIFBViewModel()
        { }

        public XElement xServices = new XElement("Services");
        public string ActiveService = "";

        public string Name
        {
            get { return _Name; }
            set
            {
                if (_Name != value)
                {
                    string old = _Name;
                    _Name = value;
                    RaisePropertyChangedEvent("Name");
                }
            }
        }
        private string _Name = "";

        public string Comment
        {
            get { return _Comment; }
            set
            {
                if (_Comment != value)
                {
                    string old = _Comment;
                    _Comment = value;
                    RaisePropertyChangedEvent("Comment");
                }
            }
        }
        private string _Comment = "";

        public List<FBMenuList> MenuList
        {
            get { return _MenuList; }
            set
            {
                if (_MenuList != value)
                {
                    List<FBMenuList> old = _MenuList;
                    _MenuList = value;
                    RaisePropertyChangedEvent("MenuList");
                }
            }
        }
        private List<FBMenuList> _MenuList = new List<FBMenuList>();

        public List<string> ServiceList
        {
            get { return _ServiceList; }
            set
            {
                if (_ServiceList != value)
                {
                    List<string> old = _ServiceList;
                    _ServiceList = value;
                    RaisePropertyChangedEvent("ServiceList");
                }
            }
        }
        private List<string> _ServiceList = new List<string>();

        public List<IdentificationListItem> IdentificationList
        {
            get { return _IdentificationList; }
            set
            {
                if (_IdentificationList != value)
                {
                    List<IdentificationListItem> old = _IdentificationList;
                    _IdentificationList = value;
                    RaisePropertyChangedEvent("IdentificationList");
                }
            }
        }
        private List<IdentificationListItem> _IdentificationList = new List<IdentificationListItem>();

        public List<VersionInfoItem> VersionInfoList
        {
            get { return _VersionInfoList; }
            set
            {
                if (_VersionInfoList != value)
                {
                    List<VersionInfoItem> old = _VersionInfoList;
                    _VersionInfoList = value;
                    RaisePropertyChangedEvent("VersionInfoList");
                }
            }
        }
        private List<VersionInfoItem> _VersionInfoList = new List<VersionInfoItem>();

        public List<CompilerInfoItem> CompilerInfoList
        {
            get { return _CompilerInfoList; }
            set
            {
                if (_CompilerInfoList != value)
                {
                    List<CompilerInfoItem> old = _CompilerInfoList;
                    _CompilerInfoList = value;
                    RaisePropertyChangedEvent("CompilerInfoList");
                }
            }
        }
        private List<CompilerInfoItem> _CompilerInfoList = new List<CompilerInfoItem>();

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChangedEvent(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ForegroundGreenSelectedItemConverter : Northwoods.GoXam.Converter
    {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            SolidColorBrush f = new SolidColorBrush(Colors.Transparent);
            if (value is bool)
            {
                if ((bool)value) f = new SolidColorBrush(Color.FromArgb(0xFF, 0xa0, 0xd4, 0x68));
            }
            return f;
        }
    }
}
